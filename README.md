This repository contains all the scripts needed to run the experiment and analyse the data reported in the article  "Charalampaki, A., Peters, C., Maurer, H., Maurer, L. K., Müller, H., Verrel, J., & Filevich, E. (2023). Motor outcomes congruent with intentions may sharpen metacognitive representations. Cognition, 235, 105388"


Due to space limitation, raw data can be found along withe the preregistration of the second experiment here: ttps://osf.io/javx5. 
