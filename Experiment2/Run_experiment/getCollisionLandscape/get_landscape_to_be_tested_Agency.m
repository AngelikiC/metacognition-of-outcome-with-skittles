%% Check the landscape for target hits with changing parameters (release speed, release angle, traget position, target size) to use the resulting array in the experiment

%%

%clear all

user = 'caro';

switch user
    case {'office'}
        userpath='~/Dropbox/HiWis/Lukas/';
        resultDir=strcat(userpath, 'SkittlesMatlab/Results/');
    case {'caro'}
        userpath='/Users/Caroline/Dropbox/PhDs/Caroline/Skittles_AoM/';
        resultDir=strcat(userpath, 'SkittlesMatlab/landscapes_target_positions_sizes/');
end
addpath(strcat(userpath, 'SkittlesMatlab/Analysis/fx'));
addpath(strcat(userpath, 'SkittlesMatlab/BasicVariables'));
addpath(strcat(userpath, 'SkittlesMatlab/getCollisionLandscape/functions/'));
%%
SkitSet = set_SkitSet_outcome;

% angleRange must be the same as the accuracy of rounding alpha in
% get_leverData_1ms_LJ 
angleRange = 0:0.5:180; 

% speedRange must be the same as specified in get_leverData_1ms_LJ
speedRange = 0:.05:6; %This is in m/sec, as in Mueller & Sternad 2004 (JEP: HPP)

% target_sizes and ball_sizes: increments must be the stepsizes of the staircases in the main experiment code (as well as lower and upper bounds) 
target_sizes = .02:.003:0.14;
ball_sizes = .02:.003:0.14;

collisionFlagValue = NaN; %-.1; 

s = 0;
for speed = speedRange
    a = 0;
    s = s+1;
    for angle = angleRange
        a = a+1;
        for tsize = 1:length(target_sizes)
            %t = target_sizes(tsize);
            for bsize = 1:length(ball_sizes)
                %b = ball_sizes(bsize);
                
                SkitSet.TargetRadius = target_sizes(tsize);
                
                SkitSet.BallRadius = ball_sizes(bsize);
                
                BRP = calculate_release_parameters_toPlot(angle, speed, SkitSet, collisionFlagValue);
                
                targetHit_landscape(s,a,tsize,bsize) = target_hit(BRP);  % replaced ~isnan(BRP.x_collision) with the new function target_hit -> BRP.x_collison does not really yield target hits (only)!
            end
        end
    end
end

fname = ['tpx' num2str(SkitSet.xTarget) '_tpy' num2str(SkitSet.yTarget)]; 
save([resultDir fname, '.mat'], 'targetHit_landscape', 'speedRange', 'angleRange', 'target_sizes', 'ball_sizes');

