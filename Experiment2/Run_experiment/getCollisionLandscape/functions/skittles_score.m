function score = skittles_score( d, post )

if post == 1
    score = 0;
end

if post == 0
    if ( d > 0.5 ) 
        score = 0;
    else 
        score = 100 * ( 1 - d / 0.5 );
    end
end