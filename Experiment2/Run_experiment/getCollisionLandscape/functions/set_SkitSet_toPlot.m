function SkitSet = set_SkitSet_toPlot
%% settings for skittles task

settingsToUse = 1;

switch settingsToUse
    case 1  %Our experiment
        SkitSet.xTarget         = 0.4000; 
        SkitSet.yTarget         = 0.5000; 
        SkitSet.TargetRadius    = 0.0500;
        SkitSet.xCenter         = 0;       
        SkitSet.yCenter         = 0; 
        SkitSet.xLever          = 0;        %starting position
        SkitSet.yLever          = -1.5000;  %In Heiko's coordinates, these are 'meters' and follow Mueller and Sternad (2004)
        SkitSet.LeverLength     = 0.4000;
        SkitSet.BallRadius      = 0.0500;   %Radius of both ball and target skittle
        SkitSet.Mass            = 0.1000; 
        SkitSet.CenterRadius    = 0.25;     %radius of the center pole (base or the cone drawn by openGL)
        SkitSet.xStiff          = 1;        %Spring constant (k in Mueller and Sternad 2004) in x ...
        SkitSet.yStiff          = 1;        %and y directions
        SkitSet.Damping         = 0.0100;
        SkitSet.xOmega          = 3.1619;   %this we know. Just the frequency, used for the trajectory x(t), y(t) calculation
        SkitSet.yOmega          = 3.1619;
        SkitSet.LeverWidth      = 0.0400;
        SkitSet.max_fligth_time = 2;        % 2 sec is correct. using more? for debugging
        SkitSet.FrameRate       = 1000;      %rate of lever data measurement
        
    case 2  %Mueller & Sternad 2004
        SkitSet.xTarget         = 0.28; 
        SkitSet.yTarget         = 1.620; 
        SkitSet.xCenter         = 0; 
        SkitSet.yCenter         = -.6;
        SkitSet.xLever          = 0;        
        SkitSet.yLever          = -1.5000;  
        SkitSet.LeverLength     = 0.4000;
        SkitSet.BallRadius      = 0.0500; 
        SkitSet.Mass            = 0.1000; 
        SkitSet.CenterRadius    = 0.25; 
        SkitSet.xStiff          = 1;       
        SkitSet.yStiff          = 1;       
        SkitSet.Damping         = 0.0100;
        SkitSet.xOmega          = 3.1619;  
        SkitSet.yOmega          = 3.1619;
        SkitSet.LeverWidth      = 0.0400;
        SkitSet.max_fligth_time = 2;       
        SkitSet.FrameRate       = 1000;    
        
    case 3  %Sternad et al 2011 (Experiment 1)
        SkitSet.xTarget         = 0.35; 
        SkitSet.yTarget         = 1.250; 
        SkitSet.xCenter         = 0.105; 
        SkitSet.yCenter         = -.6;
        SkitSet.xLever          = 0;        
        SkitSet.yLever          = -1.5000;  
        SkitSet.LeverLength     = 0.4000;
        %Target skittle radius different, but not important because we just
        %plot the distance, regardless of the collision success
        SkitSet.BallRadius      = 0.02500; 
        SkitSet.Mass            = 0.1000; 
        SkitSet.CenterRadius    = 0.33; 
        SkitSet.xStiff          = 1;       
        SkitSet.yStiff          = 1;       
        SkitSet.Damping         = 0.0100;
        SkitSet.xOmega          = 3.1619;  
        SkitSet.yOmega          = 3.1619;
        SkitSet.LeverWidth      = 0.0400;
        SkitSet.max_fligth_time = 2;       
        SkitSet.FrameRate       = 1000;    
        
        
    case 4  %Sternad et al 2011 (Experiment 2)
        SkitSet.xTarget         = 0.05; 
        SkitSet.yTarget         = 1.058; 
        SkitSet.xCenter         = 0; 
        SkitSet.yCenter         = 0;
        SkitSet.xLever          = 0;        
        SkitSet.yLever          = -1.5000;  
        SkitSet.LeverLength     = 0.4000;
        %Target skittle radius different, but not important because we just
        %plot the distance, regardless of the collision success
        SkitSet.BallRadius      = 0.02500; 
        SkitSet.Mass            = 0.1000; 
        SkitSet.CenterRadius    = 0.25; 
        SkitSet.xStiff          = 1;       
        SkitSet.yStiff          = 1;       
        SkitSet.Damping         = 0.0100;
        SkitSet.xOmega          = 3.1619;  
        SkitSet.yOmega          = 3.1619;
        SkitSet.LeverWidth      = 0.0400;
        SkitSet.max_fligth_time = 2;       
        SkitSet.FrameRate       = 1000;    
        
case 5  %Heiko's example
        SkitSet.xTarget         = -0.7; 
        SkitSet.yTarget         = 0; 
        SkitSet.xCenter         = 0; 
        SkitSet.yCenter         = 0;
        SkitSet.xLever          = 0;        
        SkitSet.yLever          = -1.5000;  
        SkitSet.LeverLength     = 0.4000;
        %Target skittle radius different, but not important because we just
        %plot the distance, regardless of the collision success
        SkitSet.BallRadius      = 0.0500; 
        SkitSet.Mass            = 0.1000; 
        SkitSet.CenterRadius    = 0.25; 
        SkitSet.xStiff          = 1;       
        SkitSet.yStiff          = 1;       
        SkitSet.Damping         = 0.0100;
        SkitSet.xOmega          = sqrt(SkitSet.xStiff/SkitSet.Mass-(SkitSet.Damping/(2*SkitSet.Mass))^2); %3.1619;  
        SkitSet.yOmega          = sqrt(SkitSet.yStiff/SkitSet.Mass-(SkitSet.Damping/(2*SkitSet.Mass))^2); %3.1619;
        SkitSet.LeverWidth      = 0.0400;
        SkitSet.max_fligth_time = 2;       
        SkitSet.FrameRate       = 1000;    
   
end

