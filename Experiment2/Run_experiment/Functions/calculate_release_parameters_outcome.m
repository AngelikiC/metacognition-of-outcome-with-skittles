function BRP = calculate_release_parameters_outcome( t0, alpha, v, idx, BRP )


% Calculates a time vector and the ball position in x and y based on the
% initial conditions and affected by damping (or exponential decay). All
% these formulas are described in the Appendix A of Mueller and Sternad
% (2004). 
%
% idx is the current row index in the leverData matrix. In the main code
% it's named a bit better, leverDataIndex. Didn't want o change it here too
% to keep this function compatible with Heiko's
%
global trial;
global SkitSet;
global resultData
% Heiko had a global BRP. I wantto call this function twice to get the
% actual and the alternative trajectories. So no global here, better as a
% return value. 
%global BRP;

% replace SkitSet size of target and ball by staircased values

SkitSet.TargetRadius = resultData.stairs(2).Signal;
SkitSet.BallRadius   = resultData.stairs(3).Signal;


% 1) alpha is in degrees, alpha*pi/180 converts from degrees to rad
% 2) vx = v * sin(alpha). This might look wrong (the projection ont x is 
% usually with cos, not sin). However this is correct because alpha here is
% the angle between the lever and the left horizontal side (see Sternad et
% al 2009 Plos Comp, figure 1 for the correct reference axis). If beta is
% the angle that the tangential velocity vector does with the right
% horizontal line just under it (as you would want if you just project v
% into vx and vy), then you get that beta = 90-alpha, so cos(beta) = sin (alpha).  
vx = v * sin( alpha * pi / 180 );
vy = v * cos( alpha * pi / 180 );
BRP.x0 = SkitSet.xLever - cos( alpha * pi / 180 ) * SkitSet.LeverLength;
BRP.y0 = SkitSet.yLever + sin( alpha * pi / 180 ) * SkitSet.LeverLength;
Ex = SkitSet.Mass * vx .^2 / 2 + SkitSet.xStiff * BRP.x0 .^2 / 2;               %E is total energy (kinetic + potential)
BRP.Ax = sqrt(2 * Ex / SkitSet.xStiff);                                     %A is (max) Amplitude in the x direction
Ey = SkitSet.Mass * vy .^2 / 2 + SkitSet.yStiff * BRP.y0 .^2 / 2;
BRP.Ay = sqrt( 2 * Ey / SkitSet.yStiff );
if ( BRP.Ax ~= 0 )
     a = BRP.x0 / BRP.Ax;
     BRP.phix = acos(a);                                                    %Phase difference will be plugged in the BallX(t), BallX(t) below
else
    BRP.phix = 0;
end
if ( vx > 0 ) 
    BRP.phix = -BRP.phix;
end
if ( BRP.Ay ~= 0 )
    a = BRP.y0 / BRP.Ay;
    BRP.phiy = acos( a ); 
else
    BRP.phiy = 0;
end
if ( vy > 0 )
    BRP.phiy = -BRP.phiy;
end

%Calculate d (minimal distance to target, below), score and check for post hits
%All these events will be (strictly speaking) displayed after a while, not
%at release. But can already be calculated at release. 
timeVector = linspace(0, SkitSet.max_fligth_time, 3000);                                        %why only every 3 points?
Damping = exp( -timeVector * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
BallX = BRP.Ax .* Damping .* cos( SkitSet.xOmega .* timeVector + BRP.phix );
BallY = BRP.Ay .* Damping .* cos( SkitSet.yOmega .* timeVector + BRP.phiy );

%Determine point of collision with target

distance = sqrt( ( BallX - SkitSet.xTarget ).^2 + ( BallY - SkitSet.yTarget ).^2 );
idx_collision = find( distance < ( SkitSet.TargetRadius + SkitSet.BallRadius ), 1, 'first' );
if isempty( idx_collision ) == 0
    BRP.x_collision = BallX( idx_collision );
    BRP.y_collision = BallY( idx_collision );
    BRP.t_collision = timeVector( idx_collision );
end

%Check whether the centre post was (will be) hit
distance = sqrt( ( BallX - SkitSet.xCenter ).^2 + ( BallY - SkitSet.yCenter ).^2 );
idx_PostHit = find( distance < ( SkitSet.CenterRadius + SkitSet.BallRadius ), 1, 'first' );
if ~isempty(idx_PostHit)
    PostHit = 1; % this is general posthits (also after cross!!)
%     BRP.x_collision = NaN;
%     BRP.y_collision = NaN;
%     BRP.t_collision = NaN;
else
    PostHit = 0;
end

%Calculate minimal distance to target
trajectory = sqrt( ( BallX - SkitSet.xTarget ).^2 + ( BallY - SkitSet.yTarget ).^2);
[BRP.minDist, minDistIndex] = min( trajectory ); % minDistIndex is the index (timepoint) where the ball is closest to the target. If this index is bigger than the posthit index (idx_PostHit), then we are looking at a posthit before cross (--> a 'real' posthit in our sense)
BRP.posthit = 0;

if ( PostHit && idx_PostHit < minDistIndex ) % here we look for posthits before cross --> this is what we are interested in, and this is why there is posthit=0 in landscape even if there is no x_collision parameters
  %->Ball hits center post before it hits target
  BRP.minDist = 0.5;          % From Heiko: 0.5 is more or less an arbitrary
                              % chosen distance value when participants hit 
                              %the center post. Hermann set this value many 
                              %years ago when he created the task and we never changed it.
  BRP.posthit = 1;
  BRP.t_posthit = timeVector( idx_PostHit );
  BRP.x_posthit = BallX( idx_PostHit );
  BRP.y_posthit = BallY( idx_PostHit );
end


% store to share
BRP.trajectory.x = BallX;
BRP.trajectory.y = BallY;
BRP.trajectory.time = timeVector;
BRP.score = skittles_score( BRP.minDist, BRP.posthit );
BRP.damping = Damping;
BRP.idx = idx_collision;
BRP.Ax = BRP.Ax;
BRP.Ay = BRP.Ay;

%First get the first time that the ball crosses the vertical axis
positiveX = BRP.trajectory.x > SkitSet.xCenter;
positiveY = BRP.trajectory.y > SkitSet.yCenter;
%find(diff(positiveX)) gives you the time of crossing of the vertical
%midline
BRP.crossBehindPost=find([diff(positiveX) 0] & positiveY, 1);%Add a 0 to the diff vector to make dimensions agree
BRP.posthitAfterCross=BRP.crossBehindPost<BRP.t_posthit*1000;

try
    if isnan(BRP.posthit) | isnan(BRP.posthitAfterCross)
        BRP.posthitBeforeCross=NaN;
    else
        BRP.posthitBeforeCross = BRP.posthit & ~BRP.posthitAfterCross;
    end
catch
    BRP.posthitBeforeCross=NaN;
    warning('Error calculate release parameters in trial: ')
    disp(trial);
end
