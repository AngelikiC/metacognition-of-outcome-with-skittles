function resultData = getType2Response_outcome(resultData, trial,feedback)

global win winCenter
global VASsettings
global block;
global visualBlock;
global frameCount

startConfJudge = tic;

[topEdge, lowEdge]  = drawConfidenceVAS_vertical_outcome;
initialY= round(rand * (lowEdge(end) - topEdge(1)) + topEdge(1));

SetMouse(0,initialY,win)
[mouse.x, mouse.y, buttons] = GetMouse(win);

while ~any(buttons) % wait for press
    if errorTrial()
        if visualBlock
            resultData.visual.type1(block).errorTrials(trial)=1;
        else
            resultData.type1(block).errorTrials(trial)=1;
        end
        break
    end
    % [topEdge, lowEdge]  = drawConfidenceVAS;
    [topEdge, lowEdge]  = drawConfidenceVAS_vertical_outcome;
    [mouse.x, mouse.y, buttons] = GetMouse(win);
    if mouse.y > lowEdge(end); mouse.y = lowEdge(end); end
    if mouse.y < topEdge(1);    mouse.y = topEdge(1);    end
    Screen('DrawLine', win, 255 * [0 0 0], winCenter.x - VASsettings.height,mouse.y,  winCenter.x + VASsettings.height, mouse.y, 4);
    Screen('Flip', win);
    frameCount=frameCount+1;
end

while any(buttons) % wait for release
    % if space is pressed-> error trial, abort
    if (resultData.type1(block).errorTrials(trial)==1 && ~visualBlock)
        break
    elseif visualBlock
        if resultData.visual.type1(block).errorTrials(trial);break;end
    elseif errorTrial()
        if visualBlock
            resultData.visual.type1(block).errorTrials(trial)=1;
        else
            resultData.type1(block).errorTrials(trial)=1;
        end
        break
    end
    [topEdge, lowEdge]  = drawConfidenceVAS_vertical_outcome;
    [mouse.x, mouse.y, buttons] = GetMouse(win);
    if mouse.y > lowEdge(end); mouse.y = lowEdge(end); end
    if mouse.y < topEdge(1);    mouse.y = topEdge(1);    end
    Screen('DrawLine', win, 255 * [0 0 0], winCenter.x - VASsettings.height,mouse.y,  winCenter.x + VASsettings.height, mouse.y, 4);
    Screen('Flip', win);
    frameCount=frameCount+1;
end
  %---------------
    % Give feedback:
    %---------------
        
    if feedback
        if resultData.type1(block).correct(trial)
            feedbackColor = 255 * [0 1 0];  %green
        else
            feedbackColor = 255 * [1 0 0];  %red
        end
            
        drawConfidenceVAS_vertical_outcome;
        
        %draw the cursor centered on the mouse y-position
        Screen('DrawLine', win, feedbackColor, winCenter.x - VASsettings.height, mouse.y, winCenter.x + VASsettings.height, mouse.y, 4);
        Screen('Flip',win);
        frameCount=frameCount+1;
        
        WaitSecs(.5);
    end
if visualBlock
    resultData.visual.type2(block).RT(trial)        = toc(startConfJudge);
    resultData.visual.type2(block).mousePosY(trial) = mouse.y;
    resultData.visual.type2(block).conf(trial)      = 100-(topEdge(1)-mouse.y) * -100 / (lowEdge(end) - topEdge(1));
    resultData.visual.type2(block).initialY(trial)  = initialY;
else
    resultData.type2(block).RT(trial)        = toc(startConfJudge);
    resultData.type2(block).mousePosY(trial) = mouse.y;
    resultData.type2(block).conf(trial)      = 100-(topEdge(1)-mouse.y) * -100 / (lowEdge(end) - topEdge(1));
    resultData.type2(block).initialY(trial)  = initialY;
end
end
