function calculate_collision_parameters_outcome( t0, vx, vy )

global SkitSet;
global BRP;
global BCP;

BCP.t0 = t0;
BCP.x0 = BRP.x_collision;
BCP.y0 = BRP.y_collision;
BCP.vx = vx;
BCP.vy = vy;
Ex = SkitSet.Mass * vx^2 / 2 + SkitSet.xStiff * BCP.x0^2 / 2;
BCP.Ax = sqrt(2 * Ex / SkitSet.xStiff);
Ey = SkitSet.Mass * vy^2 / 2 + SkitSet.yStiff * BCP.y0^2 / 2;
BCP.Ay = sqrt( 2 * Ey / SkitSet.yStiff );
if ( BCP.Ax ~= 0 )
     a = BCP.x0 / BCP.Ax;
     BCP.phix = acos(a);
else
    BCP.phix = 0;
end
if ( vx > 0 ) 
    BCP.phix = -BCP.phix;
end
if ( BCP.Ay ~= 0 )
    a = BCP.y0 / BCP.Ay;
    BCP.phiy = acos( a ); 
else
    BCP.phiy = 0;
end
if ( vy > 0 )
    BCP.phiy = -BCP.phiy;
end

%Check for post hits
tmp = linspace(0, 2, 3000);
Damping = exp( -tmp * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
BallX = BCP.Ax .* Damping .* cos( SkitSet.xOmega .* tmp + BCP.phix );
BallY = BCP.Ay .* Damping .* cos( SkitSet.yOmega .* tmp + BCP.phiy );

%Check whether the centre post was hit
distance = sqrt( ( BallX - SkitSet.xCenter ).^2 + ( BallY - SkitSet.yCenter ).^2 );
idx_PostHit = find( distance <= ( SkitSet.CenterRadius + SkitSet.BallRadius ), 1, 'first' );
if idx_PostHit > 0
    BCP.posthit = 1;
    BCP.t_posthit = tmp( idx_PostHit );
    BCP.x_posthit = BallX( idx_PostHit );
    BCP.y_posthit = BallY( idx_PostHit );
else
    BCP.posthit = 0;
end