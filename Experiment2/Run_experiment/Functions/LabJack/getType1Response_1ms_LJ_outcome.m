function  status = getType1Response_1ms_LJ_outcome(status, BRP, BRP_alt, colorTraces,dev,backlog,streamTime)
% global GL
% global win
global SkitSet
global trial
global resultData
% global frameCount;
global visualBlock;
global block;
global leverData
global leverDataIndex

% perhaps I can set status to global.... makes the code easier to read

color(1,:) = [0.9 0.2 0.9];
color(2,:) = [0.1 0.8 0.1];


%use predetermined colors for the rest of trials
actualTraceColor=colorTraces.actualTrace(trial);
alternativeTraceColor=colorTraces.alternativeTrace(trial);

%initialize some variables
touch=0;
firstEntry=1;
NrOfStreamReads=1;


%start stream and read first time for initial angle
%dev.streamStart;
while backlog>dev.scansPerRead
    [data,backlog]=dev.streamRead;
end
startAngle=data(2);

initialSel = resultData.type1(block).initialTrajSelection(trial); % random array defined in main code in 'combinations'

%adjust start angle (randomly), so that the selection is not jumpy in the
%beginning and select randomly if left or right trajectory is first selected

if initialSel == 1
    startAngleAdjusted=startAngle-0.5 * SkitSet.response1Selection;
elseif initialSel == 0
    startAngleAdjusted=startAngle+0.5 * SkitSet.response1Selection;
end

% loop while the lever is not touched. store lever data and show scene
while ~touch
    while backlog>=dev.scansPerRead || firstEntry
        firstEntry=0;
        [data,backlog]=dev.streamRead;
        
        % leave loop if touched
        if data(1)>dev.gripThreshold
            touch=1;
        end
        
        %read all data from backlog
        dataAv=true;
        while dataAv
            streamTime=streamTime+1/dev.StreamRate;
            NrOfStreamReads=NrOfStreamReads+1;
            %Define the index to store the data in such a way that, if you go over the
            %preallocated length, you start again from 1
            leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;
            leverData( leverDataIndex, 1: 4) = [streamTime data(1,1) data(2) 0];
            
            t = leverData( leverDataIndex, 1 );
            %take the average of last 6 frames (reduce jitter)
            if leverDataIndex>5 && t>5*1/SkitSet.FrameRate && ~visualBlock
                leverData(leverDataIndex,3)=mean(leverData( leverDataIndex-5:leverDataIndex, 3 ));
            elseif t>5*1/SkitSet.FrameRate &&~visualBlock
                %    angle = mean([leverData(mod(leverDataIndex-5, SkitSet.leverDataPreallocateLength):SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]); %lets see if this works (if not use line above)
                leverData(leverDataIndex,3)=mean([leverData( SkitSet.leverDataPreallocateLength+leverDataIndex-5:SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]);
            end
            
            if backlog<=dev.scansPerRead*2
                dataAv=false;
            else
                [data,backlog]=dev.streamRead;
            end
        end
    end
    [status,correct,~] = drawTrajectories_outcome(startAngleAdjusted, status, BRP, BRP_alt, [actualTraceColor alternativeTraceColor],color,data(2));
    firstEntry=1;
end

% loop while the lever tip is touched
while touch
    while backlog>=dev.scansPerRead*2 || firstEntry
        firstEntry=0;
        [data,backlog]=dev.streamRead;
        
        dataAv=true;
        while dataAv
            streamTime=streamTime+1/dev.StreamRate;  
            if data(1)>dev.gripThreshold; NrOfStreamReads=NrOfStreamReads+1; end
            % before line above was: NrOfStreamReads=NrOfStreamReads+1;
            %Define the index to store the data in such a way that, if you go over the
            %preallocated length, you start again from 1
            leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;
            
            leverData( leverDataIndex, 1: 4) = [streamTime data(1,1) data(2) 0];
            
            t = leverData( leverDataIndex, 1 );
            %take the average of last 6 frames (reduce jitter)
            if leverDataIndex>5 && t>5*1/SkitSet.FrameRate && ~visualBlock
                leverData(leverDataIndex,3)=mean(leverData( leverDataIndex-5:leverDataIndex, 3 ));
            elseif t>5*1/SkitSet.FrameRate &&~visualBlock
                %    angle = mean([leverData(mod(leverDataIndex-5, SkitSet.leverDataPreallocateLength):SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]); %lets see if this works (if not use line above)
                leverData(leverDataIndex,3)=mean([leverData( SkitSet.leverDataPreallocateLength+leverDataIndex-5:SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]);
            end
            
            if backlog<=dev.scansPerRead*2
                dataAv=false;
            else
                [data,backlog]=dev.streamRead;
            end
        end        
        
%         part=sum(data(1,:)>dev.gripThreshold); % this is designed for reading multiple data points at once... too complicated for scans per read =1
        if data(1)<dev.gripThreshold % before: data(1)<dev.scansPerRead 
            touch=0;
            RT=NrOfStreamReads/dev.StreamRate;
        end

    end
    [status,correct,leftTrajectorySelected]=drawTrajectories_outcome(startAngleAdjusted, status, BRP, BRP_alt, [actualTraceColor alternativeTraceColor],color,data(2));
    firstEntry=1;
end

%after releasing the lever stop stream and store data 
dev.streamStop;

% check for targethit

targetHit = target_hit_outcome(BRP);
targetHit_alt = target_hit_outcome(BRP_alt);

%% use this to find the smallest vdiff that gives acceptable result mainly for same condition
% altranarive = BRP_alt.v
% reaal = BRP.v
% difrene = abs(BRP.v - BRP_alt.v) %(this is the correct way vdiff is defined)
% correct = 1
% warning('HARD CODED CORRECT!!! REMOVE!!!')
%fprintf('\n\n alternative: %.3f real: %.3f  difference: %.3f condition: %s  \n\n ', BRP_alt.v,BRP.v, abs(BRP.v - BRP_alt.v),resultData.block(block).condition(trial) );

%store answer
if visualBlock
    resultData.visual.type1(block).RT(trial)           = RT; %TODO: ms precission?
    resultData.visual.type1(block).correct(trial)      = correct;
    resultData.visual.type1(block).response(trial)     =leftTrajectorySelected;
    resultData.visual.type1(block).initialLeverAngle(trial)   =startAngle;
    resultData.visual.type1(block).leverAngleAdjusted(trial)  =startAngleAdjusted;
    if correct
        resultData.visual.type1(block).chosenColor(trial)=actualTraceColor;
    else
        resultData.visual.type1(block).chosenColor(trial)=alternativeTraceColor;
    end   
else
    resultData.type1(block).RT(trial)           = RT; %TODO: ms precission?
    resultData.type1(block).correct(trial)      = correct;
    resultData.type1(block).response(trial)     =leftTrajectorySelected;
    resultData.type1(block).initialLeverAngle(trial)   =startAngle;
    resultData.type1(block).leverAngleAdjusted(trial)  =startAngleAdjusted;
    resultData.type1(block).targetHit(trial)    = targetHit;
    resultData.type1(block).targetHit_alt(trial)    = targetHit_alt;
    if correct
        resultData.type1(block).chosenColor(trial)=actualTraceColor;
    else
        resultData.type1(block).chosenColor(trial)=alternativeTraceColor;
    end
end




   
end