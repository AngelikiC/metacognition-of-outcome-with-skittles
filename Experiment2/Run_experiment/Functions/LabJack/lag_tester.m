close all
clear

homePath = '~/Users/metamotorlab/Dropbox/PhDs7caroline/Skittles_AoM/SkittlesMatlab/Functions/Labjack';
addpath(genpath(homePath))

%% initialize stuff
bufferValue = 2;

streamTime = 0;
index = 0;
dev = LJ_Handle;
leverData = nan(10000,8);
% 1.angle 2.grip. 3.streamTime 4.time before read
%5. time after read %6. flip

%% open Screen

whichScreen     = 0;
bg              = 0.65;
grey            = round(255*[bg bg bg]);

Screen('Preference', 'SkipSyncTests', 1); % 1 or 2->without testing
debuggingRect = [0 0 800 800];
%[win , winRect] = PsychImaging( 'OpenWindow', whichScreen, grey, debuggingRect, [], [], 0, 0 );


%% start DAQ
dev.streamStart;

%% loop
while index <10000
    index=index+1;
    t_before_r = GetSecs;
    [leverRead, backlog, otherbacklog]=dev.streamRead;
    leverData(index,:) = [leverRead(2,1),leverRead(1,1),streamTime,t_before_r,GetSecs,0,double(backlog), double(otherbacklog)];
    dataAv=true;
    
    
    while dataAv
        streamTime=streamTime+1/1000;
        if backlog<=bufferValue
            dataAv=false;
        else
            index=index+1;
            t_before_r = GetSecs;
            [leverRead, backlog, otherbacklog]=dev.streamRead;
            
            leverData(index,:) = [leverRead(2,1),leverRead(1,1),streamTime,t_before_r,GetSecs,0, double(backlog), double(otherbacklog)];
        end
    end
    %Screen('Flip',win);
    leverData(index,6)=1; %flip
end

%% close everything
dev.streamStop;
dev.close;
%Screen('CloseAll');



plot(diff(find(leverData(:,6))))