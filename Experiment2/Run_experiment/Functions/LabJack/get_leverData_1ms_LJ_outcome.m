function get_leverData_1ms_LJ(event)

global leverData
global leverDataIndex
global status
global angle
global SkitSet
global t
global BRP
global visualBlock
%global zeroSpeed;


%Define the index to store the data in such a way that, if you go over the
%preallocated length, you start again from 1
leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;


%changeF=round(event.StreamTime*SkitSet.FrameRate);
%leverDataIndex=mod( leverDataIndex, SkitSet.leverDataPreallocateLength )+changeF;
%try

%FIRST argument has to be time stamp
leverData( leverDataIndex, 1: 3) = [event.TimeStamps event.Grip event.Angle];
leverData( leverDataIndex, 4 ) = 0;
t = leverData( leverDataIndex, 1 );

%take the average of last 6 frames (reduce jitter)
if leverDataIndex>5 && t>5*1/SkitSet.FrameRate && ~visualBlock
    angle = mean(leverData( leverDataIndex-5:leverDataIndex, 3 ));
    leverData(leverDataIndex,3)=angle;
elseif t>5*1/SkitSet.FrameRate &&~visualBlock
    angle = mean([leverData( SkitSet.leverDataPreallocateLength+leverDataIndex-5:SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]);
    %    angle = mean([leverData(mod(leverDataIndex-5, SkitSet.leverDataPreallocateLength):SkitSet.leverDataPreallocateLength, 3 ); leverData(1:leverDataIndex,3)]); %lets see if this works (if not use line above)
    leverData(leverDataIndex,3)=angle;
else
    angle=leverData(leverDataIndex,3);
end



if status.ball.grabbed == 0
    if leverData( leverDataIndex, 2 ) >= LJ_Handle.gripThreshold
        status.ball.grabbed = 1;
    end
end

if status.ball.grabbed == 1 && leverData( leverDataIndex, 2 ) <LJ_Handle.gripThreshold      % if ball was held and now is released
    status.ball.thrown = 1;
end

if status.ball.thrown == 1 && status.ball.flightPredicted == 0
    %calculate velocity by getting the angular difference over the last
    %(SkitSet.dataPointsForVelocity) datapoints (set to 6 initially)
    idx_start = leverDataIndex - SkitSet.dataPointsForVelocity;
    if idx_start < 1
        idx_start = mod( idx_start, SkitSet.leverDataPreallocateLength ) + 1;
    end
    
    %calculate distance traveled in m/sec. So we need length of circular
    %segment s. Wiki confirms that s = alpha/180 * pi * radius ->
    %distance in meters. This is exactly what this formula calculates.
    %Then, we need time and that's the second term: it calculates secs as
    %(Frames/secs)/frames needed to travel the section s
    v = pi * SkitSet.LeverLength * ( leverData( leverDataIndex, 3 ) - leverData( idx_start, 3 ) ) / 180 * SkitSet.FrameRate / SkitSet.dataPointsForVelocity;
        if length(v) == 0
        1+1
    end
    
    %Store Initial release parameters (i.e. the actual release parameters at t0)
    BRP.t0    = leverData( leverDataIndex, 1 );
    BRP.angle = leverData( leverDataIndex, 3 );
    BRP.v     = v;
    BRP.leverDataIndex = leverDataIndex;
    BRP.idx0 = leverDataIndex; %redundant but breaks the least in other functions
    
    BRP = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v, BRP.leverDataIndex, BRP);

    status.ball_released = 1;
    status.ball.flightPredicted = 1;
    
end
end