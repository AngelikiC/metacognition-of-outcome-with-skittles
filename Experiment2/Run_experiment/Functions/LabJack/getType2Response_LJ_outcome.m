function  resultData=getType2Response_LJ_outcome(resultData,trial,daq,feedback)

global win winCenter
global VASsettings
global SkitSet
global frameCount
global block;
global visualBlock;


startConfJudge = tic;
grip= daq.getGrip;

while grip<LJ_Handle.gripThreshold % wait for press
    if errorTrial()
        if visualBlock
            resultData.visual.type1(block).errorTrials(trial)=1;
        else
            resultData.type1(block).errorTrials(trial)=1;
        end
        break
    end
    [topEdge, lowEdge]  = drawConfidenceVAS_vertical;
    
    grip= daq.getGrip;
    angle=daq.getAngle;
    if angle>SkitSet.response2leverRange(2); angle = SkitSet.response2leverRange(2); end
    if angle<SkitSet.response2leverRange(1); angle =  SkitSet.response2leverRange(1);   end
    
    posY = lowEdge(end)-((angle - SkitSet.response2leverRange(1))/(SkitSet.response2leverRange(2)-SkitSet.response2leverRange(1))*(lowEdge(end)-topEdge(1)));
    
    Screen('DrawLine', win, 255 * [0 0 0], winCenter.x - VASsettings.height,posY,  winCenter.x + VASsettings.height, posY, 4);
    Screen('Flip', win);
    frameCount=frameCount+1;
end

while grip>LJ_Handle.gripThreshold % wait for release
    
    % if space is pressed-> error trial, abort
    if (resultData.type1(block).errorTrials(trial)==1 && ~visualBlock)
        break
    elseif visualBlock
        if resultData.visual.type1(block).errorTrials(trial);break;end
    elseif errorTrial()
        if visualBlock
            resultData.visual.type1(block).errorTrials(trial)=1;
        else
            resultData.type1(block).errorTrials(trial)=1;
        end
        break
    end
    
    [topEdge, lowEdge]  = drawConfidenceVAS_vertical;
    grip= daq.getGrip;
    angle=daq.getAngle;
    if angle>SkitSet.response2leverRange(2); angle = SkitSet.response2leverRange(2); end
    if angle<SkitSet.response2leverRange(1); angle =  SkitSet.response2leverRange(1);   end
    
    posY = lowEdge(end)-((angle - SkitSet.response2leverRange(1))/(SkitSet.response2leverRange(2)-SkitSet.response2leverRange(1))*(lowEdge(end)-topEdge(1)));
    
    Screen('DrawLine', win, 255 * [0 0 0], winCenter.x - VASsettings.height,posY,  winCenter.x + VASsettings.height, posY, 4);
    Screen('Flip', win);
    frameCount=frameCount+1;
end

%---------------
% Give feedback:
%---------------
if visualBlock
    if feedback
        if resultData.visual.type1(block).correct(trial)
            feedbackColor = 255 * [0 1 0];  %green
        else
            feedbackColor = 255 * [1 0 0];  %red
        end
        
        drawConfidenceVAS_vertical;
        
        %draw the cursor centered on the mouse y-position
        Screen('DrawLine', win, feedbackColor, winCenter.x - VASsettings.height, mouse.y, winCenter.x + VASsettings.height, mouse.y, 4);
        Screen('Flip',win);
        frameCount=frameCount+1;
        
        WaitSecs(.5);
    end
else
    if feedback
        if resultData.type1(block).correct(trial)
            feedbackColor = 255 * [0 1 0];  %green
        else
            feedbackColor = 255 * [1 0 0];  %red
        end
        
        drawConfidenceVAS_vertical;
        
        %draw the cursor centered on the mouse y-position
        Screen('DrawLine', win, feedbackColor, winCenter.x - VASsettings.height, mouse.y, winCenter.x + VASsettings.height, mouse.y, 4);
        Screen('Flip',win);
        frameCount=frameCount+1;
        
        WaitSecs(.5);
    end
end

if visualBlock
    resultData.visual.type2(block).RT(trial)        = toc(startConfJudge);
    resultData.visual.type2(block).mousePosY(trial) = posY;
    resultData.visual.type2(block).conf(trial)      = 100-(topEdge(1)-posY) * -100 / (lowEdge(end) - topEdge(1));
    %     elseif resultData.blindTrials(trial)
    %         resultData.motor.type2.block(block).RT(trial)
    %         resultData.motor.type2.block(block).mousePosX(trial) = posX;
    %         resultData.motor.type2.block(block).conf(trial)      = (posX - leftEdge(1)) * 100 / (rightEdge(end) - leftEdge(1));
else
    resultData.type2(block).RT(trial)        = toc(startConfJudge);
    resultData.type2(block).mousePosY(trial) = posY;
    resultData.type2(block).conf(trial)      = 100-(topEdge(1)-posY) * -100 / (lowEdge(end) - topEdge(1));
end
end
