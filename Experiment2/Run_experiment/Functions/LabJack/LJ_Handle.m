%% a handle designed for the LabJack T7 to read from AIN0-1
%everything concerning the communication is specified in this class:
%contains a simple read, data streaming and can notify listeners at
%fixed intervals
classdef LJ_Handle <handle
    properties
        
        nr; %the identifier
        %NumFound=0; %number of devices connected to the computer
        deviceType          = NaN; %not needed
        connectionType      = NaN; %not needded
        identifier          = NaN; %not needed
        %aSerialNumbers=0; %serial number of device
        %identifier=0;
        DeviceScanBacklog   = 0;
        LJMScanBacklog      = 0;
        
        
        errChecker          = 1; %turn errorchecking on(1) or off(0)
        lastError           = '';
        data                = [];
        lib                 = '/usr/local/lib/libLabJackM';
        header              = '/usr/local/include/LabJackM.h';
        
        %channel specification
        num_adresses        = 2;
        anglePin            = 'AIN2'; %specifies the analog input
        gripPin             = 'AIN0';
        %    timeRegister='CORE_TIMER'  %channel for time streaming
        %     bitPlus='STREAM_DATA_CAPTURE_16'; %also neccessary for time (stamp) streaming (streaming only in 16bit possible, but time is 32bit number)
        
        %stream specifications
        StreamRate          = 1000;
        scansPerRead        = 1;
        streamStarted       = false;
        resolutionIndex     = 8;
    end
    
    properties (Constant)
        range=10;
        gripThreshold=4.5;
    end
    


    
    %%
    methods
        %constructor
        function obj=LJ_Handle
         %  global debug;
         %   if ~isempty(debug)
         %     obj.errChecker=debug;
         %  end
            
            if not(libisloaded('libLabJackM'))
                warning('off','all');
                loadlibrary(obj.lib, obj.header);
                warning('on','all');
            end
            
            [err, obj.deviceType, obj.connectionType, obj.identifier, obj.nr] =  calllib('libLabJackM', 'LJM_OpenS', 'T7', 'USB','ANY',0);
            if obj.errChecker; errCheck(err); end;
            
            if obj.nr==0
                error('NO LabJack found. Connected? If yes try to reconnect the LabJack')
            end
            
            obj.streamStarted= obj.isStreaming;
            
            writeName(obj,'AIN_ALL_RESOLUTION_INDEX',obj.resolutionIndex);
            writeName(obj,'STREAM_SETTLING_US',200);
            
            %range from -1V to +1V (1) -0.1V to +0.1V (0.1) -10V to +10V (0)
            writeName(obj,'AIN_ALL_RANGE',0);
        end
        
        % returns 1 if a stream is enabled, 0 if not
        function bool=isStreaming(obj)
            bool=0;
            [err,~,bool]=calllib('libLabJackM','LJM_eReadName',obj.nr,'STREAM_ENABLE',bool);
            if obj.errChecker; errCheck(err); end;
        end
        
        % returns the angle of the lever (on the left side the pin has 0V, on the right 5V)
        function angle=getAngle(obj)
            [err,~,angle]=calllib('libLabJackM','LJM_eReadName',obj.nr,obj.anglePin,0);
            if obj.errChecker; errCheck(err); end;
            %   angle=angle* 10/LJ_Handle.range;
            angle=(angle-5)*180/5;
        end
        
        % analog to getAngle
        function grip=getGrip(obj)
            [err,~,grip]=calllib('libLabJackM','LJM_eReadName',obj.nr,obj.gripPin,0);
            if obj.errChecker; errCheck(err); end;
        end
        
        
        function writeName(obj,channel,value)
            [err, ~] = calllib('libLabJackM','LJM_eWriteName',obj.nr, channel, value);
            if obj.errChecker; errCheck(err); end;
        end
        
        
        function value = readName(obj,channel)
            [err,~,value]=calllib('libLabJackM','LJM_eReadName',obj.nr,channel,0);
            if obj.errChecker; errCheck(err); end;
        end
        %start a stream on the object (read from AIN1 and AIN0)
        function streamStart(obj)
            writeName(obj,'STREAM_SETTLING_US',200);
            if not(obj.streamStarted)
                [err, ~] =calllib('libLabJackM','LJM_WriteLibraryConfigS','LJM_STREAM_TRANSFERS_PER_SECOND', 999999999);
                if obj.errChecker; errCheck(err); end;
                
                [err,~,adresses,~]=calllib('libLabJackM','LJM_NamesToAddresses',obj.num_adresses,{obj.gripPin,obj.anglePin}, repmat(99,1,obj.num_adresses),repmat(99,1,obj.num_adresses));
                if obj.errChecker; errCheck(err); end;
                
                [err, ~, scanRateReal]= calllib('libLabJackM','LJM_eStreamStart',obj.nr,obj.scansPerRead,obj.num_adresses,adresses,obj.StreamRate);
                if obj.errChecker; errCheck(err); end;
                
                obj.streamStarted=true;
                
                if not(scanRateReal==obj.StreamRate)
                    disp('ATTENTION! STREAM RATE AT:')
                    disp(scanRateReal);
                end
                %prepare data array
                obj.data=zeros(obj.num_adresses,obj.scansPerRead);
            else
                disp('YOU ARE TRYING TO START A STREAM TWICE...')
            end
            
            %this loop is neccessary because sometime the bug appears that
            %angle and grip pin are exchanged
            
            validStream=0;
            while ~validStream
                [leverRead]=obj.streamRead;
              %  if obj.errChecker; errCheck(err); end;
                
                angle=leverRead(2,1); %changed
                
                validStream=~((angle<181.4 && angle > 179.6)|| angle > 345);
                if ~validStream; obj.streamStop; obj.streamStart;end;
                
            end
        end
        
        function streamStop(obj)
            if obj.streamStarted
                err=calllib('libLabJackM','LJM_eStreamStop',obj.nr);
                if obj.errChecker; errCheck(err); end;
                obj.streamStarted=false;
            else
                disp('NO STREAM TO STOP!')
            end
        end
        %read from stream at fixed rate (obj.Rate)
        function [data,LJMScanBacklog,DeviceScanBacklog]=streamRead(obj)
            %writeName(obj,'STREAM_SETTLING_US',200);
            %not really clear what the difference between LJMScanBacklog
            %and DeviceScanBacklog is
            [err, data, DeviceScanBacklog , LJMScanBacklog]= calllib('libLabJackM','LJM_eStreamRead',obj.nr,obj.data,obj.DeviceScanBacklog,obj.LJMScanBacklog);
            if obj.errChecker; errCheck(err); end
            
            data(2,:)=data(2,:)* 10/LJ_Handle.range;
            data(2,:)=(data(2,:)-5)*360/10;
            % set a limit to the lever movement at 0 and 180 deg 
            if data(2,:)<-90
              data(2,:)=180;
            end
            if data(2,:)<0
              data(2,:)=0;
            end
            
            %this returns the time-stamps for the stream at every single point
            %because the time is 32 bit and streaming is only possible for
            %16bit channels stream_data_capture_16 captures the second half
            %of the data. data(3,:) + 65536*data(4,:) returns the proper value.
            % 1//40000000.0 returns the time in sec.
            
            %data(3,:)=(data(3,:) + 65536*data(4,:))/40000000.0;
        end
        function close(obj)
            err= calllib('libLabJackM','LJM_Close',obj.nr);
            if obj.errChecker; errCheck(err); end;
        end
    end
end