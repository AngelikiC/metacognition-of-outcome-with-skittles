function getOptimalVdiff_outcome(targetHit_landscape, target_sizes)

global block
global trial
global visualBlock
global resultData
global BRP_alt BRP
global t_trials
global f_trials
%global zeroSpeed;

vdiff_min = 0.160;%0.01; % the minimum value that vdiff is allowed to take in the exploration of possible vdiffs (when the staricased value does not match the real one)
vdiff_max = 1.96;
stepSize_vdiff_getOptimal = .02; %the step size used in the 'exploration' of the closest vdiff that matches the same/different condition, but is not the staircassed one (check other values 0.3)
conditionCode = (resultData.block(block).condition(trial) == 'd') + 1;

%The first trajectory is given by BRP. Now get the second (alternative)
%trajectory with staircased differences in the relevant parameter of
%interest.

%     if t_trials
%         vdiff= SkitSet.t_vdiff; %in the test trials a fixed vdiff is used
%         if binornd(1,0.5); vdiff=-vdiff; end;  %in 50% negative
%     else


if visualBlock
    resultData.visual.movementData(block).RP_t0(trial) = BRP.t0;
    directionAlternative=resultData.visual.type1(block).directionAlternative(trial);
    whichStair=3;
else
    resultData.block(block).RP_t0(trial) = BRP.t0;
    whichStair=resultData.type1(block).whichStair(trial);
    directionAlternative=resultData.type1(block).directionAlternative(trial);
end


%%
% In Skittles_AoM the alternative trajectory is restricted in terms of two
% conditions:
% 1) same outcome (real and alternative trajectory hit the target or don't hit the target)
% 2) different outcome (one hits the target, the other does not)
% Therefore, for each throw we first choose the condition we want to be
% in (50/50) and then we choose an alternative trajectory that meets
% this condition dependent on the real trajectory.
% In a perfect world, the staircased vdiff leads to an alternative that
% meets our desired outcome.
% Otherwise, we want to change the staircased vdiff to the nearest
% vdiff that yields the desired outcome.
% Let's see if this works!
%
% In training trials, don't use same/different outome conditions
% In normal trials, check if real and alternative trajectory produce same outcome
% or not
% check for target hits BRP

targetHit = target_hit_outcome(BRP);
acc = 0.5;  % accuracy for rounding angle, in this case half an angle
speed_vector = 0:.05:6; % get speed vector used for producing the landscape.

if t_trials || f_trials
    vdiff   = resultData.stairs(whichStair).Signal(conditionCode) * directionAlternative ; % returns the staircased vdiff and asignes pseudoradom sign to it (+/-)
    BRP_alt = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
    check = NaN;
    direction_vdiff = 0;
    
    %Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
    BRP_alt.t0    = BRP.t0;
    BRP_alt.angle = BRP.angle;
    BRP_alt.v     = BRP.v+vdiff;
    
else
    vdiff   = resultData.stairs(whichStair).Signal(conditionCode) * directionAlternative ; % returns the staircased vdiff and asignes pseudorandom sign to it (+/-)
    
    BRP_alt_1 = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
    BRP_alt_2 = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v - vdiff, BRP.leverDataIndex, BRP_alt); % change sign of vdiff if condition not met--> it is no longer 50/50 then --> ok??
    
    targetHit_alt_1 = target_hit_outcome(BRP_alt_1);
    targetHit_alt_2 = target_hit_outcome(BRP_alt_2);
    
    
    % check conditions
    
    cond = resultData.block(block).condition(trial) ; % 50/50 condition 1 and 2, randomized
    
    if cond == 's'
        if targetHit == targetHit_alt_1 % desired outcome
            BRP_alt = BRP_alt_1;
            check = 1; % this can later on be used to check how often each possible way of getting the alternative traj was used
            direction_vdiff = 0;
            
%             Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
            BRP_alt.t0    = BRP.t0;
            BRP_alt.angle = BRP.angle;
            BRP_alt.v     = BRP.v+vdiff;
      
        elseif targetHit == targetHit_alt_2 % acceptable outcome (not the desired direction for the alternative)
            BRP_alt = BRP_alt_2;
            check = 2;
            direction_vdiff = 0;
            
%             Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
            BRP_alt.t0    = BRP.t0;
            BRP_alt.angle = BRP.angle;
            BRP_alt.v     = BRP.v-vdiff;
            
%             We effectively changed the value of the direction of the alternative
%             (eg from 1, which was set in the randomization to -1, which satisfies
%             both the same/different condition and the staricased value).
%             Hence, we find the next trial with the value we changed to,
%             and replace it with the opposite
            if resultData.type1(block).directionAlternative(trial) == 1
                resultData.type1(block).directionAlternative(trial) = -1; %if we have changed the direction from the original then this change should be saved
                nextTrialToSwapDirections = find(resultData.type1(block).directionAlternative(trial+1:end)== -1, 1);
                if ~isempty(nextTrialToSwapDirections)
                    resultData.type1(block).directionAlternative(nextTrialToSwapDirections) = 1;
                end
            elseif resultData.type1(block).directionAlternative(trial) == -1
                resultData.type1(block).directionAlternative(trial) = 1; %if we have changed the direction from the original then this change should be saved
                nextTrialToSwapDirections = find(resultData.type1(block).directionAlternative(trial+1:end)== 1, 1);
                if ~isempty(nextTrialToSwapDirections)
                    resultData.type1(block).directionAlternative(nextTrialToSwapDirections) = -1;
                end
            end
            
        else % real vdiff does not lead to desired outcome so get nearest vdiff that works
            BRP_alt_3 = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
            targetHit_alt_3 = target_hit_outcome(BRP_alt_3);
            
             if targetHit==1 % if the real trajectory hit the target
                 while (targetHit ~= targetHit_alt_3 && abs(vdiff) >= vdiff_min && abs(vdiff) <= vdiff_max)
                    if vdiff > 0
                        vdiff = vdiff - stepSize_vdiff_getOptimal;
                    elseif vdiff < 0
                        vdiff = vdiff + stepSize_vdiff_getOptimal;
                    end
                    BRP_alt_3 = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
                    targetHit_alt_3 = target_hit_outcome(BRP_alt_3);
                end
                
            else % if the real trajectory DOES NOT hit the target
                vdiff_toright = vdiff;
                vdiff_toleft = vdiff;
                
                while targetHit ~= targetHit_alt_3 && (abs(vdiff_toright) >= vdiff_min && abs(vdiff_toright) <= vdiff_max ||  abs(vdiff_toleft) >= vdiff_min && abs(vdiff_toleft) <= vdiff_max)
                    vdiff_toright = vdiff_toright -stepSize_vdiff_getOptimal;
                    vdiff_toleft = vdiff_toleft + stepSize_vdiff_getOptimal;
                    BRP_alt_to_right= calculate_release_parameters_outcome(BRP.t0,BRP.angle, BRP.v + vdiff_toright, BRP.leverDataIndex, BRP_alt);
                    targetHit_alt_to_right = target_hit_outcome(BRP_alt_to_right);
                    
                    BRP_alt_to_left= calculate_release_parameters_outcome(BRP.t0,BRP.angle, BRP.v + vdiff_toleft, BRP.leverDataIndex, BRP_alt);
                    targetHit_alt_to_left = target_hit_outcome(BRP_alt_to_left);
                    
                    if (targetHit_alt_to_right ~= targetHit && abs(vdiff_toright) >= vdiff_min && abs(vdiff_toright) <= vdiff_max)
                        targetHit_alt_3 = targetHit_alt_to_right;
                        BRP_alt_3 = BRP_alt_to_right; 
                        vdiff = vdiff_toright;
                    elseif (targetHit_alt_to_left ~= targetHit && abs(vdiff_toleft) >= vdiff_min && abs(vdiff_toleft) <= vdiff_max)
                        targetHit_alt_3 = targetHit_alt_to_left;
                        vdiff = vdiff_toleft;
                        BRP_alt_3 = BRP_alt_to_left;
                    end
                                       
                end
                
            
            
            end
            
            % now if the condition has changed
            if targetHit ~= targetHit_alt_3 %means the condition is no more 's'
                resultData.block(block).condition(trial) = 'd';
                nextTrialToSwap_trialtype =  find(resultData.block(block).condition(trial+1:end)== 'd',1);
                if ~isempty(nextTrialToSwap_trialtype)
                    resultData.block(block).condition(nextTrialToSwap_trialtype) = 's';
                end
            end
            
            BRP_alt = BRP_alt_3;
            
            if (targetHit == targetHit_alt_3 )
                check = 11; % this means that the whole loop above ended because the condition targetHit == targetHit_alt_3 was met
                
            elseif (targetHit ~= targetHit_alt_3)
                check = 12; %the while loop ended before the desired target_hit condition was met, but instead because the min vdiff was reached.this means that the condition changed and it is no more 's'
                BRP_alt = BRP_alt_1;
            end
            
            
            %Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
            BRP_alt.t0    = BRP.t0;
            BRP_alt.angle = BRP.angle;
            BRP_alt.v     = BRP.v+vdiff;
        end
            if (directionAlternative < 0 && vdiff < 0) || (directionAlternative > 0 && vdiff > 0)
                direction_vdiff = 's';
            elseif (directionAlternative < 0 && vdiff > 0) || (directionAlternative > 0 && vdiff < 0)
                direction_vdiff = 'd';
            else
                direction_vdiff = NaN;
            end
            
        
        
        
    elseif cond == 'd'
          if targetHit ~= targetHit_alt_1 % desired outcome
              BRP_alt = BRP_alt_1;
              check = 5;
              direction_vdiff = 0;
              
              %Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
              BRP_alt.t0    = BRP.t0;
              BRP_alt.angle = BRP.angle;
              BRP_alt.v     = BRP.v+vdiff;
              
          elseif targetHit ~= targetHit_alt_2 % acceptable outcome (not the desired direction for the alternative)
              BRP_alt = BRP_alt_2;
              check = 6;
              direction_vdiff = 0;
              
              %Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
              BRP_alt.t0    = BRP.t0;
              BRP_alt.angle = BRP.angle;
              BRP_alt.v     = BRP.v-vdiff;
              
              if resultData.type1(block).directionAlternative(trial) == 1
                  resultData.type1(block).directionAlternative(trial) = -1; %if we have changed the direction from the original then this change should be saved
                  nextTrialToSwapDirections = find(resultData.type1(block).directionAlternative(trial+1:end)== -1, 1);
                  if ~isempty(nextTrialToSwapDirections)
                      resultData.type1(block).directionAlternative(nextTrialToSwapDirections) = 1;
                  end
              elseif resultData.type1(block).directionAlternative(trial) == -1
                  resultData.type1(block).directionAlternative(trial) = 1; %if we have changed the direction from the original then this change should be saved
                  nextTrialToSwapDirections = find(resultData.type1(block).directionAlternative(trial+1:end)== 1, 1);
                  if ~isempty(nextTrialToSwapDirections)
                      resultData.type1(block).directionAlternative(nextTrialToSwapDirections) = -1;
                  end
              end
              
              
          else  %try to find other possible values and if not use only then the original value
              BRP_alt_4 = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
              targetHit_alt_4 = target_hit_outcome(BRP_alt_4);
              
              if targetHit==1 % if the real trajectory hit the target
                  
                  while (targetHit == targetHit_alt_4 && abs(vdiff) >= vdiff_min && abs(vdiff) <= vdiff_max)
                      if vdiff > 0
                          vdiff = vdiff + stepSize_vdiff_getOptimal;
                      elseif vdiff < 0
                          vdiff = vdiff - stepSize_vdiff_getOptimal;
                      end
                      
                      BRP_alt_4 = calculate_release_parameters_outcome(BRP.t0,BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt); %
                      targetHit_alt_4 = target_hit_outcome(BRP_alt_4);
                  end
              else  % if the real trajectory DOES NOT hit the target
                  
                  vdiff_toright = vdiff;
                  vdiff_toleft = vdiff;
                  
                  while targetHit == targetHit_alt_4 && (abs(vdiff_toright) >= vdiff_min && abs(vdiff_toright) <= vdiff_max ||  abs(vdiff_toleft) >= vdiff_min && abs(vdiff_toleft) <= vdiff_max)
                      vdiff_toright = vdiff_toright -stepSize_vdiff_getOptimal;
                      vdiff_toleft = vdiff_toleft + stepSize_vdiff_getOptimal;
                      BRP_alt_to_right= calculate_release_parameters_outcome(BRP.t0,BRP.angle, BRP.v + vdiff_toright, BRP.leverDataIndex, BRP_alt);
                      targetHit_alt_to_right = target_hit_outcome(BRP_alt_to_right);
                      
                      BRP_alt_to_left= calculate_release_parameters_outcome(BRP.t0,BRP.angle, BRP.v + vdiff_toleft, BRP.leverDataIndex, BRP_alt);
                      targetHit_alt_to_left = target_hit_outcome(BRP_alt_to_left);
                      
                      if (targetHit_alt_to_right ~= targetHit && abs(vdiff_toright) >= vdiff_min && abs(vdiff_toright) <= vdiff_max)
                          targetHit_alt_4 = targetHit_alt_to_right;
                          BRP_alt_4 = BRP_alt_to_right; 
                          vdiff = vdiff_toright;
                      elseif (targetHit_alt_to_left ~= targetHit && abs(vdiff_toleft) >= vdiff_min && abs(vdiff_toleft) <= vdiff_max)
                          targetHit_alt_4 = targetHit_alt_to_left;
                          vdiff = vdiff_toleft;
                          BRP_alt_4 = BRP_alt_to_left;
                      end
                  end
               end
              
              %%! i did this after elisa left as to chenge the condition similar to the trajectory in the previous step
       
              if targetHit == targetHit_alt_4 %means the condition is no more 'd'
                  resultData.block(block).condition(trial) = 's';
                  nextTrialToSwap_trialtype =  find(resultData.block(block).condition(trial+1:end)== 's',1);
                  if ~isempty(nextTrialToSwap_trialtype)
                      resultData.block(block).condition(nextTrialToSwap_trialtype) = 'd';
                  end
              end
              
              BRP_alt = BRP_alt_4;
              
              if (targetHit ~= targetHit_alt_4 )
                  check = 13; % this means that the whole loop above ended because the condition targetHit ~= targetHit_alt_4 was met
              elseif (targetHit == targetHit_alt_4)
                  check = 14; %the while loop ended before the desired target_hit condition was met, but instead because the min vdiff was reached.%this meand the condition changed from d to s
                  BRP_alt = BRP_alt_1;
              end
              
              
              %Store BRP_alt Initial 'release' parameters (i.e. the actual release parameters at t0)
              BRP_alt.t0    = BRP.t0;
              BRP_alt.angle = BRP.angle;
              BRP_alt.v     = BRP.v+vdiff;
          end
        
          if (directionAlternative < 0 && vdiff < 0) || (directionAlternative > 0 && vdiff > 0)
              direction_vdiff = 's';
          elseif (directionAlternative < 0 && vdiff > 0) || (directionAlternative > 0 && vdiff < 0)
              direction_vdiff = 'd';
          else
            direction_vdiff = NaN;
          end
    end
end


    
    % check for targethits BRP_alt
    
    targetHit_alt = target_hit_outcome(BRP_alt);
    
    
    %     % change vdiff to the other direction in the following cases:
    %     % (1) one trajectory hits the pole, but the other does not
    %     % (2) one trajectory goes to the left, the other to the right
    %     % (3) one trajectory hit the pole after crossing behind it, the other
    %     %     hits it directly
    %
    %     if (BRP.v> 0&& BRP.v+vdiff<0)||(BRP.v<0 && BRP.v+vdiff>0)
    %      vdiff= -vdiff;
    %      BRP_alt = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
    %     end
    %
    if (BRP.posthitBeforeCross && ~BRP_alt.posthitBeforeCross )|| (BRP_alt.posthitBeforeCross && ~BRP.posthitBeforeCross )
        if visualBlock
            resultData.visual.type1(block).unequalPosthit(trial)=1;
        else
            resultData.type1(block).unequalPosthit(trial)=1;
        end
    end
    if (BRP.v> 0 && BRP.v+vdiff<0)||(BRP.v<0 && BRP.v+vdiff>0)
        %(BRP_alt.posthit && ~BRP_alt.posthitAfterCross && BRP.posthitAfterCross)||(BRP.posthit && ~BRP.posthitAfterCross && BRP_alt.posthitAfterCross)
        %         vdiff= -vdiff;
        %
        %         % if we still have the same problem set v to 0
        %         if (BRP.v> 0&& BRP.v+vdiff<0)||(BRP.v<0 && BRP.v+vdiff>0)
        %             if abs(BRP.v)> abs(vdiff)/3 % to counter cases that could get very hard due to slow release speed, trajectories in both directions have to ba accepted
        %                 vdiff=-BRP.v;
        %                 zeroSpeed=1;
        %             else
        if visualBlock
            resultData.visual.type1(block).TrajectoriesDifferentDirections(trial)=1;
        else
            resultData.type1(block).TrajectoriesDifferentDirections(trial)=1;
        end
        %             end
        %         end
        %         BRP_alt = calculate_release_parameters_outcome(BRP.t0, BRP.angle, BRP.v + vdiff, BRP.leverDataIndex, BRP_alt);
    end
   % check
   
    vdiff_correct_sign = BRP.v-BRP_alt.v; %%! i added this to make sure vdiff has the correct sign  assigned to it
    
   %  fprintf('you did hit target= %i and computer hit target = %i and condition was: %s ',targetHit,targetHit_alt,cond) %%!delete
    if visualBlock %%if you need this you need to fix the vdiff
        resultData.visual.type1(block).vdiff(trial) = vdiff;
        resultData.visual.movementData(block).releaseSpeed(trial)= v;
        resultData.visual.movementData(block).releaseAngle(trial)= BRP.angle;
    else
        %resultData.type1(block).vdiff(trial)            = vdiff;
        resultData.type1(block).vdiff(trial)            = vdiff_correct_sign; %%check if now the vdiff has the correct sign
        resultData.type1(block).vdiff_staircase(trial)  = resultData.stairs(whichStair).Signal(conditionCode);
        resultData.type1(block).tSize(trial)            = resultData.stairs(2).Signal;
        resultData.type1(block).bSize(trial)            = resultData.stairs(3).Signal;
        resultData.type1(block).direction(trial)        = direction_vdiff;
        resultData.type1(block).targetHit(trial)        = targetHit;
        resultData.type1(block).targetHit_alt(trial)    = targetHit_alt;
        resultData.block(block).releaseSpeed(trial)     = BRP.v;
        resultData.block(block).releaseAngle(trial)     = BRP.angle;
        resultData.block(block).releaseSpeed_alt(trial) = BRP_alt.v;
        resultData.block(block).releaseAngle_alt(trial) = BRP_alt.angle;
        resultData.type1(block).check(trial)            = check;
    end
