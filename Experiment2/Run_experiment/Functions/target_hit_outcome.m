function targetHit = target_hit_outcome(BRP)

%input is an array of BRPs 
%output array of booleans that tell you whether you hit the target

SkitSet = set_SkitSet_outcome;

% Caro: changed the function, originally it was:
% targetHit(i)= ~isnan(BRP(i).x_collision) && (~(BRP(i).t_posthit<BRP(i).t_collision));
% now it accounts for instances where there is no posthit
% (~BRP(i).posthit))

targetHit =nan(1,length(BRP));
for i=1:length(BRP)
    targetHit(i)= ~isnan(BRP(i).x_collision) && ( (~BRP(i).posthit) || (BRP(i).t_posthit>BRP(i).t_collision)) && (BRP(i).t_collision < SkitSet.max_fligth_time) ;
end
end


