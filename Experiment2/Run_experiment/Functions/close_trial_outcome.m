function [resultData] = close_trial_outcome(trial, resultData)

global leverData;
global BRP BRP_alt;
global status;
global BCP;
global TRP;
global block;
global SkitSet

%Save data
%file_name = [subject_folder, '\Sub', num2str( sub, '%04d' ), 'Trial', num2str( current_trial, '%04d' ) ];
% 
% if BRP.idx0 < 599
%     tmp = leverData( [ mod( BRP.idx0 - 599 , 5000 ) : 5000, 1 : BRP.idx0 + 400 ], : );
% elseif BRP.idx0 > 4600
%     tmp = leverData( [ BRP.idx0 - 599 : 5000, 1 : mod( BRP.idx0 + 400, 5000 ) ], : );
% elseif BRP.idx0==599
%     tmp=leverData([5000,1:999],:);
% else
%     tmp = leverData( BRP.idx0 - 599 : BRP.idx0 + 400, : );
% end

before_release = 600;
after_release = 400;

if BRP.idx0 < (before_release-1)
    tmp = leverData( [ mod( BRP.idx0 - before_release-1 , SkitSet.leverDataPreallocateLength ) : SkitSet.leverDataPreallocateLength, 1 : BRP.idx0 + after_release ], : );
elseif BRP.idx0 > (SkitSet.leverDataPreallocateLength-after_release)
    tmp = leverData( [ BRP.idx0 - before_release-1 : SkitSet.leverDataPreallocateLength, 1 : mod( BRP.idx0 + after_release, SkitSet.leverDataPreallocateLength ) ], : );
elseif BRP.idx0==(before_release-1)
    tmp=leverData([SkitSet.leverDataPreallocateLength,1:(before_release+after_release-1)],:);
else
    tmp = leverData( BRP.idx0 - (before_release-1) : BRP.idx0 + after_release, : );
end


    resultData.block(block).movementData(trial).time       = tmp(:,1);            %TODO Not saving these - chack what heiko was using them for!
    resultData.block(block).movementData(trial).contact    = tmp(:,2);          % can be used for visual-only condition
    resultData.block(block).movementData(trial).angles     = tmp(:,3);
    resultData.block(block).movementData(trial).displayed  = tmp(:,4);
    resultData.block(block).movementData(trial).BRP        = BRP;
    
    %Store other stuff
    resultData.block(block).BRP(trial)     = BRP;
    resultData.block(block).BRP_alt(trial) = BRP_alt;
    resultData.block(block).BCP(trial)     = BCP;
    resultData.block(block).TRP(trial)     = TRP;
   

% Store extra movement information (during type 1 response)

before_release = 600;

if isnan(resultData.type1(block).RT(trial))
    after_release = (SkitSet.max_fligth_time*1000) + round(10*1000) + 100;
else
    after_release = (SkitSet.max_fligth_time*1000) + round(resultData.type1(block).RT(trial)*1000) + 100; % Ball flying time + RT of Type1 and 100ms buffer
end

if BRP.idx0 < (before_release-1)
    tmp = leverData( [ mod( BRP.idx0 - before_release-1 , SkitSet.leverDataPreallocateLength ) : SkitSet.leverDataPreallocateLength, 1 : BRP.idx0 + after_release ], : );
elseif BRP.idx0 > (SkitSet.leverDataPreallocateLength-after_release)
    tmp = leverData( [ BRP.idx0 - before_release-1 : SkitSet.leverDataPreallocateLength, 1 : mod( BRP.idx0 + after_release, SkitSet.leverDataPreallocateLength ) ], : );
elseif BRP.idx0==(before_release-1)
    tmp=leverData([SkitSet.leverDataPreallocateLength,1:(before_release+after_release-1)],:);
else
    tmp = leverData( BRP.idx0 - (before_release-1) : BRP.idx0 + after_release, : );
end

    resultData.block(block).movementDataExtra(trial).time       = tmp(:,1);            %TODO Not saving these - check what heiko was using them for!
    resultData.block(block).movementDataExtra(trial).contact    = tmp(:,2);          % can be used for visual-only condition
    resultData.block(block).movementDataExtra(trial).angles     = tmp(:,3);

%Reset variables
status  = set_status_outcome;
BRP     = set_BallReleaseParameters_outcome;
BRP_alt = set_BallReleaseParameters_outcome;
BCP     = set_BallCollisionParameters_outcome;
TRP     = set_TargetReleaseParameters_outcome;

