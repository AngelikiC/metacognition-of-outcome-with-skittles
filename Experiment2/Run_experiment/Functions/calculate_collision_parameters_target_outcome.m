function calculate_collision_parameters_target_outcome( t0, vx, vy )

global SkitSet;
global TRP;
global BCP;

TRP.t0 = t0;
TRP.x0 = SkitSet.xTarget;
TRP.y0 = SkitSet.yTarget;
TRP.vx = vx;
TRP.vy = vy;
Ex = SkitSet.Mass * vx^2 / 2 + SkitSet.xStiff * TRP.x0^2 / 2;
TRP.Ax = sqrt(2 * Ex / SkitSet.xStiff);
Ey = SkitSet.Mass * vy^2 / 2 + SkitSet.yStiff * TRP.y0^2 / 2;
TRP.Ay = sqrt( 2 * Ey / SkitSet.yStiff );
if ( TRP.Ax ~= 0 )
     a = TRP.x0 / TRP.Ax;
     TRP.phix = acos(a);
else
    TRP.phix = 0;
end
if ( vx > 0 ) 
    TRP.phix = -TRP.phix;
end
if ( TRP.Ay ~= 0 )
    a = TRP.y0 / TRP.Ay;
    TRP.phiy = acos( a ); 
else
    TRP.phiy = 0;
end
if ( vy > 0 )
    TRP.phiy = -TRP.phiy;
end

%Check for post hits
tmp = linspace(0, 2, 3000);
Damping = exp( -tmp * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
BallX = TRP.Ax .* Damping .* cos( SkitSet.xOmega .* tmp + TRP.phix );
BallY = TRP.Ay .* Damping .* cos( SkitSet.yOmega .* tmp + TRP.phiy );

%Check whether the centre post was hit
distance = sqrt( ( BallX - SkitSet.xCenter ).^2 + ( BallY - SkitSet.yCenter ).^2 );
idx_PostHit = find( distance <= ( SkitSet.CenterRadius + SkitSet.BallRadius ), 1, 'first' );
if idx_PostHit > 0
    TRP.posthit = 1;
    TRP.t_posthit = tmp( idx_PostHit );
    TRP.x_posthit = BallX( idx_PostHit );
    TRP.y_posthit = BallY( idx_PostHit );
else
    TRP.posthit = 0;
end