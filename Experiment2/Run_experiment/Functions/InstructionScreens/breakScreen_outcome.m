function [ breaks,pause_time ] = breakScreen_outcome( win,breaks,subjID,resultPath,f_trials)
%BREAKSCREEN display
global SkitSet;
global t_trials
global resultData;
global frameCount;
global debug
global block

breaks=breaks+1;
frameCount=frameCount+1;
if ~exist([resultPath filesep subjID filesep 'Savepoints'],'dir')
    mkdir([resultPath filesep subjID filesep 'Savepoints'])
end

if ~t_trials
    if f_trials
        save([resultPath filesep subjID filesep 'Savepoints' filesep 'SkittlesResultFeedback_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
    else
        save([resultPath filesep subjID filesep 'Savepoints' filesep 'SkittlesResult_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
    end
    % save(['./Results/SkittlesEverything_' subjName, '_' subjNum '.mat'])
else
    save([resultPath filesep subjID filesep 'Savepoints' filesep 'SkittlesTrainingResult_' subjID, '_break_' int2str(breaks) '.mat'], 'resultData')
end

message=sprintf('Break. Task block %d of 4 successfully completed. \n Well done! \n To continue, please press the spacebar.',block);

pauseTic=tic;
if debug;wrapat= 60;else;wrapat= 110;end
%Screen('DrawText',win,message,winCenter.x,winCenter.y);
DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat);
Screen( 'Flip', win, [], [], 0 );
KbWait(-1);
pause_time=toc(pauseTic);

end
