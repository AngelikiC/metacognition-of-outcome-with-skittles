function pause_time = visualInstruction_outcome(win)
%Visual task instructions display 
global SkitSet;
global debug
global frameCount

frameCount=frameCount+1;

message=['When you are ready, please press the spacebar to continue.'];
pauseTic=tic;

%Screen('DrawText',win,message,winCenter.x,winCenter.y);
if debug;wrapat= 60;else;wrapat= 110;end
DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat,0,0,2);
Screen( 'Flip', win, [], [], 0 );
KbWait(-1);
pause_time=toc(pauseTic);

end

