function [ x, y ] = getTrajectories_outcome( timeVector, BRP)

global SkitSet


t = timeVector;
Damping = exp( -t * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
x = BRP.Ax * Damping .* cos( SkitSet.xOmega * t + BRP.phix );
y = BRP.Ay * Damping .* cos( SkitSet.yOmega * t + BRP.phiy );


%     if ( sqrt( ( x - SkitSet.xCenter )^2 + ( y - SkitSet.yCenter )^2 ) < SkitSet.CenterRadius ) %Check for post hit
%         x = BRP.x_posthit;
%         y = BRP.y_posthit;
%         status.posthit = 1;
%     end


end