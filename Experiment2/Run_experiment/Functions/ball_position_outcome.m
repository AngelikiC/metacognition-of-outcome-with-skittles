function [ x, y, status ] = ball_position_outcome( angle, t, status )

%Checked against Heiko's last Skittles version (send 20.3.17). Some status
%and variables names are changed. The status structure is a bit more
%complex to point the difference between task, ball and sound statuses.
%Added comments. 

global BRP
global BCP
global SkitSet
global resultData

% replace SkitSet size of target and ball by staircased values

SkitSet.TargetRadius = resultData.stairs(2).Signal;
SkitSet.BallRadius   = resultData.stairs(3).Signal;


% If the ball has not been even grabbed yet, the ball rests next to the pole
if status.ball.grabbed == 0
    x = SkitSet.xCenter;
    y = SkitSet.yCenter - SkitSet.CenterRadius - SkitSet.BallRadius;
else
    % If the ball has not been released, ball position is simply lever position
    if status.ball.thrown == 0
        x = SkitSet.xLever - SkitSet.LeverLength * cos( angle * pi / 180 );
        y = SkitSet.yLever + SkitSet.LeverLength * sin( angle * pi / 180 );

    %If it has been released, ball position depends on time since throw:
    else
        t = t - BRP.t0;
        Damping = exp( -t * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
        x = BRP.Ax * Damping * cos( SkitSet.xOmega * t + BRP.phix );
        y = BRP.Ay * Damping * cos( SkitSet.yOmega * t + BRP.phiy );

        %and whether it collided with the post
        if ( sqrt( ( x - SkitSet.xCenter )^2 + ( y - SkitSet.yCenter )^2 ) < ( SkitSet.CenterRadius + SkitSet.BallRadius ) ) || status.ball.hitPost == 1 
            x = BRP.x_posthit;
            y = BRP.y_posthit;
            status.ball.hitPost = 1;
        end
        
        %or whether it collided with the target skittle
        if ( sqrt( ( x - SkitSet.xTarget )^2 + ( y - SkitSet.yTarget )^2 ) < ( SkitSet.TargetRadius + SkitSet.BallRadius ) ) || status.ball.hitTarget == 1 
            if status.ball.hitTarget == 0
              collision_outcome( t ); 
                status.ball.hitTarget = 1;
            end      
             
            %in which case it continues // commented to do not change
            %trajectory !!!
%             x = BCP.Ax * Damping * cos( SkitSet.xOmega * ( t - BRP.t_collision ) + BCP.phix );     
%             y = BCP.Ay * Damping * cos( SkitSet.yOmega * ( t - BRP.t_collision ) + BCP.phiy );

            if ( sqrt( ( x - SkitSet.xCenter )^2 + ( y - SkitSet.yCenter )^2 ) < ( SkitSet.CenterRadius + SkitSet.BallRadius ) ) || status.ball.posthit_after_collision == 1 %Check for post hit
                x = BCP.x_posthit;
                y = BCP.y_posthit;
                status.ball.posthit_after_collision = 1;
            end
        end  
        
    end
end
