function [status] = getType1Response_mouse_outcome(status, BRP, BRP_alt, trial,colorTraces)

global SkitSet
global GL
global win winCenter
global resultData
global visualBlock
global block
%global trial 

color(1,:) = [0.9 0.2 0.9];
color(2,:) = [0.1 0.8 0.1];
color_both = [0.75 0 0.75]; % use one color for both trajectories


text_y_offset = 200;%210; %%! adjust the position of the response letter keys to the position of levers
text_x_offset = 200;%250;


Screen( 'BeginOpenGL', win );
glClear;

% Draw lever
glPushMatrix;
glTranslatef( SkitSet.xLever, SkitSet.yLever, 0 );
glRotatef( -BRP.angle, 0.0, 0.0, 1.0 ); %%! i changed this
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 1.0 1 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.0 0.0 1.0 1 ] );
glRectf( -SkitSet.LeverLength, -SkitSet.LeverWidth / 2, 0, SkitSet.LeverWidth / 2 );
glPopMatrix;

%Draw target
if status.ball.thrown
    glPushMatrix;
    [ x, y, status ] = target_position_type1(  status );
    glTranslatef( x, y, 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
    glutSolidSphere( resultData.stairs(2).Signal , 100, 100 );
    glPopMatrix;
end

% Draw center post
% Draw center post and adapt its color to the blind trial color
% if resultData.block(block).blindTrials(trial)&&~visualBlock
%     coneColor=SkitSet.coneColor(SkitSet.blindTrialConeColor,:);
% else
%     coneColor=SkitSet.coneColor(mod(SkitSet.blindTrialConeColor,size(SkitSet.coneColor,1))+1,:);
% end
glPushMatrix;
glTranslatef( SkitSet.xCenter, SkitSet.yCenter, 0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [0 0 1 1] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [0 0 1 1] );
glutSolidCone( SkitSet.CenterRadius, 0.5, 100, 100 );
glPopMatrix;

% Get and draw actual ball trajectory vector
crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
crossBeforePost.index_x = Inf;

if isnan(BRP.posthit) || BRP.posthit == 0||BRP.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
    crossBehindPost = BRP.crossBehindPost;
else
    [~, crossBeforePost.index_x] = min(abs(BRP.x_posthit-BRP.trajectory.x));
    [~, crossBeforePost.index_y] = min(abs(BRP.y_posthit-BRP.trajectory.y));
    
end
firstVerticalCrossing = min(crossBehindPost, crossBeforePost.index_x);

% same procedure for alternative trajectory
crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
crossBeforePost.index_x = Inf;

if isnan(BRP_alt.posthit) || BRP_alt.posthit == 0||BRP_alt.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
    crossBehindPost = BRP_alt.crossBehindPost;
else
    [~, crossBeforePost.index_x] = min(abs(BRP_alt.x_posthit-BRP_alt.trajectory.x));
    [~, crossBeforePost.index_y] = min(abs(BRP_alt.y_posthit-BRP_alt.trajectory.y));
    
end
firstVerticalCrossing_alt = min(crossBehindPost, crossBeforePost.index_x);

% Real trajectory
for trajectoryPoint = 1 : 10 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 10 points
    glPushMatrix;
    glTranslatef( BRP.trajectory.x(trajectoryPoint), BRP.trajectory.y(trajectoryPoint), 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_both );
    glutSolidSphere( 0.01, 100, 100 );
    glPopMatrix;
end

% Get and draw alternative ball trajectory vector
for trajectoryPoint = 1 : 10 : firstVerticalCrossing_alt  %plot trajectory every 10 points
    glPushMatrix;
    glTranslatef( BRP_alt.trajectory.x(trajectoryPoint), BRP_alt.trajectory.y(trajectoryPoint), 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_both );
    glutSolidSphere( 0.01, 100, 100 );
    glPopMatrix;
end

Screen( 'EndOpenGL', win );
%fixed mapping between letter and colour. We could add stickers to keyboard
%try to make thm have the same color
Screen('DrawText', win, SkitSet.responseKeyNames(1), winCenter.x-text_x_offset, winCenter.y+text_y_offset, 255*color_both); % +620 in y if not centered
Screen('DrawText', win, SkitSet.responseKeyNames(2), winCenter.x+text_x_offset-15, winCenter.y+text_y_offset, 255*color_both); % +620

displayTime_2ACF = Screen( 'Flip', win, [], [], 0 );

keyCode=1;
while ~(find(keyCode)==SkitSet.responseKeys(1) || find(keyCode)==SkitSet.responseKeys(2))
    [RTsecs, keyCode]                   = KbPressWait;

   
    
%store answer
if visualBlock
    resultData.visual.type1(block).response{trial}    = KbName(keyCode);
    resultData.visual.type1(block).chosenColor(trial) = find(find(keyCode) == SkitSet.responseKeys);
    resultData.visual.type1(block).RT(trial)           = RTsecs - displayTime_2ACF;
    if resultData.visual.type1(block).chosenColor(trial) == colorTraces.actualTrace(trial)
        resultData.visual.type1(block).correct(trial)  = 1;
    elseif resultData.visual.type1(block).chosenColor(trial) == colorTraces.alternativeTrace(trial)
        resultData.visual.type1(block).correct(trial)  = 0;
    else
        resultData.visual.type1(block).correct(trial)  = NaN;
    end
else
    resultData.type1(block).response{trial}    = KbName(keyCode);
    resultData.type1(block).chosenColor(trial) = find(find(keyCode) == SkitSet.responseKeys);
    %fprintf('%i',resultData.type1(block).chosenColor(trial))
    resultData.type1(block).RT(trial)           = RTsecs - displayTime_2ACF;
    if resultData.type1(block).chosenColor(trial) == colorTraces.actualTrace(trial)
        resultData.type1(block).correct(trial)  = 1;
    elseif resultData.type1(block).chosenColor(trial) == colorTraces.alternativeTrace(trial)
        resultData.type1(block).correct(trial)  = 0;
    else
        resultData.type1(block).correct(trial)  = NaN;
    end
end
end