function [topEdge, lowEdge] = drawConfidenceVAS_vertical_outcome

global winCenter win
global VASsettings;
global SkitSet

centreX = winCenter.x;

%Bands should be 4 x n matrices with the rows corresponding to: left
%edge, top edge, right edge, bottom edge.
topEdge   = winCenter.y + ( (0:(VASsettings.nBands -1)) - (VASsettings.nBands/2) ) * (VASsettings.bandWidth);
lowEdge   = winCenter.y + ( (1:(VASsettings.nBands   )) - (VASsettings.nBands/2) ) * (VASsettings.bandWidth);
leftEdge  = repmat(centreX - VASsettings.height/2, 1, VASsettings.nBands);
rightEdge = repmat(centreX + VASsettings.height/2, 1, VASsettings.nBands);

bands  = [leftEdge; lowEdge; rightEdge; topEdge];
colors = repmat([VASsettings.colorDark' VASsettings.colorLight'], 1, VASsettings.nBands/2);


Screen('TextSize', win, 24);
Screen('FillRect', win, colors, bands);
TextWidth = Screen('TextBounds', win, VASsettings.Question);
DrawFormattedText(win, VASsettings.Question, centreX - (TextWidth(3)/2), min(topEdge) - 80, SkitSet.textColor);
TextWidth = Screen('TextBounds', win, VASsettings.labels{2});
DrawFormattedText(win, VASsettings.labels{2}, centreX - (TextWidth(3)/2), min(topEdge) - TextWidth(4), SkitSet.textColor);
TextWidth = Screen('TextBounds', win, VASsettings.labels{1});
DrawFormattedText(win, VASsettings.labels{1},  centreX - (TextWidth(3)/2), max(lowEdge) + TextWidth(4), SkitSet.textColor);

end