 function [status,correct,leftTrajectorySelected]=drawTrajectories_outcome(startAngle, status, BRP, BRP_alt, colorTraces,color,angleNow)
global GL
global win
global SkitSet
% global trial
global resultData
global frameCount;
% global visualBlock;
% global block;
% global leverData
% global leverDataIndex
% global useDaq   
%         angleNow=dataTotal(2,end);
        color_both = [0.75 0 0.75]; % use one color for both trajectories

        Screen( 'BeginOpenGL', win );
        glClear;
        
        % Draw lever
        glPushMatrix;
        glTranslatef( SkitSet.xLever, SkitSet.yLever, 0 );
        glRotatef( -angleNow, 0.0, 0.0, 1.0 );
        glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 1.0 1 ] );
        glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.0 0.0 1.0 1 ] );
        glRectf( -SkitSet.LeverLength, -SkitSet.LeverWidth / 2, 0, SkitSet.LeverWidth / 2 );
        glPopMatrix;
        
        % Draw target
                glPushMatrix;
                [ x, y, status ] = target_position_type1( status );
                glTranslatef( x, y, 0 );
                glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
                glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
                glutSolidSphere( resultData.stairs(2).Signal , 100, 100 );
                glPopMatrix;
        
        % Draw center post and adapt its color to the blind trial color
        %         if resultData.block(block).blindTrials(trial)&&~visualBlock
        %             coneColor=SkitSet.coneColor(SkitSet.blindTrialConeColor,:);
        %         else
        %             coneColor=SkitSet.coneColor(mod(SkitSet.blindTrialConeColor,size(SkitSet.coneColor,1))+1,:);
        %         end
        
        
        glPushMatrix;
        glTranslatef( SkitSet.xCenter, SkitSet.yCenter, 0 );
        glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [0 0 1 1] );
        glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [0 0 1 1] );
        glutSolidCone( SkitSet.CenterRadius, 0.5, 100, 100 );
        glPopMatrix;
        
        % Get and draw actual ball trajectory vector
        crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
        crossBeforePost.index_x = Inf;
        
        if isnan(BRP.posthit) || BRP.posthit == 0||BRP.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
            crossBehindPost = BRP.crossBehindPost;
        else
            [~, crossBeforePost.index_x] = min(abs(BRP.x_posthit-BRP.trajectory.x));
            [~, crossBeforePost.index_y] = min(abs(BRP.y_posthit-BRP.trajectory.y));
            
        end
        firstVerticalCrossing = min(crossBehindPost, crossBeforePost.index_x);
        
        % same procedure for alternative trajectory
        crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
        crossBeforePost.index_x = Inf;
        
        if isnan(BRP_alt.posthit) || BRP_alt.posthit == 0||BRP_alt.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
            crossBehindPost = BRP_alt.crossBehindPost;
        else
            [~, crossBeforePost.index_x] = min(abs(BRP_alt.x_posthit-BRP_alt.trajectory.x));
            [~, crossBeforePost.index_y] = min(abs(BRP_alt.y_posthit-BRP_alt.trajectory.y));
            
        end
        firstVerticalCrossing_alt = min(crossBehindPost, crossBeforePost.index_x);
        
        %BRPleft = BRP.trajectory.x(5)-BRP_alt.trajectory.x(5)<0; %is the original trajectory on the left side at random point 5?
        BRPleft = BRP.v	-BRP_alt.v <0; %%! i changed this according to Marika's comment on github as to BRPleft to be 1 when left 
        
        % this is to determine witch trajectory is right and witch left.
        % Therefore the x-axis location of a random point is chosen and
        % compared if it is left of the alternativa trajectory
        
        %this is a new idea: the selection of the trajectories starts at
        %the first lever position. Then the selection depends on how far
        %the lever is moved left or right and changes every e.g. 10??? (specified in SkitSet.response1Selevtion)
        
        leftTrajectorySelected = sum(angleNow>startAngle+SkitSet.response1Selection*[-7:2:7] & angleNow<startAngle+SkitSet.response1Selection*[-6:2:8])>=1; %selection of the left trajectory

        % show the selected trajectory in bold
        if (leftTrajectorySelected && BRPleft) || (not(leftTrajectorySelected) && not(BRPleft))
            BRPBallSize=  0.016;
            BRP_alt_BallSize=  0.01;
            correct= true;
            
        else
            BRPBallSize=  0.01;
            BRP_alt_BallSize=  0.016;
            correct= false;
        end
        
        
        %reall trajectory
        for trajectoryPoint = 1 : 10 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 10 points
            glPushMatrix;
            glTranslatef( BRP.trajectory.x(trajectoryPoint), BRP.trajectory.y(trajectoryPoint), 0 );
            glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
            glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_both);
            glutSolidSphere( BRPBallSize, 100, 100 );
            glPopMatrix;
        end
        
        % Get and draw alternative ball trajectory vector
        %         if BRP_alt.posthit
        %             posthit_alt=round(BRP_alt.t_posthit*SkitSet.FrameRate);
        %         end
        for trajectoryPoint = 1 : 10 : firstVerticalCrossing_alt % min([posthit_alt,firstVerticalCrossing])  %plot trajectory every 10 points
            glPushMatrix;
            glTranslatef( BRP_alt.trajectory.x(trajectoryPoint), BRP_alt.trajectory.y(trajectoryPoint), 0 );
            glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
            glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_both);
            glutSolidSphere( BRP_alt_BallSize, 100, 100 );
            glPopMatrix;
        end
        Screen( 'EndOpenGL', win );
        
        Screen( 'Flip', win, [], [], 0 );
        frameCount=frameCount+1;

    end