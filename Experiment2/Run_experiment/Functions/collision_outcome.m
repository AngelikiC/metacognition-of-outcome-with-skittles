function collision_outcome( t )
global BRP
global SkitSet

%Checked against Heiko's last version (sent 20.3.17) 

angle = atan2( ( BRP.y_collision - SkitSet.yTarget ), ( BRP.x_collision - SkitSet.xTarget ) );
Damping = exp( -t * SkitSet.Damping / ( 2 * SkitSet.Mass ) );
vx = -BRP.Ax * Damping * sin( SkitSet.xOmega * t + BRP.phix ) * SkitSet.xOmega;
vy = -BRP.Ay * Damping * sin( SkitSet.yOmega * t + BRP.phiy ) * SkitSet.yOmega;

vb_rad  = vx * cos( -angle ) - vy * sin( -angle );  %rotate reference frame to have collision along the axis
vb_tang = vx * sin( -angle ) + vy * cos( -angle );	%rotate by -angle 

%Velocities_After_Collision(vb_rad,0,1,1,0.9,&ball_rad,&target_rad);

v2 = -vb_rad;

if ( v2 < 0 )
	v2 = -v2;	%velocity is always positive
	sign = -1;  %but remember if the sign needed to be changed
else
    sign = 1;
end

Q = v2^2 * 0.05;
P = v2;
radiant = P * P / 4 - Q;
solv2 = P / 2 - sqrt( radiant );
solv1 = v2 - solv2;
if solv2 > solv1    %the second object cannot be faster than the first one in the chosen frame of reference
	solv2 = P / 2 -sqrt( radiant );
	solv1 = v2 - solv2;
end

v2_new = sign * solv2 + vb_rad;  %switch back to the original frame of reference
v1_new = sign * solv1 + vb_rad;

vx = v1_new * cos( angle ) - vb_tang * sin( angle );	%transform back , rotation by angle and shift by vt
vy = v1_new * sin( angle ) + vb_tang * cos( angle );	%dito

vtx = v2_new * cos( angle );	%transform back, the tangential component was 0
vty = v2_new * sin( angle );

 calculate_collision_parameters_outcome( BRP.t0 + BRP.t_collision, vx, vy );
 calculate_collision_parameters_target_outcome( BRP.t0 + BRP.t_collision, vtx, vty );
