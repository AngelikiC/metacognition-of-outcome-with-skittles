
%% SkittlesMatlab
% designed for LabJack devices (LJ)
% streaming the input (S)

clc
%% set globals
global useSound useDaq;
global SkitSet;
global status;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)
global BCP;                         % ball collision parameters
global TRP;                         % target release parameters 
global VASsettings;                 % settingd for confidence scale
global leverData leverDataIndex;    % LeverDataIndex says where in the leverData array the datapoint is to be stored
global GL;                          %includes the openGL drawing ..stuff for PTB
global win winCenter winRect;       %%!i added winRect  %window handle for PTB (necessary for e.g. showMovingBall)
global session;                     % daq session
global trial;
global resultData;
global t_trials;
global f_trials;
global c_trials;
global startingblock;
global debug;
global frameCount;
global visualBlock;
global block;

%global zeroSpeed
% TODO unequal posthit mouse
%% 
tic % for measuring how long it takes to do the task

%% user
user = 'angeliki';
%user = 'angeliki';
if  strcmp(user,'angeliki')          %% Compare strings or character vectors
    homePath = ['/Users/Angeliki/Seafile/My Library/Angeliki/Skittles_OUTCOME/Run_experiment/']
    %homePath = ['S:' filesep 'My Libraries' filesep 'My Library' filesep 'Angeliki' filesep 'Skittles_OUTCOME' filesep 'Run_experiment'];
    addpath(genpath(homePath))       %% i generate and subsequent add all folders with scripts etc
    resultPath = [homePath filesep 'Results_Outcome'];
    
elseif strcmp(user,'angeliki_lab')
    homePath = [ filesep 'Users' filesep 'metamotorlab' filesep 'Angeliki_lab' filesep 'Seafile' filesep 'My Library' filesep 'Angeliki' filesep 'Skittles_OUTCOME' filesep 'Run_experiment'];
    addpath(genpath(homePath))       %% i generate and subsequent add all folders with scripts etc
    resultPath = [homePath filesep 'Results_Outcome'];
end
cd(homePath)
commandwindow;

%% flags
debug               = 1;       % chANGE TO 1 FOR TESTING
useDaq              = 1;       % use lever through data acquisition toolbox (1) or keyboard and mouse (0)
useSound            = 0;    

%% Specify number of trials 
breaksBetweenBlocks = 1;       % 1 yes 0 no
trials_training          = 40;  % 56;  % even numbers (1 block)
trials_feedback          = 8;  % divided by 8 (1 block)
trials_per_block         = 136; %or 153 
blocks                   = 4;


%% Subject settings %%! i changed it to proceed only when an answer is given
mode  = '' ;
subjID = '' ;
while ~(strcmp(mode,'f')||strcmp(mode,'n')||strcmp(mode,'t'))
    mode  = input('Training (t), Feedback (f), normal (n) : ','s');%Test trials with no saved data
end
while  strcmpi(subjID,'')
    subjID= input('Participants ID: ','s');
end

%% Organise per type of trial
if mode=='t'
    t_trials=1;
    c_trials = 0;
    f_trials=0;
    startingblock=1;
    while exist([resultPath filesep subjID filesep 'SkittlesTrainingResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials = trials_training;
    blocks=1;
    feedback    = 0;
    disp([int2str(n_trials*blocks) ' TRAINING trials are beginning'])
elseif  mode=='n'
    t_trials=0;
    f_trials=0;
    startingblock=1;
    while exist([resultPath filesep subjID filesep 'SkittlesResult_' subjID,'.mat'],'file')
        subjID_candidate = input('Participants ID already taken. Start anyways (enter s) or different ID (enter an ID): ','s');
        if strcmp(subjID_candidate, 's')
            startingblock = 0;
            while ~ismember(startingblock,1:blocks)
                startingblock = input('From which block do you want to start? : ');
            end
            break
        else
            subjID = subjID_candidate;
        end
    end
    n_trials=trials_per_block;
    disp([int2str(n_trials*(blocks+1-startingblock)) ' trials are beginning'])
    feedback    = 0;
elseif  mode=='f'
    t_trials=0;
    f_trials=1;
    startingblock=1;
    feedback    = 1;
    blocks=1;
    while exist([resultPath filesep 'SkittlesFeedbackResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials=trials_feedback;
    disp([int2str(n_trials*blocks) ' FEEDBACK trials are beginning'])
    
end

if isempty(subjID)
    subjID = 'test';
end

if ~exist([resultPath filesep subjID],'dir')
    mkdir([resultPath filesep subjID])
end


%% Skittles settings

if startingblock==1 % experiment is started regularly (with the first block)
    
    SkitSet             = set_SkitSet_outcome;                  % Set up the whole Skittles scene
    status              = set_status_outcome;                   % a primitive state machine
    BRP                 = set_BallReleaseParameters_outcome;    % ball release (and result) parameters
    BRP_alt             = set_BallReleaseParameters_outcome;    % alternative BRP has the same structure
    BCP                 = set_BallCollisionParameters_outcome;
    TRP                 = set_TargetReleaseParameters_outcome;
    resultData          = set_resultData_outcome;               % do I need this global?
    resultData.SkitSet  = SkitSet;
    

    % Pseudorandomize combinations of direction alternative, initially selected trajectory and motor/visuomotor trials
    % 1st column: direction alternatives (1 = +vdiff, -1 = -vdiff)
    % 2nd column: initially selected trajectory (1 = left, 0 = right)
    % 3rd column: condition (0 = visuomotor, 1 = motor)

    combinations = [-1 0 0; 1 0 0; -1 1 0; 1 1 0;   1 1 1; 1 0 1; -1 1 1; -1 0 1]; % all possible combinations
    combinations = repmat(combinations,n_trials/8,1); % bring it to right lengths

    for b=1:blocks
        % alternative randomly to the left or right (-1 or 1)
        combinations = combinations(randperm(size(combinations,1)),:); % shuffle
        %resultData.type1(b).directionAlternative = combinations(:,1); %%! the alternative trajectory can be either to left or right 
        resultData.type1(b).directionAlternative = combinations(:,1);
       
        resultData.type1(b).initialTrajSelection = combinations(:,2);

        resultData.type1(b).TrajectoriesDifferentDirections = zeros(1,n_trials);
        %resultData.visual.type1(b).TrajectoriesDifferentDirections=zeros(1,n_trials/2);

        resultData.type1(b).unequalPosthit=zeros(1,n_trials);
        %resultData.visual.type1(b).unequalPosthit=zeros(1,n_trials/2);
        sca
        
        %Randomize also the colour of the two alternative trajectories
        colorTraces.actualTrace      = repmat([1;2],n_trials/2,1);
        randomOrder                  = randperm(n_trials); % randperm(N) returns a vector containing a random permutation of the integers 1:N
        colorTraces.actualTrace      = colorTraces.actualTrace(randomOrder);
        
        if useDaq ==1
            colorTraces.alternativeTrace = colorTraces.actualTrace; %3 - colorTraces.actualTrace; same color for actual and alternative trace -> try it out
        else
            colorTraces.alternativeTrace = 3 - colorTraces.actualTrace;%%! use this when using keyboard for data aquisition
        
        end
        resultData.type1(b).colorTraces = colorTraces;
    end
    
else % experiment is started at a later block, e.g. because it previously crashed
    load([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'],'SkitSet');
    load([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'],'resultData');
    status              = set_status_outcome;                   % a primitive state machine
    BRP                 = set_BallReleaseParameters_outcome;    % ball release (and result) parameters
    BRP_alt             = set_BallReleaseParameters_outcome;    % alternative BRP has the same structure
    BCP                 = set_BallCollisionParameters_outcome;
    TRP                 = set_TargetReleaseParameters_outcome;
end


%% Here, arrays for coding blind trials and the conditon of AoM are introduced. Right now, I don't use blindtrials, but this can be changed

if startingblock==1 % experiment is started regularly (with the first block)
    for b=1:blocks
        if t_trials
            resultData.block(b).blindTrials         = zeros(1,n_trials);
            resultData.block(b).condition           = zeros(1,n_trials);
        else
            %resultData.type1.blindTrials           = [zeros(1,n_trials/2),ones(1,n_trials/2)]; %array indicates the blind trials(no ball displayed); should have the same length as n_trials (perhaps n_trials is not neccessary)
            %blindTrials                            = repmat([0,1],1,n_trials/2);
            %resultData.block(b).blindTrials        = blindTrials(randperm(n_trials));
            %resultData.visual.type1(b).errorTrials = zeros(1,n_trials-sum(blindTrials));
            %% I don't use motor trials therefore I do not use the third colmun of the combination array. However, just replace the zeros with the combination array and it should work for motor and visuomotor trials
            resultData.block(b).blindTrials         = zeros(1,n_trials); % combinations(:,3);
            condition                               = repmat(['s','d'],1,n_trials/2); % s = same outcome, d = different outcome
            resultData.block(b).condition           = condition(randperm(n_trials));
        end
        resultData.type1(b).errorTrials = zeros(1,n_trials);%trials that are marked during type2 task as wrong selection (space)
    end
end


%% initialize variables/arrays
%resultData.targetPos= repmat([0 1 1 0],1,n_trials/4); % target position

if startingblock==1 % experiment is started regularly (with the first block)
    resultData.useDaq = useDaq;
    trial             = 1;                        %Initialize
    breaks            = 0;
    visualBlock       = 0;
    
    % color of cone in blind trials randomly determined
    if binornd(1,0.5)
        resultData.blindTrialColor = 1;
        SkitSet.blindTrialConeColor= 1;
    else
        resultData.blindTrialColor = 2;
        SkitSet.blindTrialConeColor= 2;
    end

else % experiment is started at a later block, e.g. because it previously crashed
    resultData.useDaq = useDaq;
    trial             = 1;                        %Initialize
    load([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'],'breaks');
    load([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'],'visualBlock');
end

% load landscape data

load([resultPath filesep 'tpx0.4_tpy0.5.mat'], 'targetHit_landscape', 'target_sizes');

%% Psychtoolbox settings

KbName('UnifyKeyNames');
whichScreen     = 0; %angeliki laptop uses : 2; %%! i made it 2 it was 0
RestrictKeysForKbCheck(SkitSet.responseKeys);
bg              = 0.65;
grey            = round(255*[bg bg bg]);
SkitSet.bg_color= grey;


if ~useDaq
    elbowYcoordsPixels = 729; %929 if winRect 0 0 1000 1000; %529 if winRect 0 0 600 600
end

if debug
    Screen('Preference', 'SkipSyncTests', 1); % 1 or 2->without testing
    %Screen('Preference', 'ConserveVRAM', 64); % ONLY FOR WINDOWS
    debuggingRect = [0 0 800 800];
    %Find (by hand) to the y pixel (in PTB coordinates) of the lever elbow
    %Will change if size of PTB's windowRect changes
else
    Screen('Preference', 'SkipSyncTests', 0); %%! it was 0
    %  Screen('Preference', 'SkipSyncTests', 1)
    debuggingRect = [];
    %  Screen('Preference', 'SkipSyncTests', 1);   %TODO make psychtoolbox work!!!!
end


%% Initialize and pseudorandomize parameters(s) for the type1 question

% speed difference
vDiff_max = 1.96;

% target size difference
% the difference between minimal and maximal size has to be divisible by
% the stepsize
tStepsize = .003; 
tSize_min = .032;
tSize_max = 0.134;

% ball size difference
bStepsize = .003;
bSize_min = .032;
bSize_max = 0.134;

if ~t_trials
    if exist([resultPath filesep subjID filesep 'SkittlesTrainingResult_' subjID,'.mat'],'file')
        resultTraining=load([resultPath filesep subjID filesep 'SkittlesTrainingResult_' subjID,'.mat']);
        vDiff_start=abs(resultTraining.resultData.type1(end).vdiff(end));
        tSize_start=resultTraining.resultData.type1(end).tSize(end);
        %traj_start = ;
    else
        warning('No training data found!! Set staircase start to default. Hit space to continue.')
        KbWait(-1);
        vDiff_start = .76;
        tSize_start = .05;
        bSize_start = .05;
    end

    % step size for staircases that run during the normal trials
    % speedDiff
    vStepsize = .05;
    vDiff_min = .16; %it was .01 but after some testin 0.160 was the smallest acceptable value

else
    % step size in training trials (bigger to approach the real vdiff faster)
    vStepsize   = .15;
    vDiff_start = .96;
    vDiff_min   = .21; 
    %resultData.type1.v_diff=SkitSet.t_vdiff;

    % target/ball size start
    tSize_start = .05;
    bSize_start = .05;
end

if startingblock==1 % If experiment is started regularly (with the first block). Otherwise, continues to use the loaded staircase.
    
    % Whichstair stores the condition of the staircase. Before adding 1, it has
    % 1 for motor trials and 0 for visuomotor trials. We add one to have 1 for
    % visuomotor trials and 2 for motor trials in the array. This is better
    % because in the analysis we loop over it to get the right staircase and
    % looping from 1: something is better than from 0.
    for b=1:blocks
        resultData.type1(b).whichStair = resultData.block(b).blindTrials+1; %use one stairCase for blind trials the other one for non-blind trials
    end
    
    % This sets up the staircase for vdiff and
    % target size
   % stairs(1) = staircaseSetup(1, vDiff_start, [vDiff_min vDiff_max], [2 1]);
    stairs(1) = staircaseSetup(2, vDiff_start, [vDiff_min vDiff_max], [2 1]); %%! we added this to have 2 staircases 1 for same 1 for diff
    stairs(2) = staircaseSetup(1, tSize_start, [tSize_min tSize_max], [1 1]); % keep in mind that using '2' here is only possible if I do not test motor trials (otherwise 1 and two will be motor and visuomotor trials and targetsize must be 3)
    stairs(3) = staircaseSetup(1, tSize_start, [tSize_min tSize_max], [1 1]);
    resultData.stairs = stairs;
end



%% I don't need visual blocks (thus i deleted this part..see original script for this)

%% Init sound

if useSound
    InitializePsychSound;
    
    [y, freq]       = psychwavread( ['.' filesep 'SoundFiles' filesep 'ball_collision.wav'] );
    wavedata        = y';
    wavedata        = [ wavedata; wavedata ];
    nrchannels      = 2;
    pahandle_target = PsychPortAudio( 'Open', [], [], 0, freq, nrchannels );
    PsychPortAudio( 'FillBuffer', pahandle_target, wavedata );
    
    [y, freq]       = psychwavread([ '.' filesep 'SoundFiles' filesep 'center_collision.wav' ]);
    wavedata        = y';
    wavedata        = [ wavedata; wavedata ];
    nrchannels      = 2;
    pahandle_center = PsychPortAudio( 'Open', [], [], 0, freq, nrchannels );
    PsychPortAudio( 'FillBuffer', pahandle_center, wavedata );
end

%% Open window and specify some OpenGL settings
% try
%second argument (debuglevel) set to 0 to increase performance. Does not check after every GL call
InitializeMatlabOpenGL( [], [], [], 0 );
[win , winRect] = PsychImaging( 'OpenWindow', whichScreen, grey, debuggingRect, [], [], 0, 0 );


%Screen( 'BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' );  % we can remove this I think
Screen( 'BeginOpenGL', win );
screenAspectRatio = winRect( 4 ) / winRect( 3 );
winCenter.x = winRect(3)/2;
winCenter.y = winRect(4)/2;
glEnable( GL.LIGHTING );
glEnable( GL.LIGHT0 );
glEnable( GL.DEPTH_TEST );
glMatrixMode( GL.PROJECTION );
glLoadIdentity;
gluPerspective( 25, 1 / screenAspectRatio, 0.1, 100 );
glMatrixMode( GL.MODELVIEW );
glLoadIdentity;
% Set light source position. The original, cool-looking scene was with [ 1 2 5 0 ].
% We changed this because the hue of the lever depended on the angle: not
% good! [0 0 1 0] is boring but correct. [1 2 15 0] is a compromise that sort of works
glLightfv( GL.LIGHT0,GL.POSITION, [ 0 0 1 0 ] );
%Use gluLookAt to position the camera. Thre 9 arguments to the function
%set:
%-view point (0,0,8):     camera at the center of the scene, elevated 8 units on z
%-center (0,0,0):         a reference point towards which the camera is aimed
%-and direction (0,1,0):  indicate which direction is up.
gluLookAt( 0, 0, 8, 0, 0, 0, 0, 1, 0);
glClearColor( bg,bg,bg, 0 );
Screen( 'EndOpenGL', win );
%Screen( 'Flip', win, [], [], 0 );

%This depends on winCenter, therefore all the way down here
VASsettings = set_VASsettings_outcome;              %to draw (banded) confidence scale

if ~debug %%! Uncomment this in real experiment
    HideCursor();
end



%% welcome screen
welcomeScreen_outcome();

%% Start data acquisition

%leverDataIndex will be a counter, increased in 1 after each iteration of the get_leverData_1ms.m
%It indicates the position of the datapoint in the leverData array
leverDataIndex = 0;

session = [];  %TODO: Do we need this?

if useDaq
    dev = LJ_Handle;
    dev.scansPerRead = 1;
    hz = dev.StreamRate;
    
    if dev.streamStarted %just to be sure that no stream is running
        dev.streamStop;
    end
    pause( 0.5 )
else
    event.Angle = 0;
    hz=Screen('NominalFrameRate',win);
    dev=0; % just for the testing purposes %%! i added this
    if strcmp(user, 'elisa')
        hz = 60;
    end
    
end

% Preallocate leverData. size = timepoints x 3. Columns correspond to:
%(1) time stamp;
%(2) released ball (finger lifted from sensor or mouse pressed);
%(3) lever angle
%(4) lever data used for screen (Bool)
leverData = zeros(SkitSet.leverDataPreallocateLength,4);
if not(hz==SkitSet.FrameRate)
    SkitSet.FrameRate=hz;
    disp('Attention Measuring-Rate is at:')
    disp(SkitSet.FrameRate)
end
%% PTB main loop
pause( .1 )

frameCount   = 1;
%src          = [];                                              %correct for both daq and no-daq options?
totalTime=GetSecs;


for block=startingblock:blocks
    
    for trial = 1 : n_trials
        %     if (resultData.targetPos(trial))
        %         SkitSet.xTarget=SkitSet.xTarget_alt1;
        %     else
        %         SkitSet.xTarget=SkitSet.xTarget_alt2;
        %     end
        %startTime = GetSecs;
        %zeroSpeed=0; % this flag indicates if the release speed was 0 (if the two trajectories point in different directions)
        
        if useDaq
            dev.streamStart;
            %streamTime=GetSecs-startTime;
            streamTime=0;
            % tDiff=0;
        else
            startTime=GetSecs;
        end
        while status.task.motor
            
            Screen( 'BeginOpenGL', win );
            glClear;
            
            if ~useDaq
                [mouse.x, mouse.y, buttons] = GetMouse(win);
                
                dx = (winRect(3)/2 - mouse.x);
                dy = (elbowYcoordsPixels - mouse.y);
                %We call these event.etc to emulate the data acquisition output
                event.Angle      = atan2d(dy,dx);
                event.Buttons    = any(buttons);
                
                streamTime =GetSecs-startTime;
                  event.TimeStamps = streamTime;
                
                get_leverData_1ms_mouse_outcome(event);
                
                
            else
                [leverRead,backlog]=dev.streamRead;
                dataAv=true;
                while dataAv
                    event.Angle=leverRead(2,1);
                    event.Grip=leverRead(1,1);
                    streamTime=streamTime+1/dev.StreamRate;
                    event.TimeStamps=streamTime;
                    % event.TimeStamps=leverRead(3,1)-tDiff;
                    
                    get_leverData_1ms_LJ_outcome(event);
                    
                    if backlog<=dev.scansPerRead*2
                        dataAv=false;
                    else
                        [leverRead,backlog]=dev.streamRead;
                    end
                end
            end
            
            %% display the skittles scene
            
            angle = leverData(leverDataIndex, 3 );
            t     = leverData(leverDataIndex, 1 );
            leverData(leverDataIndex,4)=1;
            
            showSkittlesScene_outcome(angle, t,resultData.block(block).blindTrials(trial));
            
            
            Screen( 'EndOpenGL', win );
            
            % Show rendered image at next vertical retrace
            dontsync = 0; %is correct
            %dontsync = 2; %may help %%! it was uncommented
            Screen( 'Flip', win, [], [], 0 );
            frameCount=frameCount+1;
            
            %% Define states
            if t > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
                %if leverData( leverDataIndex, 1 ) > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
                status.task.motor = 0;
                status.task.type1 = 1;
                resultData.block(block).ballCollision(trial)=status.ball.hitTarget;
            end
            
            %allLeverData{block,trial} = leverData;
            
        end
        
        %% Calculate alternative BRP
        
        getOptimalVdiff_outcome(targetHit_landscape, target_sizes);
        
        %fprintf('\n\n difference: %.3f condition: %s  \n\n ', abs(BRP.v - BRP_alt.v),resultData.block(block).condition(trial) );
        % resultData.block(block).condition(trial)  %to check if the condition changed accordingly
   
        %% do type 1 task
        if useDaq
            status = getType1Response_1ms_LJ_outcome(status, BRP, BRP_alt, resultData.type1(block).colorTraces,dev,backlog,streamTime);
        else
            status = getType1Response_mouse_outcome(status, BRP, BRP_alt, trial, resultData.type1(block).colorTraces);
            %status = getType1Response_angeliki_fromTrajectoryscript(angle, t, status, BRP, BRP_alt, trial, feedback, c_trials, dev, streamTime);
            %status = getType1Response(angle, t, status, BRP, BRP_alt, trial, resultData.type1(block).colorTraces);
        end
        
        %TODO error trials remove!
        %%keep in mind that we introduced 2 staircase one for same and 1
        %%for diff vdiff, that;s why when i need to define the signal in
        %%getOptimalVdiff.mat, i use conditionCode after .Signal so as to
        %%use the right vdiff based on condition
        
        conditionCode = (resultData.block(block).condition(trial) == 'd') + 1;  %%! we added this to have 1 or 2 as a value for creating 2 staircases for dame and diff
        if ~resultData.type1(block).errorTrials(trial) &&  (~resultData.type1(block).TrajectoriesDifferentDirections(trial)||t_trials) && (~resultData.type1(block).unequalPosthit(trial) ||resultData.block(block).blindTrials(trial)|| t_trials)%~zeroSpeed &&
            %Determine which staircase you're gonna use (What is meant here? If I update or not? )
            resultData.stairs(resultData.type1(block).whichStair(trial)) = staircaseTrial(conditionCode, resultData.stairs(resultData.type1(block).whichStair(trial)), resultData.type1(block).correct(trial));
            resultData.stairs(resultData.type1(block).whichStair(trial)) = staircaseUpdate(conditionCode, resultData.stairs(resultData.type1(block).whichStair(trial)), -vStepsize);
        end
  
        
        resultData.stairs(2) = staircaseTrial(1, resultData.stairs(2), resultData.type1(block).targetHit(trial));
        resultData.stairs(2) = staircaseUpdate(1, resultData.stairs(2), -tStepsize);
        
        resultData.stairs(3) = staircaseTrial(1, resultData.stairs(3), resultData.type1(block).targetHit(trial));
        resultData.stairs(3) = staircaseUpdate(1, resultData.stairs(3), -tStepsize);
        
        status.task.type1 = 0;
          
%         if useDaq
%             dev.streamStop;
%             
%             pause( 0.0005 )
%         end
                
        %% do type 2 task
        %
        if ~t_trials %|| n_trials-trial<10
            status.task.type2 = 1;
            resultData = getType2Response_outcome(resultData, trial,feedback);
            status.task.type2 = 0;
        end
        
        %  if t > BRP.t0 + SkitSet.max_fligth_time && status.ball_released  %TODO: is this if condition neccessary?!
        resultData = close_trial_outcome(trial, resultData);                        %reset all statuses
        %  end
        
    end
    
    
    %% Visual condition (i deleted this part. if needed see original script)
    
    %% If not a visual condition
    
    if ~(block==blocks)
        visualBlock=0;
        
        if breaksBetweenBlocks
            status.task.break=1;
            [breaks, pauseTime]= breakScreen_outcome( win,breaks,subjID,resultPath,feedback);
            status.task.break=0;
            resultData.breakTime(block)=pauseTime; % a break if the flag is set to 1 and not the last block
        end
        status.task.motor=1;
    end
    %% Save the data at the end of each block
    
    if ~t_trials
        if feedback
            %         if ~exist('Results/Feedback','dir')
            %             mkdir('Results/Feedback')
            %         end
            save([resultPath filesep subjID filesep 'SkittlesFeedbackResult_' subjID, '.mat'], 'resultData')
        else
            if ~exist([resultPath filesep subjID filesep 'EverythingSkittles'],'dir')
                mkdir([resultPath filesep subjID filesep 'EverythingSkittles'])
            end
            save([resultPath filesep subjID filesep 'SkittlesResult_' subjID, '.mat'], 'resultData')
            save([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'])
        end
    else
        %     if ~exist('Results/Training','dir')
        %         mkdir('Results/Training')
        %     end
        save([resultPath filesep subjID filesep 'SkittlesTrainingResult_' subjID,'.mat'], 'resultData')
        
        % show the results of the training
        figure('Name','Training Results vdiff')
        %subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
        %title('staircase')
        plot(abs(resultData.type1.vdiff), ['r' '-o'])
        xlabel('trial')
        ylabel('v0 difference')
        
        figure('Name','Training Results tSize')
        plot(resultData.type1.tSize, ['r' '-o'])
        xlabel('trial')
        ylabel('tRadius')
        
    end
    
    if ~t_trials && ~f_trials
        if debug;wrapat= 60;else;wrapat= 110;end
        DrawFormattedText(win, 'Saving data...','center','center',SkitSet.textColor,wrapat);
        Screen( 'Flip', win, [], [], 0 );
        save([resultPath filesep subjID filesep 'Skittlesblock_' num2str(block) '_' subjID,'.mat']);
        save([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'],'SkitSet');
        save([resultPath filesep subjID filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subjID, '.mat'],'resultData');
    end
    
    if block == blocks
        if ~t_trials && ~f_trials
            message=sprintf('THE END! \n \n Thank you for taking part in this study!');
            pauseTic=tic;
            if debug;wrapat= 60;else;wrapat= 110;end
            DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat);
            Screen( 'Flip', win, [], [], 0 );
            KbWait(-1);
            pause_time=toc(pauseTic);
            
             
            %%i added thid to plot the vdiff 
            %             figure('Name','vdiff same and different')
            %                       xlabel('trial')
            %                       ylabel('v difference')
            %                       plot(abs(resultData.type1.vdiff),'-o','MarkerFaceColor','c');
            %                       hold on
            %                       plot(find(resultData.block.condition=='s'),abs(resultData.type1.vdiff(resultData.block.condition=='s')),'o','MarkerFaceColor','g','MarkerEdgeColor','g')
            %                       plot(find(resultData.block.condition=='d'),abs(resultData.type1.vdiff(resultData.block.condition=='d')),'o','MarkerFaceColor','r','MarkerEdgeColor','r')
            %                       hold off
            %
            %                       plot(1:length(resultData.type1.vdiff),abs(resultData.type1.vdiff),'-o','MarkerFaceColor','c');
            %                       hold on
            %                       plot(find(resultData.block.condition=='s'),abs(resultData.type1.vdiff(resultData.block.condition=='s')),'-o','MarkerFaceColor','g','MarkerEdgeColor','g')
            %                       plot(find(resultData.block.condition=='d'),abs(resultData.type1.vdiff(resultData.block.condition=='d')),'-o','MarkerFaceColor','r','MarkerEdgeColor','r')
            %                       hold off
            %             %save figure in the folder
            %saveas(gcf,[resultPath filesep subjID filesep 'EverythingSkittles' filesep subjID '_vdiff_same_diff.pdf'],'-fillpage')
            
            
        else
            message=sprintf('THE END');
            pauseTic=tic;
            if debug;wrapat= 60;else;wrapat= 110;end
            DrawFormattedText(win, message,'center','center',SkitSet.textColor,wrapat);
            Screen( 'Flip', win, [], [], 0 );
            KbWait(-1);
            pause_time=toc(pauseTic);
            
            
        end
    end
end



status.task.motor=1;



% catch e
%     warning(e.message)
%     Screen( 'CloseAll' );
%     ShowCursor();
%     save([resultPath filesep subjID filesep 'SkittlesERRORTRIALS_' subjID,'.mat']);
%     if useDaq; dev.close; end
% end

t_total = GetSecs-totalTime;
resultData.totalTime=t_total;

%% Clean up
Screen( 'CloseAll' );
ShowCursor();

if useDaq; dev.close; end

clearvars -global leverData

if useSound
    PsychPortAudio( 'Stop',  pahandle_target );
    PsychPortAudio( 'Close', pahandle_target );
    PsychPortAudio( 'Stop',  pahandle_center );
    PsychPortAudio( 'Close', pahandle_center );
end
disp( ['mean frames per second = ', num2str( frameCount / t_total, '%3.2f' )] )
% print how lown it took to do the experiment 
time = toc/60; % in minutes
disp('');
disp(['This part of the experiment took: ', num2str(time), ' minutes.']);


