function BCP = set_BallCollisionParameters_outcome

BCP.t0            = 0;
BCP.x0            = 0;
BCP.y0            = 0;
BCP.vx            = 0;
BCP.vy            = 0;
BCP.Ax            = 0;
BCP.Ay            = 0;
BCP.phix          = 0;
BCP.phiy          = 0;
BCP.posthit       = 0;
BCP.t_posthit     = 0;
BCP.x_posthit     = 0;
BCP.y_posthit     = 0;
BCP.targetHitTime = 0;
BCP.postHitTime   = 0;

end



