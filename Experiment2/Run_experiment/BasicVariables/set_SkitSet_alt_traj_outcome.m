function SkitSet_alt_traj = set_SkitSet_alt_traj_outcome

global resultData
global SkitSet
 
%% settings for skittles task --> take everything from the SkitSet of experiment and just update the TargetRadius
%SkitSet.xTarget         = 0.0000;
%SkitSet.xTarget_alt1    = 0.0000;
%SkitSet.xTarget_alt2    = 0.2000;
%SkitSet.xTarget         = 0.9500;
SkitSet_alt_traj.xTarget         = SkitSet.xTarget;
SkitSet_alt_traj.yTarget         = SkitSet.yTarget;
SkitSet_alt_traj.TargetRadius    = resultData.stairs(2).Signal; % take target size trial by trial 
SkitSet_alt_traj.xCenter         = SkitSet.xCenter;        %Used for glTranslatef - didn't quite get what it does
SkitSet_alt_traj.yCenter         = SkitSet.yCenter;
SkitSet_alt_traj.xLever          = SkitSet.xLever;        %starting position
SkitSet_alt_traj.yLever          = SkitSet.yLever;  %In Heiko's coordinates, these are 'meters' and follow Mueller and Sternad (2004)
SkitSet_alt_traj.LeverLength     = SkitSet.LeverLength;
SkitSet_alt_traj.BallRadius      = SkitSet.BallRadius;
SkitSet_alt_traj.Mass            = SkitSet.Mass;
SkitSet_alt_traj.CenterRadius    = SkitSet.CenterRadius;   %radius of the center pole (base or the cone drawn by openGL)
SkitSet_alt_traj.xStiff          = SkitSet.xStiff;        %Spring constant (k in Mueller and Sternad 2004) in x ...
SkitSet_alt_traj.yStiff          = SkitSet.yStiff;        %and y directions
SkitSet_alt_traj.Damping         = SkitSet.Damping;
SkitSet_alt_traj.xOmega          = SkitSet.xOmega;   %this we know. Just the frequency, used for the trajectory x(t), y(t) calculation
SkitSet_alt_traj.yOmega          = SkitSet.yOmega;
SkitSet_alt_traj.LeverWidth      = SkitSet.LeverWidth;
SkitSet_alt_traj.max_fligth_time = SkitSet.max_fligth_time;        % 2 sec is correct. using more? for debugging
SkitSet_alt_traj.FrameRate       = SkitSet.FrameRate;      %rate of lever data measurement 
SkitSet_alt_traj.dataPointsForVelocity = SkitSet.dataPointsForVelocity;
SkitSet_alt_traj.leverDataPreallocateLength = SkitSet.leverDataPreallocateLength;


%% settings for type 1 and type 2 response
SkitSet_alt_traj.responseKeys     = SkitSet.responseKeys;             %Will be used for type 1 response
SkitSet_alt_traj.responseKeyNames = SkitSet.responseKeyNames;  %Will be displayed as text in type 1 response


SkitSet_alt_traj.response2leverRange = SkitSet.response2leverRange;
SkitSet_alt_traj.response1Selection  = SkitSet.response1Selection; % sensitivity for response type 1 selection (broadness of angle for selection of trajectory)
%     SkitSet.t_vdiff=1.5; % task 1 difficulty for t_trials

SkitSet_alt_traj.textColor      = SkitSet.textColor; %black text display
SkitSet_alt_traj.coneColor(1,:) = SkitSet.coneColor(1,:);
SkitSet_alt_traj.coneColor(2,:) = SkitSet.coneColor(2,:);
SkitSet_alt_traj.blindTrialConeColor = SkitSet.blindTrialConeColor;

end

