function status = set_status_outcome
%different tasks: movement, type 1, type 2 
status.task.motor = 1;    %starts with the throwing task
status.task.type1 = 0;    
status.task.type2 = 0;
status.task.break = 0;
status.task.visual=0;
%ball 
status.ball.grabbed   = 0;
status.ball.contact   = 0;
status.ball.thrown    = 0;
status.ball.flightPredicted = 0;
status.ball.hitPost = 0;
status.ball.hitTarget = 0;
status.ball.posthit_target = 0;     %i don't know what this does
status.ball.posthit_after_collision  = 0; %also don't know

%sounds played - necessary at all?
status.soundPlayed.targetCollision       = 0;
status.soundPlayed.centerBallCollision   = 0;
status.soundPlayed.centerBallCollision   = 0;
status.soundPlayed.centerTargetCollision = 0;
status.soundPlayed.centerBallCollision_2  = 0;


end



