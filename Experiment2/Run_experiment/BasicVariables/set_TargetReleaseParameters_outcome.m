function TRP = set_TargetReleaseParameters_outcome

TRP.t0          = 0;
TRP.x0          = 0;
TRP.y0          = 0;
TRP.vx          = 0;
TRP.vy          = 0;
TRP.Ax          = 0;
TRP.Ay          = 0;
TRP.phix        = 0;
TRP.phiy        = 0;
TRP.posthit     = 0;
TRP.t_posthit   = 0;
TRP.x_posthit   = 0;
TRP.y_posthit   = 0;

end

