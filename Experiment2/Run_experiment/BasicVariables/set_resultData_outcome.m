function resultData = set_resultData_outcome

resultData.block.releaseSpeed      = [];
resultData.block.releaseAngle      = [];
resultData.block.ballCollision     = [];
resultData.block.blindTrials       = []; %?
%resultData.vdiff             = [];
% resultData.alt1.releaseSpeed = [];  %Record which two alternatives were shown, described by their parameters
% resultData.alt1.releaseAngle = [];
% resultData.alt2.releaseSpeed = [];
% resultData.alt2.releaseAngle = [];
resultData.type1.response           = [];
resultData.type1.RT                 = [];
resultData.type1.chosenColor        = [];
resultData.type1.correct            = [];
resultData.type1.initialLeverAngle  = [];
resultData.type1.leverAngleAdjusted = [];
resultData.type1.errorTrials        = [];


resultData.type2.RT          = [];
resultData.type2.mousePosY   = [];
resultData.type2.conf        = [];

resultData.visual.type1.RT                 = [];
resultData.visual.type1.correct            = [];
resultData.visual.type1.response           = [];
resultData.visual.type1.initialLeverAngle  = [];
resultData.visual.type1.leverAngleAdjusted = [];
resultData.visual.type1.chosenColor         = [];

resultData.visual.type2.RT          = [];
resultData.visual.type2.mousePosY   = [];
resultData.visual.type2.conf        = [];
resultData.visual.movementData=[];


resultData.totalTime         = [];
resultData.useDaq            = [];
resultData.breakTime         = [];
 resultData.intraBlockBreak  = [];
end
