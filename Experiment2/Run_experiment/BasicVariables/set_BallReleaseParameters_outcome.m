function BRP = set_BallReleaseParameters_outcome

BRP.t0          = NaN;
BRP.angle       = NaN;
BRP.v           = NaN;
BRP.x0          = NaN;
BRP.y0          = NaN;
BRP.Ax          = NaN;
BRP.Ay          = NaN;
BRP.phix        = NaN;
BRP.phiy        = NaN;
BRP.posthit     = NaN;
BRP.minDist     = NaN;
BRP.score       = NaN;
BRP.t_posthit   = NaN;
BRP.x_posthit   = NaN;
BRP.y_posthit   = NaN;
BRP.x_collision = NaN;
BRP.y_collision = NaN;
BRP.t_collision = NaN;
BRP.idx0        = NaN;
BRP.trajectory  = [];

end
