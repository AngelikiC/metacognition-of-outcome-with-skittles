function [r g b] = SkittlesClr(d,ok)
%returns the color that belongs to a given distance to the target
%ok=0 if center post is hit

if (ok == 0) 
    r = 0; g = 0; b = 50;
else
    r = 255 - d * 255;
    if (r < 80)
        r = 80;
    end
    g = 255 - d * d * 12 * 255;
    if (g < 80)
        g = 80;
    end
    b = 255 - d * d * 120 * 255;
    if (b < 80)
        b = 80;
    end
end