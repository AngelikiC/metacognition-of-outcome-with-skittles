%PlotSkittlesTaskspace

%% Specify some values
a_start = -180;           %Minimaler Abwurfwinkel
a_end = 180;            %Maximaler Abwurfwinkel
v_start = 0;            %Minimale Abwurfgeschwindigkeit
v_end = 6;              %Maximale Abwrufgeschwindigkeit
a_resolution = 100;     %Horizontale Aufl?sung
v_resolution = 100;     %Vertikale Aufl?sung

plot_colorbar = 1;      %Colorbar is plotted when 1
save_as_high_definition_tif = 0; %Speichert die Abbidlung als tif mit 300 (farbe) bzw. 600 (grau)
grey_scaled_figure = 0; %Berechnet die Colormatrix nur als Grauwerte

%% Define SkittlesSetup
SkitSet.xTarget = 0; %Target Position X
SkitSet.yTarget = 0.9;  %Target Position Y
SkitSet.xCenter = 0;
SkitSet.yCenter = 0;
SkitSet.xLever = 0;
SkitSet.yLever = -1.5;
SkitSet.LeverLength = 0.4;
SkitSet.BallRadius = 0.05;
SkitSet.Mass = 0.1;
SkitSet.CenterRadius = 0.25;
SkitSet.xStiff = 1.0;
SkitSet.yStiff = 1.0;
SkitSet.Damping = 0.01;
SkitSet.xOmega = sqrt(SkitSet.xStiff/SkitSet.Mass-(SkitSet.Damping/(2*SkitSet.Mass))^2); 
SkitSet.yOmega = sqrt(SkitSet.yStiff/SkitSet.Mass-(SkitSet.Damping/(2*SkitSet.Mass))^2); 

%% Calculate ColorMatrix
alpha = linspace(a_start,a_end,a_resolution);
velocity = linspace(v_start,v_end,v_resolution);
clrmatrix = zeros(v_resolution,a_resolution,3);

for j = 1:a_resolution
      for i = 1:v_resolution
          [d,ok] = SkitDistanceToTarget_simplified(alpha(j),velocity(i),SkitSet);
          %[d,ok] = SkitDistanceToTarget(alpha(j),velocity(i),SkitSet);
          if grey_scaled_figure == 1
              [r g b] = SkittlesClr(d,ok);
              clr = mean([r g b]);
              r = clr; g = clr; b = clr;
          else
              [r g b] = SkittlesClr(d,ok);
          end
          clrmatrix(i,j,1:3) = [r g b]/256;
      end
end

%% Plot Taskspace
h1 = figure('Name', 'SkittlesTaskSpace');
set(h1, 'units', 'normalized', 'outerposition', [0 0 1 1]);
subplot('Position',[0.10 0.15 0.72 0.75]) 
h2 = imagesc([a_start a_end],[v_start v_end],clrmatrix);
set(gca,'YDir','normal');
set(gca, 'FontSize', 20);
set(gcf,'Color',[1 1 1]);
set(gca,'TickDir','out');
box off
title_text=strcat('Target Position: X=', num2str(SkitSet.xTarget,'%1.2f'), ', Y=', num2str(SkitSet.yTarget,'%1.2f'));
title(title_text, 'fontsize', 24);
xlabel('Angle (deg)', 'fontsize', 24);
ylabel('Velocity (m/s)', 'fontsize', 24);

%% Plot Skittles-Colorbar

if plot_colorbar == 1
   %Calculate colormatrix for colorbar
   d_colorbar = linspace(0, 0.7, 100);
   clrbar_clrmatrix = zeros(length(d_colorbar), 1, 3);

   for i = 1 : length(d_colorbar)
       if grey_scaled_figure == 1
              [r g b] = SkittlesClr(d_colorbar(i), 1);
              clr = mean([r g b]);
              r = clr; g = clr; b = clr;
          else
              [r g b] = SkittlesClr(d_colorbar(i), 1);
       end
       clrbar_clrmatrix(i, 1, 1:3) = [r g b]/256;
   end

   subplot('Position',[0.90 0.20 0.035 0.40]) 
   imagesc([0 1],[0 0.7],clrbar_clrmatrix);
   set(gca,'YDir','normal');
   set(gca, 'FontSize', 20);
   set(gca,'TickDir','out');
   box off
   %ylabel('Distance [m]', 'fontsize', 24);
   set(gca,'XTick',[]);
   set(gca,'YTick', 0 : 0.1 : 0.7);
   box on
   
   %Plot Foul
   foul_clrmatrix = zeros(1,1,3);
   if grey_scaled_figure == 1
          [r g b] = SkittlesClr(1, 0);
          clr = mean([r g b]);
          r = clr; g = clr; b = clr;
          foul_clrmatrix(1,1,1:3) = [r g b]/256;
      else
          foul_clrmatrix(1,1,1:3) = [0 0 50]/256;
   end

   subplot('Position',[0.90 0.65 0.035 0.055]) 
   h2 = imagesc([0 1],[0 1], foul_clrmatrix);
   set(gca,'YDir','normal');
   set(gca, 'FontSize', 20);
   set(gca,'TickDir','out');
   box on
   set(gca,'XTick',[]);
   set(gca,'YTick',0.5);
   set(gca,'YTickLabel',{'Foul'}, 'FontSize',20);
   annotation('textbox','Position',[0.8341 0.7266 0.1454 0.05], 'LineStyle','none','String','Distance (m)', 'FontSize', 22, 'HorizontalAlignment', 'center');
   box on
end

if save_as_high_definition_tif == 1
    set(gcf,'PaperPositionMode','auto') %F?r zbuffer-Option erforderlich
    if grey_scaled_figure == 1
        print('-dtiff', '-r300', 'Taskspace') % -> tiff-Format -> dpi -> Scalierung wie auf Bildschirm -> filename
    else
        print('-dtiff', '-r150', 'Taskspace_-07_0') % -> tiff-Format -> dpi -> Scalierung wie auf Bildschirm -> filename
    end
end