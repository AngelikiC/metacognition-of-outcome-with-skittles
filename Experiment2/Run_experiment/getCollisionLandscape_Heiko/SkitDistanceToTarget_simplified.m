function [d, ok] = SkitDistanceToTarget_simplified(alfa, v, SkitSet)
%returns the global (whole trajectory) minimum of the ball to the target distance d
%and checks if the center post is hit (0->Hit, 1->NoHit)
global BallX;   %Globale Variablen sind f�r das Plotten der Skittles-Aufgabe erfoderlich (Plot_Skittles_Task_1dof)
global BallY;
global indexm;
global vx;
global vy;
global SRP;

%Time of measured Movement.
t = linspace(0, 2, 1000);

alfa = pi*alfa/180;
vx = v*sin(alfa);
vy = v*cos(alfa);
SRP.x0 = SkitSet.xLever-cos(alfa) * SkitSet.LeverLength;
SRP.y0 = SkitSet.yLever+sin(alfa) * SkitSet.LeverLength;
Ex = SkitSet.Mass * vx^2 / 2 + SkitSet.xStiff * SRP.x0^2 / 2;
SRP.Ax = sqrt(2 * Ex / SkitSet.xStiff);
Ey = SkitSet.Mass * vy^2 / 2 + SkitSet.yStiff * SRP.y0^2 / 2;
SRP.Ay = sqrt(2 * Ey / SkitSet.yStiff);
if (SRP.Ax ~= 0)
     a = SRP.x0 / SRP.Ax;
     SRP.phix = acos(a);
else
    SRP.phix = 0;
end
if (vx > 0) 
    SRP.phix = -SRP.phix;
end
if (SRP.Ay ~= 0)
    a = SRP.y0 / SRP.Ay;
    SRP.phiy = acos(a); 
else
    SRP.phiy = 0;
end
if (vy > 0)
    SRP.phiy = -SRP.phiy;
end

%Ball Postion in X, Y directions
Damping = exp(-t * SkitSet.Damping / (2*SkitSet.Mass));
BallX = SRP.Ax .* Damping .* cos(SkitSet.xOmega .* t + SRP.phix);
BallY = SRP.Ay .* Damping .* cos(SkitSet.yOmega .* t + SRP.phiy);

%Check whether the centre post was hit
distance = sqrt((BallX - SkitSet.xCenter).^2 + (BallY - SkitSet.yCenter).^2);
idx_PostHit = find(distance <= (SkitSet.CenterRadius + SkitSet.BallRadius), 1, 'first');
if idx_PostHit > 0
    PostHit = 1;
else
    PostHit = 0;
end

%Calculate minimal distance to target
trajectory = sqrt((BallX - SkitSet.xTarget) .^2 + (BallY - SkitSet.yTarget) .^2);
[d indexm] = min(trajectory);
ok = 1;

if (PostHit && idx_PostHit < indexm)
  %->Ball hits center post before it hits target
  d = 0.5; 
  ok = 0;
end