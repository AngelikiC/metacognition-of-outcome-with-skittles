% Written by Angeliki Charalampaki 2021
% A script for preparing the data for dprime 2 by 2 analysis on R

clearvars; close all;

user = 'angeliki_office';

if  strcmp(user, 'angeliki_office')
     experiment_folder = [filesep 'Users' filesep 'Angeliki' filesep 'Seafile' filesep 'My Library' filesep 'Angeliki' filesep 'Skittles_OUTCOME' filesep];
     data_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_raw' filesep];
    save_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_preprocessed'];
    figure_path = [experiment_folder 'Analyze_data' filesep 'Slittles_figures'];
        
end
cd(save_path)
onlypositive =1;


%% Or if you want there is also this option
% Set Participants to include 
load('participants_for_target_hmeta_separate_for_condition_OUTCOME_fromthe28_selected.mat')
subject_clean = participant_target_hit;   % these are all participants included in MLE analysis


if onlypositive % Run the analysis by exluding the 4 participants that had negative dprime.
    negative_dprime = {'213AX','214XW','342RD','432PT'};
    subject_exclude = negative_dprime;
else
    subject_exclude =[];
    subject_clean(ismember(subject_clean,subject_exclude))=[];
    subject_clean()
end



metadata_Skittles_OUTCOME_target_MLE = cell2table(cell(0,13),'VariableNames',{'Participant','same_targetmiss_meta_d','same_targetmiss_d','same_targetmiss_mratio','diff_targetmiss_meta_d','diff_targetmiss_d','diff_targetmiss_m_ratio',...
                'same_targethit_meta_d','same_targethit_d','same_targethit_mratio','diff_targethit_meta_d','diff_targethit_d','diff_targethit_m_ratio'});

%% 1) load data for MLE


        for number_of_subj=1:length(subject_clean)
            
            fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
            load(sprintf('data_OUTCOME_%s.mat',subject_clean{number_of_subj}));
            Participant={subject_clean{number_of_subj}};
            
            
            % Normalize confidence
            conf_norm = (confidence_analysis-min(confidence_analysis))/(max(confidence_analysis)-min(confidence_analysis));
 
            % Confidence for Target Hit:
            nor_conf_same_target_hit=conf_norm(condition_analysis == 's' & targetHit_analysis==1);
            nor_conf_diff_target_hit=conf_norm(condition_analysis == 'd' & targetHit_analysis==1 );
            % Confidence for:Target Miss
            nor_conf_diff_target_miss=conf_norm(condition_analysis == 'd' & targetHit_analysis==0 );
            nor_conf_same_target_miss=conf_norm(condition_analysis == 's' & targetHit_analysis==0);

            
            nRatings=6; % number of ratings
            
            % Ratings for: Overall
            ratings_same_target_hit=discretize(nor_conf_same_target_hit,nRatings); % bins the confidence ratings: ratings_same==1 are the smallest and ratings_same==nRatings is the largest rating
            ratings_diff_target_hit=discretize(nor_conf_diff_target_hit,nRatings);
            
            ratings_same_target_miss=discretize(nor_conf_same_target_miss,nRatings); % bins the confidence ratings: ratings_same==1 are the smallest and ratings_same==nRatings is the largest rating
            ratings_diff_target_miss=discretize(nor_conf_diff_target_miss,nRatings);
           
            % Stimulus for: Overall
            stimulus_same_target_hit = stimulus_analysis(condition_analysis == 's'  & targetHit_analysis==1);
            stimulus_diff_target_hit = stimulus_analysis(condition_analysis == 'd'  & targetHit_analysis==1);
            stimulus_same_target_miss = stimulus_analysis(condition_analysis == 's'  & targetHit_analysis==0);
            stimulus_diff_target_miss = stimulus_analysis(condition_analysis == 'd'  & targetHit_analysis==0);

            
            % Response for: Overall
            response_same_target_hit=response_analysis(condition_analysis == 's'  & targetHit_analysis==1);
            response_diff_target_hit=response_analysis(condition_analysis == 'd'  & targetHit_analysis==1);
            response_same_target_miss=response_analysis(condition_analysis == 's'  & targetHit_analysis==0);
            response_diff_target_miss=response_analysis(condition_analysis == 'd'  & targetHit_analysis==0);

            
            % Trials2Counts for: Overall
            
            %  function [nR_S1, nR_S2] = trials2counts(stimID, response, rating, nRatings, padCells, padAmount)
            [nR_S1_same_target_hit, nR_S2_same_target_hit]=trials2counts(stimulus_same_target_hit,response_same_target_hit,ratings_same_target_hit,nRatings,1);
            [nR_S1_diff_target_hit, nR_S2_diff_target_hit]=trials2counts(stimulus_diff_target_hit,response_diff_target_hit,ratings_diff_target_hit,nRatings,1);
            [nR_S1_same_target_miss, nR_S2_same_target_miss]=trials2counts(stimulus_same_target_miss,response_same_target_miss,ratings_same_target_miss,nRatings,1);
            [nR_S1_diff_target_miss, nR_S2_diff_target_miss]=trials2counts(stimulus_diff_target_miss,response_diff_target_miss,ratings_diff_target_miss,nRatings,1);

            
            % fit data for MLE: Overall
            %fit = fit_meta_d_MLE(nR_S1, nR_S2, s, fncdf, fninv)
            fit_same_target_hit=fit_meta_d_MLE(nR_S1_same_target_hit, nR_S2_same_target_hit);
            fit_diff_target_hit=fit_meta_d_MLE(nR_S1_diff_target_hit, nR_S2_diff_target_hit);
            
            fit_same_target_miss=fit_meta_d_MLE(nR_S1_same_target_miss, nR_S2_same_target_miss);
            fit_diff_target_miss=fit_meta_d_MLE(nR_S1_diff_target_miss, nR_S2_diff_target_miss);

                       
            MetaData_target = table(Participant,fit_same_target_miss.meta_da,fit_same_target_miss.da,fit_same_target_miss.M_ratio,fit_diff_target_miss.meta_da,fit_diff_target_miss.da,fit_diff_target_miss.M_ratio,...
               fit_same_target_hit.meta_da,fit_same_target_hit.da,fit_same_target_hit.M_ratio,fit_diff_target_hit.meta_da,fit_diff_target_hit.da,fit_diff_target_hit.M_ratio );
            MetaData_target.Properties.VariableNames={'Participant','same_targetmiss_meta_d','same_targetmiss_d','same_targetmiss_mratio','diff_targetmiss_meta_d','diff_targetmiss_d','diff_targetmiss_m_ratio',...
                'same_targethit_meta_d','same_targethit_d','same_targethit_mratio','diff_targethit_meta_d','diff_targethit_d','diff_targethit_m_ratio'};
  
           
            metadata_Skittles_OUTCOME_target_MLE = [metadata_Skittles_OUTCOME_target_MLE; MetaData_target];
            %             save('metadata_Skittles_OUTCOME_TARGET_MLE_for_JASP_28.mat', 'metadata_Skittles_OUTCOME_target_MLE');
            %             writetable(metadata_Skittles_OUTCOME_target_MLE,'metadata_Skittles_OUTCOME_TARGET_MLE_for_JASP_28.csv')
            
            if onlypositive
                save('metadata_Skittles_OUTCOME_TARGET_MLE_for_only_possitivedprime.mat', 'metadata_Skittles_OUTCOME_target_MLE');
                writetable(metadata_Skittles_OUTCOME_target_MLE,'metadata_Skittles_OUTCOME_TARGET_MLE_onlypositiveDPRIME.csv')
            else
                save('metadata_Skittles_OUTCOME_TARGET_MLE_for_JASP_21.mat', 'metadata_Skittles_OUTCOME_target_MLE');
                writetable(metadata_Skittles_OUTCOME_target_MLE,'metadata_Skittles_OUTCOME_TARGET_MLE_for_JASP_21.csv')
            end

     
        end
           

      
        fprintf('\n******\n Finished All the analyses \n******\n\n');
        