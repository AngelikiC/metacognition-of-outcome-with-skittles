%% Get the ROPE and HDI on your h-meta plots (experiment 02)
% For 28 participants: HDI includes 0 => not important
% Only positive. Means the 24 participants that had positive d'
%% H-meta Target Hit: Same vs Diff 
clearvars
cd('/Users/Angeliki/Seafile/My Library/Angeliki/Skittles_OUTCOME/Analyze_data/Skittles_data_Outcome/Data_Skittles_Outcome_preprocessed');
load('metadata_Skittles_OUTCOME_MLE_28part_conf.mat')

onlypositive=1;
if onlypositive % Run the analysis by exluding the 4 participants that had negative dprime.
    negative_dprime = {'213AX','214XW','342RD','432PT'};
    idx_exclude = find(contains(metadata_Skittles_OUTCOME_MLE.Participant, negative_dprime));
    metadata_Skittles_OUTCOME_MLE(idx_exclude,:)=[]; %exclude those participants with negative d'
end


mratios_pooled_MLE= [metadata_Skittles_OUTCOME_MLE.same_mratio;metadata_Skittles_OUTCOME_MLE.diff_m_ratio];
sd_mratios_pooled_MLE = std(mratios_pooled_MLE);
Rope_Skittles_OUTCOME = [-0.1*sd_mratios_pooled_MLE  0.1*sd_mratios_pooled_MLE] ; %get the ROPE. test if using the 0.1 gives reasonable result. as here we are dealing with logarithmic values

if onlypositive % Run the analysis by exluding the 4 participants that had negative dprime.
     load('h_fit_outcome_target_hit_between_conditions_24_part_positivedprime_paper_3th_extra_conf.mat')
else
    load('h_fit_outcome_target_hit_between_conditions_28_part_paper_3th_extra_conf.mat')
end
Skittles_outcomee_target_hit  = exp(outcome_target_hit_only.fit.mcmc.samples.mu_logMratio(:,:,1)) - exp(outcome_target_hit_only.fit.mcmc.samples.mu_logMratio(:,:,2));

hdi_target_hit = calc_HDI(Skittles_outcomee_target_hit(:));

fprintf(('\n Mratio session values = %.2f and %.2f'), exp(outcome_target_hit_only.fit.mu_logMratio(1)), exp(outcome_target_hit_only.fit.mu_logMratio(2)));
fprintf(['\n Estimated difference in Mratio between sessions: ', num2str(exp(outcome_target_hit_only.fit.mu_logMratio(1)) - exp(outcome_target_hit_only.fit.mu_logMratio(2)))]);
fprintf(['\n HDI on difference in Mratio: ', num2str(hdi_target_hit)]);
fprintf(['\n ROPE  : ', num2str(Rope_Skittles_OUTCOME) '\n\n']);


figure %using the fit
% c= histfit(Skittles_outcomee_target_hit(:),77); %create a histogramm fit using the number of bins from previous step
c= histfit(Skittles_outcomee_target_hit(:),120); %create a histogramm fit using the number of bins from previous step
delete(c(1))
c(2).Color = 'black';
hold on
xline(Rope_Skittles_OUTCOME(1),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xline(Rope_Skittles_OUTCOME(2),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
% xlim([-0.55, 0.45]);
% xlim([-0.55, 0.77]);
xlim([-0.77, 0.5]);
line([hdi_target_hit(1); hdi_target_hit(2)],[30:50;30:50], 'Color','black','linestyle','-','LineWidth',8)
hold off

%using the histogramm
figure1 = figure;
% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
histogram(Skittles_outcomee_target_hit(:),'DisplayStyle','stairs','FaceColor','none','EdgeColor','black','LineWidth',1.3) 
xline(Rope_Skittles_OUTCOME(1),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xline(Rope_Skittles_OUTCOME(2),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
% xlim([-0.55, 0.45]);
% xlim([-0.55, 0.77]);
xlim([-0.77, 0.5]);
line([hdi_target_hit(1); hdi_target_hit(2)],[30:50;30:50], 'Color','black','linestyle','-','LineWidth',8)
box(axes1,'on');
hold(axes1,'off');
set(axes1,'FontSize',23,'FontName','Arial Narrow','XTick',[-0.6 0 0.4],'YTick',zeros(1,0));



%% H-meta Target Miss: Same vs Diff 
if onlypositive % Run the analysis by exluding the 4 participants that had negative dprime.

     load('h_fit_outcome_target_miss_between_conditions_24part_positivedprime_paper_th3_extra_conf.mat')
else
    load('h_fit_outcome_target_miss_between_conditions_28part_paper_th3_extra_conf.mat')
end


Skittles_outcome_target_miss  = exp(outcome_miss_only.fit.mcmc.samples.mu_logMratio(:,:,1)) - exp(outcome_miss_only.fit.mcmc.samples.mu_logMratio(:,:,2));
hdi_target_miss = calc_HDI(Skittles_outcome_target_miss(:));

%Get the values
fprintf(('\n Mratio session values = %.2f and %.2f'), exp(outcome_miss_only.fit.mu_logMratio(1)), exp(outcome_miss_only.fit.mu_logMratio(2)));
fprintf(['\n Estimated difference in Mratio between sessions: ', num2str(exp(outcome_miss_only.fit.mu_logMratio(1)) - exp(outcome_miss_only.fit.mu_logMratio(2)))])
fprintf(['\n HDI on difference in Mratio): ', num2str(hdi_target_miss)])
fprintf(['\n ROPE : ', num2str(Rope_Skittles_OUTCOME) '\n\n']);


% d= histfit(Skittles_outcome_target_miss(:),146); %number of bins
% according to histogramm
figure
d= histfit(Skittles_outcome_target_miss(:),120); %create a histogramm fit using the number of bins common for all
delete(d(1))
d(2).Color = 'black';
hold on
xline(Rope_Skittles_OUTCOME(1),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xline(Rope_Skittles_OUTCOME(2),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
% xlim([-0.55, 0.45]);
xlim([-0.77, 0.5]);

% xlim([-0.55, 0.77]);
line([hdi_target_miss(1); hdi_target_miss(2)],[30:50;30:50], 'Color','black','linestyle','-','LineWidth',8)
hold off



%using the histogramm
figure2 = figure;
% Create axes
axes1 = axes('Parent',figure2);
hold(axes1,'on');
histogram(Skittles_outcome_target_miss(:),'DisplayStyle','stairs','FaceColor','none','EdgeColor','black','LineWidth',1.3)
xline(Rope_Skittles_OUTCOME(1),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xline(Rope_Skittles_OUTCOME(2),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
% xlim([-0.55, 0.45]);
% xlim([-0.55, 0.77]);
xlim([-0.77, 0.5]);
line([hdi_target_miss(1); hdi_target_miss(2)],[30:50;30:50], 'Color','black','linestyle','-','LineWidth',8)
box(axes1,'on');
hold(axes1,'off');
set(axes1,'FontSize',23,'FontName','Arial Narrow','XTick',[-0.6 0 0.4],'YTick',zeros(1,0));


%% H-meta Target Hit vs Target Miss
difference = Skittles_outcomee_target_hit - Skittles_outcome_target_miss  ;

hdi_difference = calc_HDI(difference(:));


%Get the values
fprintf(['\n HDI on difference in Mratio): ', num2str(hdi_difference)]);

figure
d= histfit(difference(:),120); %create a histogramm fit using the number of bins from previous step
delete(d(1))
d(2).Color = 'black';
hold on
xline(Rope_Skittles_OUTCOME(1),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xline(Rope_Skittles_OUTCOME(2),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xlim([-0.77, 0.5]);
line([hdi_difference(1); hdi_difference(2)],[30:50;30:50], 'Color','black','linestyle','-','LineWidth',8)
hold off




figure3 = figure;
% Create axes
axes1 = axes('Parent',figure3);
hold(axes1,'on');
histogram(difference(:),'DisplayStyle','stairs','FaceColor','none','EdgeColor','black', 'LineWidth',1.3)
xline(Rope_Skittles_OUTCOME(1),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
xline(Rope_Skittles_OUTCOME(2),'Color','black','linestyle',':','linestyle','--','LineWidth',1)
% xlim([-0.55, 0.45]);
% xlim([-0.55, 0.77]);
xlim([-0.77, 0.5]);
line([hdi_difference(1); hdi_difference(2)],[30:50;30:50], 'Color','black','linestyle','-','LineWidth',8)
box(axes1,'on');
hold(axes1,'off');
set(axes1,'FontSize',23,'FontName','Arial Narrow','XTick',[-0.6 0 0.4],'YTick',zeros(1,0));


