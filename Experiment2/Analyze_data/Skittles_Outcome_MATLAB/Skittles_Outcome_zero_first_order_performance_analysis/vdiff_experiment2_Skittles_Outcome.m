%% A script to create tables of performance (correct,confidence) and number of trials for exclusions and further analysis. Run the first part of the script (with the loop)to get 
%the tables and then step by step the next parts to see if you need to exclude further the participants. The last part creates the tables for vdiff to use in R.

clearvars; close all;
    experiment_folder = ['~' filesep 'Experiment1' filesep];
    data_path = [experiment_folder 'Run_experiment' filesep 'Results_Outcome' filesep];
    save_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data'];
    figure_path = [experiment_folder 'Analyze_data' filesep 'Slittles_figures'];
    

cd(save_path)

%% define participants
[subject_list] = (get_participant_name(data_path))'; %i have a function for that and i transpoze to meet my standrard script for analysis

subject_exclude=subject_list([8 end]); % subject_list(8) has only 1 block and subject_list(end) is a folder
subject_clean=subject_list;
subject_clean(ismember(subject_clean,subject_exclude))=[];

% create an empty table that will contain the data from all participants at tne end
Performance = cell2table(cell(0,24),'VariableNames',{'Participant','percent_correct','mean_confidence_same',...
    'std_confidence_same','mean_confidence_diff','std_confidence_diff','correct_same','correct_diff',...
    'correct_same_Target_hit','correct_same_Target_No_hit','correct_diff_Target_hit','correct_diff_Target_No_hit',...
    'mean_confidence_same_Target_hit','std_confidence_same_Target_hit','mean_confidence_same_Target_No_hit',...
    'std_confidence_same_Target_No_hit','mean_confidence_diff_Target_hit','std_confidence_diff_Target_hit',...
    'mean_confidence_diff_Target_No_hit','std_confidence_diff_Target_No_hit','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'});

Performance_correct = cell2table(cell(0,12),'VariableNames',{'Participant','percent_correct','correct_same','correct_diff','correct_same_Target_hit','correct_same_Target_No_hit','correct_diff_Target_hit','correct_diff_Target_No_hit','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'});

number_Trials = cell2table(cell(0,13),'VariableNames',{'Participant','n_same','n_diff','n_same_Target_hit','n_same_Target_No_hit',...
    'n_diff_Target_hit','n_diff_Target_No_hit', 'n_S1','n_S2','n_same_S1','n_same_S2','n_diff_S1','n_diff_S2'});

    
%% load data
for number_of_subj=1:length(subject_clean)
    
    fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
    load(sprintf('data_OUTCOME_%s.mat',subject_clean{number_of_subj}));
    Participant={subject_clean{number_of_subj}};
    
    same = condition_analysis == 's'; % The outcome was the same
    diff = condition_analysis == 'd'; % The outcome was different
  
    %overall performance
    percent_correct =(sum(correct_analysis==1)/trials_for_analysis)*100;
   
    % same correct
    correct_same = (sum(correct_analysis==1 & same)/sum(same))*100;
    correct_same_S1 = (sum(correct_analysis==1 & same & stimulus_analysis==0)/sum(same & stimulus_analysis==0))*100;
    correct_same_S2 = (sum(correct_analysis==1 & same & stimulus_analysis==1)/sum(same & stimulus_analysis==1))*100;
    
    correct_same_Target_hit = (sum(correct_analysis==1 & same & targetHit_analysis==1)/sum(same & targetHit_analysis==1))*100;
    correct_same_Target_No_hit = (sum(correct_analysis==1 & same & targetHit_analysis==0)/sum(same & targetHit_analysis==0))*100;
   
    % same confidence
    mean_confidence_same = mean(confidence_analysis(same)); %all
    std_confidence_same = std(confidence_analysis(same));
    
    mean_confidence_same_Target_hit = mean(confidence_analysis(same & targetHit_analysis==1)); %hit
    std_confidence_same_Target_hit = std(confidence_analysis(same & targetHit_analysis==1));
    
    mean_confidence_same_Target_No_hit = mean(confidence_analysis(same & targetHit_analysis==0)); % No hit
    std_confidence_same_Target_No_hit = std(confidence_analysis(same & targetHit_analysis==0));
        
    
    %different correct
    correct_diff = (sum(correct_analysis==1 & diff)/sum(diff))*100;
    correct_diff_S1 = (sum(correct_analysis==1 & diff & stimulus_analysis==0)/sum(diff & stimulus_analysis==0))*100;
    correct_diff_S2 = (sum(correct_analysis==1 & diff & stimulus_analysis==1)/sum(diff & stimulus_analysis==1))*100;
    
    correct_diff_Target_hit = (sum(correct_analysis==1 & diff & targetHit_analysis==1)/sum(diff & targetHit_analysis==1))*100;
    correct_diff_Target_No_hit = (sum(correct_analysis==1 & diff & targetHit_analysis==0)/sum(diff & targetHit_analysis==0))*100;
    
    % different confidence
    mean_confidence_diff = mean(confidence_analysis(diff));
    std_confidence_diff = std(confidence_analysis(diff));
    
    mean_confidence_diff_Target_hit = mean(confidence_analysis(diff & targetHit_analysis==1));
    std_confidence_diff_Target_hit = std(confidence_analysis(diff & targetHit_analysis==1));    
    
    mean_confidence_diff_Target_No_hit = mean(confidence_analysis(diff & targetHit_analysis==0));
    std_confidence_diff_Target_No_hit = std(confidence_analysis(diff & targetHit_analysis==0));    
    
    %number of trials for each condition
    n_S1=sum(stimulus_analysis==0);    % number of S1 trials
    n_S2=sum(stimulus_analysis==1);    % number of S2 trials
    n_same=sum(same);
    n_diff=sum(diff);
    n_same_S1=sum(same & stimulus_analysis==0);
    n_same_S2=sum(same & stimulus_analysis==1);
    n_diff_S1=sum(diff & stimulus_analysis==0);
    n_diff_S2=sum(diff & stimulus_analysis==1);
    n_same_Target_hit=sum(same & targetHit_analysis==1);
    n_same_Target_No_hit=sum(same & targetHit_analysis==0);
    
    n_diff_Target_hit=sum(diff & targetHit_analysis==1);
    n_diff_Target_No_hit=sum(diff & targetHit_analysis==0);
    
    %create  table for each participant
    performance = table(Participant,percent_correct,mean_confidence_same,std_confidence_same,mean_confidence_diff,...
        std_confidence_diff,correct_same,correct_diff,correct_same_Target_hit,correct_same_Target_No_hit,correct_diff_Target_hit,correct_diff_Target_No_hit,...
        mean_confidence_same_Target_hit,std_confidence_same_Target_hit,mean_confidence_same_Target_No_hit,std_confidence_same_Target_No_hit,mean_confidence_diff_Target_hit,...
        std_confidence_diff_Target_hit,mean_confidence_diff_Target_No_hit,std_confidence_diff_Target_No_hit, correct_same_S1,correct_same_S2,correct_diff_S1,correct_diff_S2);
    
    performance.Properties.VariableNames={'Participant','percent_correct','mean_confidence_same',...
        'std_confidence_same','mean_confidence_diff','std_confidence_diff','correct_same','correct_diff',...
        'correct_same_Target_hit','correct_same_Target_No_hit','correct_diff_Target_hit','correct_diff_Target_No_hit',...
        'mean_confidence_same_Target_hit','std_confidence_same_Target_hit','mean_confidence_same_Target_No_hit',...
        'std_confidence_same_Target_No_hit','mean_confidence_diff_Target_hit','std_confidence_diff_Target_hit',...
        'mean_confidence_diff_Target_No_hit','std_confidence_diff_Target_No_hit','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'};
    
    performance_correct = table(Participant,percent_correct,correct_same,correct_diff,correct_same_Target_hit,correct_same_Target_No_hit,correct_diff_Target_hit,correct_diff_Target_No_hit,correct_same_S1,correct_same_S2,correct_diff_S1,correct_diff_S2);
    
    performance_correct.Properties.VariableNames={'Participant','percent_correct','correct_same','correct_diff','correct_same_Target_hit','correct_same_Target_No_hit','correct_diff_Target_hit','correct_diff_Target_No_hit','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'};

    
    nbrOfTrials = table(Participant,n_same,n_diff,n_same_Target_hit,n_same_Target_No_hit,n_diff_Target_hit,n_diff_Target_No_hit,n_S1,n_S2,n_same_S1,n_same_S2,n_diff_S1,n_diff_S2);
    
    nbrOfTrials.Properties.VariableNames={'Participant','n_same','n_diff','n_same_Target_hit','n_same_Target_No_hit',...
        'n_diff_Target_hit','n_diff_Target_No_hit', 'n_S1','n_S2','n_same_S1','n_same_S2','n_diff_S1','n_diff_S2'};
    
    
    %concatenate data from all participants
    Performance = [Performance; performance];
    number_Trials = [number_Trials; nbrOfTrials];
    Performance_correct = [Performance_correct; performance_correct];
    
    save('Performance.mat', 'Performance','number_Trials','performance_correct');
    writetable(Performance,'Performance.csv')
    writetable(number_Trials,'number_Trials.csv')
end



%%  Find if there is participant with type 1 performance >80 or <60
% and then create the appropriate table to use in R
% load('Performance.mat') %load the mat file that has the performance from all the participants
index_exclude_performance=find(Performance.percent_correct>85 | Performance.percent_correct<60);
participant_exclude_perform_overall=Performance.Participant(index_exclude_performance); % exclude these participants because their ovarall performance fits exclusion criteria

index_exclude_performance_same= find(Performance.correct_same>85 |Performance.correct_same<60);  
index_exclude_performance_diff=find(Performance.correct_diff>85 |Performance.correct_diff<60);
index_exclude_performance_same_diff= [index_exclude_performance_same; index_exclude_performance_diff];

participant_exclude_perform_same_diff = unique(Performance.Participant(index_exclude_performance_same_diff)); %exclude those who have perform for both group without repeating their index



% create csv file for R analysis excluding those participants
per = Performance;
per(ismember(Performance.Participant,participant_exclude_perform_same_diff),:)=[]; %delete the rows based on participants to be excluded
per(ismember(per.Participant,participants_to_exclude_S_same_diff_separately),:)=[]; 
writetable(per,'Performance_28part.csv')





%Find if S1 and S2 were not equally represented and exclude these participan
% total=number_Trials.n_S1+number_Trials.n_S2; %number of trials
% s1_per=(number_Trials.n_S1./total)*100; %percent S1  The./ divides the element in each row pf one column with the other
% s2_per=(number_Trials.n_S2./total)*100;
% percent_traject=[s1_per s2_per];
% unequal_S= find(s1_per>60 | s1_per<40); %It is the same if i did this for S2
% part_excl_S=number_Trials.Participant(unequal_S);

% 
% total_same=number_Trials.n_same_S1+number_Trials.n_same_S2	; %number of trials
% s1_per_same =(number_Trials.n_same_S1./total_same)*100; %percent S1  The./ divides the element in each row pf one column with the other
% %s2_per=(number_Trials.n_same_S2./total_same)*100;
% unequal_S_same= find(s1_per_same>60 | s1_per_same<40); %It is the same if i did this for S2
% part_excl_S_same=number_Trials.Participant(unequal_S_same);
% 
% total_diff=number_Trials.n_diff_S1+number_Trials.n_diff_S2	; %number of trials
% s1_per_diff =(number_Trials.n_diff_S1./total_diff)*100; %percent S1  The./ divides the element in each row pf one column with the other
% unequal_S_diff= find(s1_per_diff>60 | s1_per_diff<40); %It is the same if i did this for S2
% part_excl_S_diff=number_Trials.Participant(unequal_S_diff);
% 
% participants_to_exclude_S1_same_diff_separately= unique([part_excl_S_same;  part_excl_S_diff])



total_same=number_Trials.n_same_S1+number_Trials.n_same_S2	; %number of trials
s1_per_same =(number_Trials.n_same_S1./total_same)*100; %percent S1  The./ divides the element in each row pf one column with the other
%s2_per=(number_Trials.n_same_S2./total_same)*100;
unequal_S_same= find(s1_per_same>70 | s1_per_same<30); %It is the same if i did this for S2
part_excl_S_same=number_Trials.Participant(unequal_S_same);

total_diff=number_Trials.n_diff_S1+number_Trials.n_diff_S2	; %number of trials
s1_per_diff =(number_Trials.n_diff_S1./total_diff)*100; %percent S1  The./ divides the element in each row pf one column with the other
unequal_S_diff= find(s1_per_diff>70 | s1_per_diff<30); %It is the same if i did this for S2
part_excl_S_diff=number_Trials.Participant(unequal_S_diff);

participants_to_exclude_S_same_diff_separately= unique([part_excl_S_same;  part_excl_S_diff]);

save('participants_exclusion.mat', 'participant_exclude_perform_overall', 'participant_exclude_perform_same_diff', 'participants_to_exclude_S_same_diff_separately')
% save('participants_exclusion.mat', 'participants_to_exclude', 'participant_exclude_perform_overall', 'participant_exclude_perform_same_diff', 'participants_to_exclude_S_same_diff_separately')

%% Target Hit based analysis (do not run with small dataset otherwise you will exclude all participants): 
% find participants that have few trials in same
% hit, sameNohit, diffHit, diffNohit
few_Target_hit_trials = find(number_Trials.n_diff_Target_hit<70 | number_Trials.n_diff_Target_No_hit < 70 ...
| number_Trials.n_same_Target_hit<70 | number_Trials.n_same_Target_No_hit < 70 );

% few_Target_hit_trials_100 = find(n_Trials_TargetHit.n_diff_Target_hit<100 | n_Trials_TargetHit.n_diff_Target_No_hit < 100 ...
% | n_Trials_TargetHit.n_same_Target_hit<100 | n_Trials_TargetHit.n_same_Target_No_hit < 100 );


participant_exclude_Target_Hit=performance_TargetHit.Participant(few_Target_hit_trials); % exclude
% participant_exclude_Target_Hit_100=performance_TargetHit.Participant(few_Target_hit_trials_100); % exclude

save('Performance_Target_Hit.mat', 'participant_exclude_Target_Hit','-append'); %include this info in mat file


%% create table for vdiff (use part of the first part of the script)
    
    vdiff = cell2table(cell(0,5),'VariableNames',{'Participant','vdiff_same','vdiff_diff','vdiff_stair_same','vdiff_stair_diff'});
    vdiff_same=vdiff_analysis(condition_analysis == 's');
    vdiff_diff=vdiff_analysis(condition_analysis == 'd');
    
    mean_vdiff_same=mean(abs(vdiff_same));
    mean_vdiff_diff=mean(abs(vdiff_diff));
      
    vdiff_stair_same=vdiff_staircase_analysis(condition_analysis == 's');
    vdiff_stair_diff=vdiff_staircase_analysis(condition_analysis == 'd');
    
    mean_vdiff_stair_same=mean(abs(vdiff_stair_same));
    mean_vdiff_stair_diff=mean(abs(vdiff_stair_diff));
    
    vdiff_sub = table(Participant,mean_vdiff_same,mean_vdiff_diff,mean_vdiff_stair_same,mean_vdiff_stair_diff);
    vdiff_sub.Properties.VariableNames={'Participant','vdiff_same','vdiff_diff','vdiff_stair_same','vdiff_stair_diff'};
    
    vdiff = [vdiff; vdiff_sub];
    
    writetable(vdiff,'vdiff_table.csv')
   
    
