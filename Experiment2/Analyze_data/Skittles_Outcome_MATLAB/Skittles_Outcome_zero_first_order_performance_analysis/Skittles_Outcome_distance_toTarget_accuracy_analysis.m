%% A script for transforming my data and excluding based on the criteria based on Skittles Outcome
% With this script i create the .csv that can be used to test how the
% outcome(distance to target) is afected by trial index


clearvars; close all;
experiment_folder = ['~' filesep 'Experiment2' filesep];
data_path = [experiment_folder 'Run_experiment' filesep 'Results_Outcome' filesep];
save_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_preprocessed'];
figure_path = [experiment_folder 'Analyze_data' filesep 'Slittles_figures'];

cd(data_path)

%% Define participants
load('participants_for_target_hmeta_separate_for_condition_OUTCOME_fromthe28_selected.mat')
subject_clean = participant_target_hit;

data_Outcome_distance= cell2table(cell(0,10),'VariableNames', {'id','trial','dist', 'correct','condition','confidence','stimulus',...
        'response','TargetHit', 'TargetHit_alt'});

    
%% load data
for number_of_subj= 1:length(subject_clean)
        
        fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
        if (strcmp(subject_clean{number_of_subj},'214XS'))
            load([data_path  subject_clean{number_of_subj} filesep 'EverythingSkittles' filesep 'SkittlesEverything_' subject_clean{number_of_subj} '.mat' ]);
        else
            load([data_path  subject_clean{number_of_subj} filesep 'SkittlesResult_' subject_clean{number_of_subj} '.mat' ]); %the data were saved in different file for this participant
        end
    
         pause(1)
       
    % Organise the variables in usable format
    response         =[];
    correct          =[];
    RT_type1         =[];
    RT_type2         =[];
    vdiff_staircase  =[];
    vdiff            =[];
    BRP_alt.v        =[];
    BRP.v            =[];
    condition        =[];
    unequalPosthit   =[];
    errorTrials      =[];
    stimulus         =[];
    confidence       =[];
    targetHit        =[];
    targetHit_alt    =[];
    check_target    =[];
    dist = [];
    for b = 1: length(resultData.type1)   %to loop among blocks
                if resultData.useDaq == 0 %means using mouse for report (run script from home)
                    response=[response resultData.type1(b).response]; % concatenates the result from every block in one line
                    response =(strcmp(response,'w')); %means 1 to the left 0 to the right
                else
                    response=[response resultData.type1(b).response]; % concatenates the result from every block in one line
                end
        correct=[correct resultData.type1(b).correct];
        RT_type1= [RT_type1 resultData.type1(b).RT];
        RT_type2= [RT_type2 resultData.type2(b).RT];
        vdiff_staircase=[vdiff_staircase resultData.type1(b).vdiff_staircase];
        vdiff=[vdiff resultData.type1(b).vdiff];
        condition=[condition resultData.block(b).condition];
        unequalPosthit=[unequalPosthit resultData.type1(b).unequalPosthit];
        errorTrials= [errorTrials resultData.type1(b).errorTrials];
        confidence=[confidence resultData.type2(b).conf];
        targetHit= [targetHit resultData.type1(b).targetHit]   ; % new addition
        targetHit_alt=[targetHit_alt resultData.type1(b).targetHit_alt];       
        check_target = [check_target resultData.type1(b).check ]; 
        dist = [dist  resultData.block(b).BRP.minDist ];
    end % here i stop looping among blocks and start refering to all the trials
    
    
     
  
    if numel(find(errorTrials==1))< (length(resultData.type1) * length(resultData.type1(1).correct))/10 % Participants excluded: more than 10% of trials they made a mistake in responding to the type-1 task
        comment{1}=('Procced with this Participant');
        
        %measure the rest now...
        % Set Stimulus
        stimulus = ones(size(vdiff)); %preallocate space for stimulus, to have the size of the total trials
        stimulus(vdiff<0) = 0;         % to the left probably later called S1: I made it that way because in trils2counts the naming does not match the fact that here response==1, means to the left
        response_corrected = 1-response; % i want response==0 meaning to the left, while in the script it is defined: resultData.type1(block).response(trial)     =leftTrajectorySelected
        
        %% test if LEFT stimulus and LEFT response are correctly registered : when participant was correct
   
        correct_test = stimulus==response_corrected; %use this as a sanity check for the responses registered as correct. i use 1-response because stimulus=0 means stimulus was left stim  response==1 means response was left
        correct2verify = find(correct~=correct_test);
        if    ~isempty(correct2verify)
            fprintf('\n******\n Trials to flip for participant %s\n******\n\n',subject_clean{number_of_subj});
            comment{2}=('PROBLEM with responses recorded');
        else
            fprintf('\n******\n NO trials to flip for participant %s\n******\n\n',subject_clean{number_of_subj});
            comment{2}=('NO problem with responses');
        end
        
        
        
          %% Test if target Hit/alt Hit corresponds with the condition
          % some time the same condition or diff condition is not saved corectly.
          % This is the case because in some instances due to the staircase we
          % had to select a vdiff that resulted in the condition different from
          % what was originally saved.
          % Find these trials and make the appropriate changes
           
          condition_same_real = targetHit == targetHit_alt; %find those trials they are same or different in terms of target hit
          condition_diff_real = targetHit ~= targetHit_alt; %find those trials they are same or different in terms of target hit
          
          same = condition == 's'; % The outcome was the same
          diff = condition == 'd'; % The outcome was different
          
          error_in_condition_registration_same = find(condition_same_real & ~same); %trials that while the outcome was same was not register as being in condition 'same'
          error_in_condition_registration_diff = find(condition_diff_real & ~ diff);             %trials that while the outcome was diff was not register as being in condition 'diff'
          
          if ~isempty(error_in_condition_registration_same) || ~isempty(error_in_condition_registration_diff)
              comment{7}=sprintf('TargetHit original had ERROR');
              
              %Find what happened in those trials
              % For those Not registered as same:
              if  ~isempty(error_in_condition_registration_same)
                  condition(error_in_condition_registration_same(ismember(check_target(error_in_condition_registration_same),[1 2 11 14]))) = 's';   %These check values created while run experiment indicate condition = 'same'
              end
              % For those Not registered as DIFF:
              if  ~isempty(error_in_condition_registration_diff)
                  condition(error_in_condition_registration_same(ismember(check_target(error_in_condition_registration_same),[5 6 13 12]))) = 'd';
              end
              
              comment{9}=sprintf('%d Trials had problem with same and %d Trials with diff',length(error_in_condition_registration_same),length(error_in_condition_registration_diff))
              comment{3}=sprintf('TargetHit properly saved');
              
          else
              comment{3}=sprintf('TargetHit was correct');
              comment{7}=sprintf('TargetHit original CORRECT'); % i added this here because otherwise comment(7) was always with error
              
          end
          
        
        %% Inspect trials to exlude
        %1) Trials will be excluded if the selected vdiff  is not the staircased vdiff
        %    and if because of that the trial is trivially easy or much
        %    too hard compared  to other trials of the same participant
        %    If vdiff_corrected ~=vdiff_staircase, is outside of the boundaries (2std), then exclude it
        
        % 1(i)Find if:  vdiff ~= vdiff_staircase
        vdiff_equals_staircase=ismembertol(abs(vdiff),abs(vdiff_staircase)); %gives the trials where vdiff_correct==vdiff_staircase
        vdiff_diff_staircase=~vdiff_equals_staircase;
        
        index_wrong_v=find(vdiff_diff_staircase);      %gives the index of trials where the vdiff used is not the staircased(see procedure)
        
        if ~isempty(index_wrong_v)
            
            index_wrong_v_same = find(vdiff_diff_staircase(condition=='s'));
            index_wrong_v_diff = find(vdiff_diff_staircase(condition=='d'));
            
            
            % 2(ii) get the lower and upper threshold for exclusion of trials based on vdiff
            %% NOW that we have 2 separate staircase one for same one for diff i need to exclude them based on the staircase of the respective condition
            % same
            vdiff_same = vdiff(condition=='s');
            vdiff_stair_same = vdiff_staircase(condition =='s');
         
            mean_vdiff_Last10_Values_same = movmean(abs(vdiff_same),[10 0]);%M = movmean(A,[kb kf]) computes the mean with a window of length kb+kf+1 that includes the element in the current position, kb elements backward, and kf elements forward
            std_vdiff_Last10_Values_same = movstd(abs(vdiff_same),[10 0]);
    
            upper_threshold_2std_same = abs(mean_vdiff_Last10_Values_same + (2*std_vdiff_Last10_Values_same));
            lower_threshold_2std_same = abs(mean_vdiff_Last10_Values_same - (2*std_vdiff_Last10_Values_same));
            
            vdiff_outside2std_same = upper_threshold_2std_same(vdiff_diff_staircase(condition=='s'))<=abs(vdiff_same(vdiff_diff_staircase(condition=='s')))|abs(lower_threshold_2std_same(vdiff_diff_staircase(condition=='s')))>=abs(vdiff_same(vdiff_diff_staircase(condition=='s'))); %gives logic output when this is true for the vdiff_diff_staircased
            
            index_wrong_v_include_same=index_wrong_v_same(~vdiff_outside2std_same);%gives the index of the trials that are inside 2STD
            index_wrong_v_exclude_same=index_wrong_v_same(vdiff_outside2std_same); %gives the index of the trials that are outside 2STD and these should be excluded!!
            index_wrong_v_exclude_same(index_wrong_v_exclude_same==1) = [] ;       %trial 1 has mean and limits equal to its value. make no sense to exclude
            
             % diff
            vdiff_diff = vdiff(condition =='d');
            vdiff_stair_diff = vdiff_staircase(condition =='d');
            mean_vdiff_Last10_Values_diff = movmean(abs(vdiff_diff),[10 0]);%M = movmean(A,[kb kf]) computes the mean with a window of length kb+kf+1 that includes the element in the current position, kb elements backward, and kf elements forward
            std_vdiff_Last10_Values_diff = movstd(abs(vdiff_diff),[10 0]);
    
            upper_threshold_2std_diff = abs(mean_vdiff_Last10_Values_diff + (2*std_vdiff_Last10_Values_diff)); %i use absolute values as i do the same with vdiff
            lower_threshold_2std_diff = abs(mean_vdiff_Last10_Values_diff - (2*std_vdiff_Last10_Values_diff));
            
            vdiff_outside2std_diff = upper_threshold_2std_diff(vdiff_diff_staircase(condition=='d'))<=abs(vdiff_diff(vdiff_diff_staircase(condition=='d')))|lower_threshold_2std_diff(vdiff_diff_staircase(condition=='d'))>=abs(vdiff_diff(vdiff_diff_staircase(condition=='d'))); %gives logic output when this is true for the vdiff_diff_staircased
            index_wrong_v_include_diff=index_wrong_v_diff(~vdiff_outside2std_diff);%gives the index of the trials that are inside 2STD
            index_wrong_v_exclude_diff=index_wrong_v_diff(vdiff_outside2std_diff);%gives the index of the trials that are outside 2STD and these should be excluded!!
            
        
            
            index_wrong_v_exclude_all = [index_wrong_v_exclude_diff index_wrong_v_exclude_same];
            problem_vdiff_staircased=zeros(size(vdiff)); %preallocate space to have the size of the total trials
            problem_vdiff_staircased(index_wrong_v_exclude_all)=1;% logic indexing for those who have very big or very small vdiff
            v_excluded=numel(index_wrong_v_exclude_all);
            
           
            
            cleanIndexes=find(RT_type1 <= 8 & RT_type1 >= 0.2 & ~errorTrials==1 & ~unequalPosthit==1 & ~problem_vdiff_staircased==1); % find the indexes of the trials to be included
            comment{4}=sprintf('%d Trials had random vdiff', numel(index_wrong_v));
            comment{5}=sprintf('%d of which were excluded', v_excluded);
            comment{6}=sprintf('%d Trials were included',numel(cleanIndexes));
        else
            cleanIndexes=find(RT_type1 <= 8 & RT_type1 >= 0.2 & ~errorTrials==1 & ~unequalPosthit==1);
            comment{4}=('No trials had random vdiff');
            comment{5}=('No trials had random vdiff');
            comment{6}=sprintf('%d Trials were included',numel(cleanIndexes));
        end
        
        
    else
        fprintf('\n******\n `Participant needs to be excluded: %s\n******\n\n',subject_clean{number_of_subj}); % exclude due to many errors
        comment{1}=('Participant to be excluded');
        
    end
    
    correct_analysis         = correct(cleanIndexes);
    response_analysis        = response_corrected(cleanIndexes);
    stimulus_analysis        = stimulus(cleanIndexes);
    condition_analysis       = condition(cleanIndexes);
    confidence_analysis      = confidence(cleanIndexes);
    trials_for_analysis      = numel(cleanIndexes); % How many trials will be finally included?
    targetHit_analysis       = targetHit(cleanIndexes);
    targetHit_alt_analysis   = targetHit_alt(cleanIndexes);
    vdiff_staircase_analysis = vdiff_staircase(cleanIndexes);
    vdiff_analysis           = vdiff(cleanIndexes);
    RT_type1_analysis        = RT_type1(cleanIndexes);
    RT_type2_analysis        = RT_type2(cleanIndexes);
    
    dist = dist(cleanIndexes);
    
    if (strcmp(subject_clean{number_of_subj},'312DEb')) %this is because the name of this part is larger than the other and creates problem with concatenate
        id = '312DE';
    elseif (strcmp(subject_clean{number_of_subj},'ABCD'))
        id = 'ABCD0' ;
    else
        id                       = subject_clean{number_of_subj};
    end
    id_column =  repmat(id,trials_for_analysis,1);
    trials = 1: trials_for_analysis;
    
  
    
   
    %create  table for each participant
    %table with less variales for R mixed anlaysis
    sub_data_distance = table(id_column,trials',dist',correct_analysis', condition_analysis', confidence_analysis',stimulus_analysis',...
       response_analysis',targetHit_analysis', targetHit_alt_analysis');
    
    sub_data_distance.Properties.VariableNames={'id','trial','dist', 'correct','condition','confidence','stimulus',...
        'response','TargetHit', 'TargetHit_alt'};

    pause(1)
    %concatenate data from all participants
    data_Outcome_distance = [data_Outcome_distance; sub_data_distance];
      
  
    fprintf('\n******\n Finished the analysis for participant %s\n******\n\n',subject_clean{number_of_subj});
    
   pause(1)
end % here i stop looping among participants

cd(save_path);  

%save('data_Outcome_distance_all.mat','data_Outcome_distance')
 %writetable(data_Outcome_distance,'data_Outcome_distance_all.csv')
fprintf('\n******\n Finished All the analyses \n******\n\n');



