%% A script to get participants name for using in the hmeta analysis(based on number of trials within condition)
% You just run the whole script.


clearvars; close all;

experiment_folder = ['~' filesep 'Experiment2' filesep];
data_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_raw' filesep];
save_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_preprocessed'];
figure_path = [experiment_folder 'Analyze_data' filesep 'Slittles_figures'];
cd(save_path)


save_data=0;
%%  Find if there is participant with type 1 performance >85 or <60 

% and then create the appropriate table to use in R
load('Performance.mat') %load the mat file that has the performance from all the participants
index_exclude_performance=find(Performance.correct_same>85|Performance.correct_same<60| Performance.correct_diff>85 | Performance.correct_diff<60);
participant_exclude_perform_same_diff = unique(Performance.Participant(index_exclude_performance)); %exclude those who have perform for both group without repeating their index


%% Find if S1 and S2 were not equally represented and exclude these participan (i skip for now)
% % I CHECK WITHIN SAME AND DIFFERENT. (overall i found no participant should be excluded but we used separate staircases for same & diff)
% 
total_same=number_Trials.n_same_S1+number_Trials.n_same_S2	; %number of trials
s1_per_same =(number_Trials.n_same_S1./total_same)*100; %percent S1  The./ divides the element in each row pf one column with the other
%s2_per=(number_Trials.n_same_S2./total_same)*100;
unequal_S_same= find(s1_per_same>70 | s1_per_same<30); %It is the same if i did this for S2
part_excl_S_same=number_Trials.Participant(unequal_S_same);

total_diff=number_Trials.n_diff_S1+number_Trials.n_diff_S2	; %number of trials
s1_per_diff =(number_Trials.n_diff_S1./total_diff)*100; %percent S1  The./ divides the element in each row pf one column with the other
unequal_S_diff= find(s1_per_diff>70 | s1_per_diff<30); %It is the same if i did this for S2
part_excl_S_diff=number_Trials.Participant(unequal_S_diff);

participants_to_exclude_S1_same_diff_separately= unique([part_excl_S_same;  part_excl_S_diff]);


%% Exclude & Create Tables for R
% Concatenate first 
participants_to_exclude=  unique([participant_exclude_perform_same_diff; participants_to_exclude_S1_same_diff_separately]);
participants_incl = number_Trials.Participant;
participants_incl(ismember(participants_incl, participants_to_exclude))=[];


n_trial_corrected_byperformance = number_Trials;
n_trial_corrected_byperformance(ismember(n_trial_corrected_byperformance.Participant,participants_to_exclude),:)=[];

%------   target miss -------------%
target_miss = n_trial_corrected_byperformance(:,[1 5 7]);

enough_Target_miss_trials = find(n_trial_corrected_byperformance.n_diff_Target_miss>=49 & n_trial_corrected_byperformance.n_same_Target_miss>=49 ); %we need more than 49 trials to have a reliable estimate
target_miss = target_miss(enough_Target_miss_trials,:);
target_miss.total =  target_miss.n_same_Target_miss +  target_miss.n_diff_Target_miss;
balanced_miss = (target_miss.n_same_Target_miss./target_miss.total)*100
target_miss_index = find(balanced_miss<70 & balanced_miss>30)



%------- target hit------------------%
target_hit = n_trial_corrected_byperformance(:,[1 4 6]);


enough_Target_hit_trials = find(n_trial_corrected_byperformance.n_diff_Target_hit>=49 & n_trial_corrected_byperformance.n_same_Target_hit>=49 );
target_hit = target_hit(enough_Target_hit_trials,:);
target_hit.total =  target_hit.n_same_Target_hit +  target_hit.n_diff_Target_hit;
balanced_hit =(target_hit.n_same_Target_hit./target_hit.total)*100
target_hit_index = find(balanced_hit<70 & balanced_hit> 30)


%--------SAME target------------%

same_target =  n_trial_corrected_byperformance(:,[1 4 5]);

enough_Target_same_trials = find(n_trial_corrected_byperformance.n_same_Target_miss>=49 & n_trial_corrected_byperformance.n_same_Target_hit>=49 );

same_target = same_target(enough_Target_same_trials,:);
same_target.total = same_target.n_same_Target_hit + same_target.n_same_Target_miss
balanced_same = (same_target.n_same_Target_hit./same_target.total)*100
same_target_index = find(balanced_same <70 & balanced_same >30  )


%--------DIFF target------------%
diff_target =  n_trial_corrected_byperformance(:,[1 6 7]);

enough_Target_diff_trials = find(n_trial_corrected_byperformance.n_diff_Target_miss>=49 & n_trial_corrected_byperformance.n_diff_Target_hit>=49 );
diff_target = diff_target(enough_Target_diff_trials,:);
diff_target.total = diff_target.n_diff_Target_hit + diff_target.n_diff_Target_miss
balanced_diff = (diff_target.n_diff_Target_hit./diff_target.total)*100
diff_target_index = find(balanced_diff < 70 & balanced_diff> 30)



participant_target_miss =participants_incl(target_miss_index); 
participant_target_hit =participants_incl(target_hit_index);


participant_same_target = participants_incl(same_target_index);

participant_diff_target = participants_incl(diff_target_index)

if save_data==1
    save('participants_for_target_hmeta_separate_for_condition_OUTCOME_fromthe28_selected.mat', 'participant_target_miss','participant_target_hit','participant_same_target','participant_diff_target');
end
 
    

