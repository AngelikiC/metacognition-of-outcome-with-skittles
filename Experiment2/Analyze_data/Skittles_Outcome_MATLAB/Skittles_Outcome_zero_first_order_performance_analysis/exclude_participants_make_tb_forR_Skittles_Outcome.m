%% A script to exclude participants based on the exclusion criterian set  
% You just run the whole script.




clearvars; close all;
experiment_folder = ['~' filesep 'Experiment2' filesep];
data_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_raw' filesep];
save_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_preprocessed'];
figure_path = [experiment_folder 'Analyze_data' filesep 'Slittles_figures'];



cd(save_path)

%%  Find if there is participant with type 1 performance >80 or <60 ()

% and then create the appropriate table to use in R
load('Performance.mat') %load the mat file that has the performance from all the participants
% I will exclude based on condition since we used 2 staircase 
index_exclude_performance_same= find(Performance.correct_same>85 |Performance.correct_same<60);  
index_exclude_performance_diff=find(Performance.correct_diff>85 |Performance.correct_diff<60);
index_exclude_performance_same_diff= [index_exclude_performance_same; index_exclude_performance_diff];

participant_exclude_perform_same_diff = unique(Performance.Participant(index_exclude_performance_same_diff)); %exclude those who have perform for both group without repeating their index




%% Find if S1 and S2 were not equally represented and exclude these participan
% I CHECK WITHIN SAME AND DIFFERENT. (overall i found no participant should be excluded but we used separate staircases for same & diff)

total_same=number_Trials.n_same_S1+number_Trials.n_same_S2	; %number of trials
s1_per_same =(number_Trials.n_same_S1./total_same)*100; %percent S1  The./ divides the element in each row pf one column with the other
%s2_per=(number_Trials.n_same_S2./total_same)*100;
unequal_S_same= find(s1_per_same>70 | s1_per_same<30); %It is the same if i did this for S2
part_excl_S_same=number_Trials.Participant(unequal_S_same);

total_diff=number_Trials.n_diff_S1+number_Trials.n_diff_S2	; %number of trials
s1_per_diff =(number_Trials.n_diff_S1./total_diff)*100; %percent S1  The./ divides the element in each row pf one column with the other
unequal_S_diff= find(s1_per_diff>70 | s1_per_diff<30); %It is the same if i did this for S2
part_excl_S_diff=number_Trials.Participant(unequal_S_diff);

participants_to_exclude_S1_same_diff_separately= unique([part_excl_S_same;  part_excl_S_diff]);


%% Exclude & Create Tables for R
participants_to_exclude= unique([participant_exclude_perform_same_diff;  participants_to_exclude_S1_same_diff_separately] );

% Create csv file for R analysis excluding those participants
performance_for_R = Performance;
performance_for_R(ismember(Performance.Participant,participants_to_exclude),:)=[]; %delete the rows based on participants to be excluded

number_Trials_for_R = number_Trials;
number_Trials_for_R(ismember(Performance.Participant,participants_to_exclude),:)=[];


writetable(performance_for_R,'Performance_Skittles_AoM_forR.csv')



%% Target Hit based analysis (do not run with small dataset otherwise you will exclude all participants): 
% find participants that have few trials in same
% hit, sameNohit, diffHit, diffNohit
few_Target_hit_trials = find(number_Trials.n_diff_Target_hit<70 | number_Trials.n_diff_Target_miss < 70 ...
| number_Trials.n_same_Target_hit<70 | number_Trials.n_same_Target_miss < 70 );


participant_exclude_from_Target_hit_analys = number_Trials.Participant(few_Target_hit_trials);

save('participants_exclusion.mat','participant_exclude_perform_same_diff', 'participant_exclude_from_Target_hit_analys','participants_to_exclude_S1_same_diff_separately');


%% Create table for vdiff same diff
%OUTCOME
vdiff = readtable('vdiff_table_outcome.csv');
 load('participants_exclusion.mat')
 participants_to_exclude_OUTCOME = unique([participant_exclude_perform_same_diff;participants_to_exclude_S1_same_diff_separately])
vdiff(ismember(vdiff.Participant,participants_to_exclude_OUTCOME),:)=[];
% writetable(vdiff,'vdiff_included_only_part_Skittles_AoM_forR.csv')
writetable(vdiff,'vdiff_included_only_28part_Skittles_OUTCOME_forR.csv')

%AoM
vdiff = readtable('vdiff_table.csv');
 load('participants_exclusion.mat')
 participants_to_exclude_AOM = unique([participant_exclude_perform_overall ;part_excl_S])
vdiff(ismember(vdiff.Participant,participants_to_exclude_AOM),:)=[];
% writetable(vdiff,'vdiff_included_only_part_Skittles_AoM_forR.csv')
writetable(vdiff,'vdiff_included_only_37part_Skittles_AoM_forR.csv')
    
 
 
    

