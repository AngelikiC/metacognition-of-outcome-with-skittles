
%% A script to pool vdiff values and create taske diffuculty illustration
% If you run this script you will get the figure with both histogramm plots
% included.
% AoM refers to experiment 1 and Outcome to Experiment2
%% Load data frome Experiment 2
clearvars; %close all;
experiment2c = [0 0.6 0.6];
experiment1c = [1 0.650980392156863 0];%[255 166 0];
grey = [[0.670588235294118 0.654901960784314 0.654901960784314]];

experiment_folder = [filesep '~' filesep 'Experiment2' filesep];
data_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_raw' filesep];
save_path = [experiment_folder 'Analyze_data' filesep 'Skittles_data_Outcome' filesep 'Data_Skittles_Outcome_preprocessed'];
figure_path = [experiment_folder 'Analyze_data' filesep 'Slittles_figures'];


cd(save_path)
% define participants experiment 2
[subject_list] = (get_participant_name(data_path))'; %i have a function for that and i transpoze to meet my standrard script for analysis
load('participants_exclusion.mat')
%i have a function for that and i transpoze to meet my standrard script for analysis
subject_exclude=subject_list([8 end]); % subject_list(8) has only 1 block and subject_list(end) is a folder
subject_clean=subject_list;
subject_clean(ismember(subject_clean,subject_exclude))=[];

% define if you want to exclude someone from previous analysis
subject_exclude_extra= [participant_exclude_perform_same_diff; participants_to_exclude_S1_same_diff_separately] ; 
subject_clean(ismember(subject_clean,subject_exclude_extra))=[];

load('data_Outcome_all.mat')


data_Outcome(ismember(data_Outcome.id,subject_exclude_extra),:)=[];
data_Outcome(13016:end,:)=[]; %i did this manualy as one participant who is to be excluded is named ABCD0 and this was the fastest



%% Skittles Experiment 01

experiment_folder = ['~' filesep  'Experiment2' filesep];
data_path = [experiment_folder 'Data_Skittles_AoM' filesep 'Data_Skittles_AoM_raw' filesep];
preprocessed_data_path = [experiment_folder 'Data_Skittles_AoM' filesep 'Data_Skittles_AoM_preprocessed'];
save_path = preprocessed_data_path;
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];


cd(preprocessed_data_path)


% define participants experiment 1
load('participants_after_exclusion_is_done.mat')
subject_clean_experiment_1 = participant_MLE;


load('data_Skittles_AoM_all.mat')
data_AoM_34 = data_AoM(ismember(data_AoM.id,subject_clean_experiment_1),:);
%velocity_AoM = abs(data_AoM_34.vdiff);


%% Now define vdiff for each condition:
% create vdiff Incongruent Condition:
velocity_diff_experiment2 = abs(data_Outcome.vdiff(data_Outcome.condition=='d'));
velocity_diff_experiment1 = abs(data_AoM_34.vdiff(data_AoM_34.condition=='d'));


% create vdiff Congruent Condition:
velocity_same_experiment2 = abs(data_Outcome.vdiff(data_Outcome.condition=='s'));
velocity_same_experiment1 = abs(data_AoM_34.vdiff(data_AoM_34.condition=='s'));

% Pool data for each condition:
velocity_pooled_diff = [velocity_diff_experiment1;velocity_diff_experiment2];
velocity_pooled_same = [velocity_same_experiment1;velocity_same_experiment2];

%% Plot one histogram for each condition:

Supplementary_velocity_pool_createfigure(velocity_pooled_diff,grey)
xlim([0, 2]);


Supplementary_velocity_pool_createfigure(velocity_pooled_same,grey)
xlim([0, 2]);

