%% This sctipt is meant for illustration purposes only. Simply load the data from one participant and then
% you can estimate max min med vdiff and then use the script 'plot_trajectories_Skittles_outcome.m'
% to plot them. The distance between the two trajectories defines the task
% difficulty. You need to have psychotoolbox installed.
% Written by Angeliki Charalampaki Nov. 2021.

%% Specify condition and vdiff magnitude you want to plot:

v_magnitude     = 1; % what difference do you want to see? 1 maximum, 0 minimum
cond            ='s';% which condition do you want to plot: 's' means congruent, 'd' means incongruent
target_hit      = 1; % do you want example of trajectories when target was hit or not? 

%% Specify data path and load relevant variables:
data_path = (   '~/Experiment2/Analyze_data/Skittles_data_Outcome/Data_Skittles_Outcome_preprocessed');
cd(data_path);
data_raw = '~/Experiment2/Analyze_data/Skittles_data_Outcome/Data_Skittles_Outcome_raw';

load([data_path filesep 'participants_for_target_hmeta_separate_for_condition_OUTCOME_fromthe28_selected.mat'])
subject_clean = participant_target_hit;

load([data_path filesep 'data_Outcome_all.mat']);
data_use = data_Outcome(ismember(data_Outcome.id,subject_clean),:); %get the values from participants you used. If you need to check: unique(data_use(:,1))



%% Find the max and min velocity difference used and then use that to get the participant that has these value:
data_use_same = data_use(ismember(data_use.condition,'s'),:);
data_use_diff = data_use(ismember(data_use.condition,'d'),:);

data_use_same_miss =data_use_same((data_use_same.TargetHit==0),:);
data_use_same_hit   =data_use_same((data_use_same.TargetHit==1),:);

data_use_diff_miss =data_use_diff((data_use_diff.TargetHit==0),:);
data_use_diff_hit   =data_use_diff((data_use_diff.TargetHit==1),:);




%% I need to load the raw data that included BRP trajectories info
cd(data_raw)
if v_magnitude==1 && cond=='d' &&  target_hit==0
    % MAX DIFF MISS
    [diff_max_vdval,diff_max_vidx] = max(abs(data_use_diff_miss.vdiff));
    id_diff_max = data_use_diff_miss{diff_max_vidx,1};
    
    load([id_diff_max filesep 'Skittlesblock_4_' id_diff_max '.mat'])

elseif v_magnitude==1 && cond=='d' &&  target_hit==1
    % MAX DIFF HIT
    [diff_max_vdval,diff_max_vidx] = max(abs(data_use_diff_hit.vdiff));
    id_diff_max = data_use_diff_hit{diff_max_vidx,1};
    
    load([id_diff_max filesep 'Skittlesblock_4_' id_diff_max '.mat'])

elseif   v_magnitude==0 && cond=='d' && target_hit==0
    % MIN DIFF miss
    [diff_min_vdval,diff_min_vidx] = min(abs(data_use_diff_miss.vdiff));
    id_diff_min = data_use_diff_miss{diff_min_vidx,1};

    load ([id_diff_min filesep 'Skittlesblock_4_' id_diff_min '.mat'])
    
elseif v_magnitude==0 && cond=='d' && target_hit==1
    % MIN DIFF hit
    [diff_min_vdval,diff_min_vidx] = min(abs(data_use_diff_hit.vdiff));
    id_diff_min = data_use_diff_hit{diff_min_vidx,1};

    load ([id_diff_min filesep 'Skittlesblock_4_' id_diff_min '.mat'])

elseif v_magnitude==1 && cond=='s' && target_hit==1
    %MAX SAME
    [same_max_vdval,same_max_vidx] = max(abs(data_use_same_hit.vdiff));
    id_same_max = data_use_same_hit{same_max_vidx,1};
   
    load ([id_same_max filesep 'Skittlesblock_4_' id_same_max '.mat'])

elseif v_magnitude==1 && cond=='s' && target_hit==0
    %MAX SAME
    [same_max_vdval,same_max_vidx] = max(abs(data_use_same_miss.vdiff));
    id_same_max = data_use_same_miss{same_max_vidx,1};
    load ([id_same_max filesep 'Skittlesblock_4_' id_same_max '.mat'])

elseif v_magnitude==0 && cond=='s' && target_hit==1
    % MIN SAME
    [same_min_vdval,same_min_vidx] = min(abs(data_use_same_hit.vdiff));
    id_same_min = data_use_same_hit{same_min_vidx,1};
    
    load ([id_same_min filesep 'Skittlesblock_4_' id_same_min '.mat'])

elseif v_magnitude==0 && cond=='s' && target_hit==0
    % MIN SAME
    [same_min_vdval,same_min_vidx] = min(abs(data_use_same_miss.vdiff));
    id_same_min = data_use_same_miss{same_min_vidx,1};
    
    load ([id_same_min filesep 'Skittlesblock_4_' id_same_min '.mat'])

elseif v_magnitude == 2 && cond =='d'
    % Median DIFF
    diff_med_vdval = median(abs(data_use_diff.vdiff));
    diff_med_vidx = find(abs(data_use_diff.vdiff)==diff_med_vdval,1,'first');
    id_diff_med = data_use_diff{diff_med_vidx,1};

    load ([id_diff_med filesep 'Skittlesblock_4_' id_diff_med '.mat'])

elseif v_magnitude == 2 && cond =='s'
    % Median SAME
    same_med_vdval = median(abs(data_use_same.vdiff));
    same_med_vidx = find(abs(data_use_same.vdiff)==same_med_vdval,1,'first');
    id_same_med = data_use_same{same_med_vidx,1};
    
    load ([id_same_med filesep 'Skittlesblock_4_' id_same_med '.mat'])
end

%clearvars -except resultData v_magnitude cond target_hit
% Get all trials data
debug  = 1;
vdiff=[];
releaseSpeed =[];
releaseSpeed_alt =[];
releaseAngle =[];
tSize =[];
BRP =[];
BRP_alt =[];
condition = [];
targetHit = [];
  for b=1:length(resultData.block)
      vdiff=[vdiff resultData.type1(b).vdiff];
      releaseSpeed =[releaseSpeed resultData.block(b).releaseSpeed];
      releaseSpeed_alt =[releaseSpeed_alt resultData.block(b).releaseSpeed_alt];
      releaseAngle =[releaseAngle resultData.block(b).releaseAngle  ];
      tSize =[tSize resultData.type1(b).tSize];
      BRP =[BRP resultData.block(b).BRP];
      BRP_alt =[BRP_alt resultData.block(b).BRP_alt];
      condition = [condition resultData.block(b).condition];
      targetHit = [targetHit resultData.type1(b).targetHit];
  end
    %% Plot the scene:
        % Minimum 
        if v_magnitude == 0
            if strcmp(cond,'d')
                if target_hit==1
                    t=1;
                    diff_vdiff_low_index    = find(abs(vdiff)==diff_min_vdval & condition=='d' & targetHit==1);
                else
                    t=1;
                    diff_vdiff_low_index    = find(abs(vdiff)==diff_min_vdval & condition=='d' & targetHit==0);%to get a vdiff min
                end
                
                value_vdiff_min_diff    = vdiff(diff_vdiff_low_index(t))
                speed_diff_min         = releaseSpeed(diff_vdiff_low_index(t));
                speed_diff_alt_min      = releaseSpeed_alt(diff_vdiff_low_index(t));
                angle_diff_min          = releaseAngle(diff_vdiff_low_index(t));
                target_size_diff_min    = tSize(diff_vdiff_low_index(t));
                BRP_diff_real_min      = BRP(diff_vdiff_low_index(t));
                BRP_diff_alt_min        = BRP_alt(diff_vdiff_low_index(t));
           
            elseif strcmp(cond,'s')
                if target_hit==1
                    t=1;
                    same_vdiff_low_index    = find(abs(vdiff)==same_min_vdval  & condition=='s' & targetHit==1);%to get a vdiff

                else
                        t=1;
                    same_vdiff_low_index    = find(abs(vdiff)==same_min_vdval  & condition=='s' & targetHit==0);%to get a vdiff

                end
                value_vdiff_min_same    = vdiff(same_vdiff_low_index(t))
                speed_same_min          = releaseSpeed(same_vdiff_low_index(t));
                speed_same_alt_min      = releaseSpeed_alt(same_vdiff_low_index(t));
                angle_same_min          = releaseAngle(same_vdiff_low_index(t));
                target_size_same_min	= tSize(same_vdiff_low_index(t));                
                BRP_same_real_min       = BRP(same_vdiff_low_index(t));
                BRP_same_alt_min        = BRP_alt(same_vdiff_low_index(t));
            end
            
             % Maximum
        elseif v_magnitude == 1
           
            if strcmp(cond,'d') 
                if target_hit==1
                    diff_vdiff_MAX_index    = find(abs(vdiff)==diff_max_vdval & condition=='d' & targetHit==1);%to get a vdiff min
                else
                    diff_vdiff_MAX_index    = find(abs(vdiff)==diff_max_vdval & condition=='d'& targetHit==0);%to get a vdiff min

                end
                    
                value_vdiff_max         = vdiff(diff_vdiff_MAX_index)
                speed_diff_max          = releaseSpeed(diff_vdiff_MAX_index);
                speed_diff_alt_max      = releaseSpeed_alt(diff_vdiff_MAX_index);
                angle_diff_max          = releaseAngle(diff_vdiff_MAX_index);
                target_size_diff_max    = tSize(diff_vdiff_MAX_index);
                BRP_diff_real_max       = BRP(diff_vdiff_MAX_index); 
                BRP_diff_alt_max        = BRP_alt(diff_vdiff_MAX_index);

       
            elseif strcmp(cond,'s')
                
                if target_hit==1
                    t=1;
                    same_vdiff_MAX_index    = find(abs(vdiff)==same_max_vdval & condition=='s' & targetHit==1);%to get a vdiff
                else
                    t=1;
                    same_vdiff_MAX_index    = find(abs(vdiff)==same_max_vdval & condition=='s' & targetHit==0);%to get a vdiff
                end
                value_vdiff_max_same    = vdiff(same_vdiff_MAX_index(t)) % I used the second value that matches the vdiff_max from diff con
                speed_same_max          = releaseSpeed(same_vdiff_MAX_index(t));
                speed_same_alt_max      = releaseSpeed_alt(same_vdiff_MAX_index(t));
                angle_same_max          = releaseAngle(same_vdiff_MAX_index(t));
                target_size_same_max    = tSize(same_vdiff_MAX_index(t));
                BRP_same_real_max       = BRP(same_vdiff_MAX_index(t));
                BRP_same_alt_max        = BRP_alt(same_vdiff_MAX_index(t));
            end
            
             % Median
        elseif  v_magnitude == 2
           
             if strcmp(cond,'d') 
                if target_hit==1
                    t=1;
                    diff_vdiff_MED_index    = find(abs(vdiff)==diff_med_vdval & condition=='d' & targetHit==1);%to get a vdiff min
                else
                    t=1;
                    diff_vdiff_MED_index    = find(abs(vdiff)==diff_med_vdval & condition=='d'& targetHit==0);%to get a vdiff min

                end
                    
                value_vdiff_med         = vdiff(diff_vdiff_MED_index(t))
                speed_diff_med          = releaseSpeed(diff_vdiff_MED_index(t));
                speed_diff_alt_med      = releaseSpeed_alt(diff_vdiff_MED_index(t));
                angle_diff_med          = releaseAngle(diff_vdiff_MED_index(t));
                target_size_diff_med    = tSize(diff_vdiff_MED_index(t));
                BRP_diff_real_med       = BRP(diff_vdiff_MED_index(t)); 
                BRP_diff_alt_med        = BRP_alt(diff_vdiff_MED_index(t));

       
            elseif strcmp(cond,'s')
                
                if target_hit==1
                     t=1;
                    same_vdiff_MED_index    = find(abs(vdiff)==same_med_vdval & condition=='s' & targetHit==1);%to get a vdiff
                else
                     t=1;
                    same_vdiff_MED_index    = find(abs(vdiff)==same_med_vdval & condition=='s' & targetHit==0);%to get a vdiff
                end
                value_vdiff_med_same    = vdiff(same_vdiff_MED_index(t)) % I used the second value that matches the vdiff_med from diff con
                speed_same_med          = releaseSpeed(same_vdiff_MED_index(t));
                speed_same_alt_med      = releaseSpeed_alt(same_vdiff_MED_index(t));
                angle_same_med          = releaseAngle(same_vdiff_MED_index(t));
                target_size_same_med    = tSize(same_vdiff_MED_index(t));
                BRP_same_real_med       = BRP(same_vdiff_MED_index(t));
                BRP_same_alt_med        = BRP_alt(same_vdiff_MED_index(t));
            end
        end
        
        
    
    %% Now plot based on the condition you want:
    if cond =='d'
        if v_magnitude == 0
                plot_trajectories_Skittles_outcome( BRP_diff_real_min, BRP_diff_alt_min,target_size_diff_min)
            
        elseif v_magnitude == 1
            plot_trajectories_Skittles_outcome( BRP_diff_real_max, BRP_diff_alt_max,0.05)%target_size_diff_max)
       
        elseif v_magnitude == 2
            plot_trajectories_Skittles_outcome( BRP_diff_real_med, BRP_diff_alt_med,target_size_diff_med)
        end
    end

    if cond =='s'
        if v_magnitude == 0
            plot_trajectories_Skittles_outcome(BRP_same_real_min, BRP_same_alt_min,target_size_same_min)
        
        elseif v_magnitude == 1
            plot_trajectories_Skittles_outcome(BRP_same_real_max, BRP_same_alt_max,0.05)%target_size_same_max)
        
        elseif v_magnitude == 2
            plot_trajectories_Skittles_outcome(BRP_same_real_med, BRP_same_alt_med,target_size_same_med)
        end
    end



        
        
