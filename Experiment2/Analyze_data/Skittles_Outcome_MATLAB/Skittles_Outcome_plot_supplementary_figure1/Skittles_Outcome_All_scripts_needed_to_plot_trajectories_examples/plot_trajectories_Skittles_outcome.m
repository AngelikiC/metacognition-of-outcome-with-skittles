function plot_trajectories_Skittles_outcome(BRP, BRP_alt,target_size)

color_real      = [0 0 0];
color_alt       =  [0 0.6 0.6];

 
 
SkitSet         = set_SkitSet_outcome;
bg              = 1;
grey            = round(255*[bg bg bg]);
SkitSet.bg_color= grey;
% elbowYcoordsPixels = 729;

%% Specify some parameters
Screen('Preference', 'SkipSyncTests', 1); % 1 or 2->without testing
debuggingRect = [0 0 800 800];

InitializeMatlabOpenGL( [], [], [], 0 );
[win , winRect] = PsychImaging( 'OpenWindow', 0, grey, debuggingRect, [], [], 0, 0 );
Screen( 'BeginOpenGL', win );

screenAspectRatio = winRect( 4 ) / winRect( 3 );
winCenter.x = winRect(3)/2;
winCenter.y = winRect(4)/2;
glEnable( GL.LIGHTING );
glEnable( GL.LIGHT0 );
glEnable( GL.DEPTH_TEST );
glMatrixMode( GL.PROJECTION );
glLoadIdentity;
gluPerspective( 25, 1 / screenAspectRatio, 0.1, 100 );
glMatrixMode( GL.MODELVIEW );
glLoadIdentity;
glLightfv( GL.LIGHT0,GL.POSITION, [ 0 0 1 0 ]);
gluLookAt( 0, 0, 8, 0, 0, 0, 0, 1, 0);
glClearColor( bg,bg,bg, 0 );
Screen( 'EndOpenGL', win );


%% Now plot the trajectories:
Screen( 'BeginOpenGL', win );
glClear;

% Draw lever
glPushMatrix;
glTranslatef( SkitSet.xLever, SkitSet.yLever, 0 );
glRotatef( -BRP.angle, 0.0, 0.0, 1.0 ); %%! i changed this
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.0 0.0 0.0 0 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.0 0.0 0.0 0 ] );
glRectf( -SkitSet.LeverLength, -SkitSet.LeverWidth / 2, 0, SkitSet.LeverWidth / 2 );
glPopMatrix;

%Draw target
glPushMatrix;
[ x, y ] = target_position_type1_illustration;
glTranslatef( x, y, 0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.6 0.1 0.0 0 ] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [ 0.7 0.0 0.0 0 ] );
glutSolidSphere( target_size , 100, 100 );
glPopMatrix;


% Draw center post
glPushMatrix;
glTranslatef( SkitSet.xCenter, SkitSet.yCenter, 0 );
glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [0 0 1 1] );
glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, [0 0 1 1] );
glutSolidCone( SkitSet.CenterRadius, 0.5, 100, 100 );
glPopMatrix;


%% Draw trajectories
% Get and draw actual ball trajectory vector
crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
crossBeforePost.index_x = Inf;

if isnan(BRP.posthit) || BRP.posthit == 0||BRP.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
    crossBehindPost = BRP.crossBehindPost;
else
    [~, crossBeforePost.index_x] = min(abs(BRP.x_posthit-BRP.trajectory.x));
    [~, crossBeforePost.index_y] = min(abs(BRP.y_posthit-BRP.trajectory.y));
    
end
firstVerticalCrossing = min(crossBehindPost, crossBeforePost.index_x);

% same procedure for alternative trajectory
crossBehindPost = Inf;      %initialize these high: we'll search for the minimum
crossBeforePost.index_x = Inf;

if isnan(BRP_alt.posthit) || BRP_alt.posthit == 0||BRP_alt.posthitAfterCross %BRP.posthit set to 0 in calculate_release_params
    crossBehindPost = BRP_alt.crossBehindPost;
else
    [~, crossBeforePost.index_x] = min(abs(BRP_alt.x_posthit-BRP_alt.trajectory.x));
    [~, crossBeforePost.index_y] = min(abs(BRP_alt.y_posthit-BRP_alt.trajectory.y));
    
end
firstVerticalCrossing_alt = min(crossBehindPost, crossBeforePost.index_x);

% Real trajectory
%if i wanted dotted: for trajectoryPoint = 1 : 10 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 10 points

for trajectoryPoint = 1 :30 : firstVerticalCrossing  %length(BRP.trajectory.time) %plot trajectory every 30 points
    
    glPushMatrix;
    glTranslatef( BRP.trajectory.x(trajectoryPoint), BRP.trajectory.y(trajectoryPoint), 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, [ 0.1 0.6 0.0 0 ] );
    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_real );
    glutSolidSphere(0.021, 100, 100 ); %changes the thickness of line
    %glutSolidSphere( 0.01, 100, 100 ); %original values

    glPopMatrix;
end

%% Get and draw alternative ball trajectory vector

for trajectoryPoint = 1 : firstVerticalCrossing_alt  %plot trajectory every 10 points
    %if i wanted dotted:for trajectoryPoint = 1 : 10 : firstVerticalCrossing_alt  %plot trajectory every 10 points
    
    glPushMatrix;
    glTranslatef( BRP_alt.trajectory.x(trajectoryPoint), BRP_alt.trajectory.y(trajectoryPoint), 0 );
    glMaterialfv( GL.FRONT_AND_BACK,GL.AMBIENT, color_alt );

    glMaterialfv( GL.FRONT_AND_BACK,GL.DIFFUSE, color_alt );
    glutSolidSphere( 0.021, 130, 100 );
    glPopMatrix;
end

Screen( 'EndOpenGL', win );
Screen( 'Flip', win, [], [], 0 );
% pause(0.5)
end