%%edited Angeliki Charalampaki Feb 20221
% A script tp create a landscape plot. First go to the directory your raw
% data are and then load a dataset from one participant. It does not matter
% who you upload as the landscape is common for all.

e.g.
load('Skittlesblock_X_XX.mat')

SkitSet = resultData.SkitSet;

[distanceToTarget, angleRange, speedRange, targetHit] = get_Landscape_outcome_for_analysis(SkitSet);

plotLandscape_outcome_analysis(SkitSet, angleRange, speedRange, distanceToTarget, targetHit)