%% edited by Angeliki Charalampaki February 2021
function [distanceToTarget, angleRange, speedRange, targetHit_landscape] = get_Landscape_outcome_for_analysis(SkitSet)

collisionFlagValue = NaN; 

angleRange = 0:1:180; %from 0 to 180 it makes more sense but for now we just want to replicate their plots
speedRange = 0:.1:6; %This is in m/sec, as in Mueller & Sternad 2004 (JEP: HPP)

% speedRange for minimal vdiff  
% speedRange_minvdiff = speedRange + 0.01;

% speedRange for maximal vdiff  
% speedRange_maxvdiff = speedRange + 1.96;

[releaseAngle,releaseSpeed] = meshgrid(angleRange,speedRange);
%preallocate
distanceToTarget = zeros(size(releaseAngle));


s = 0;
for speed = speedRange
    a = 0;
    s = s+1;
    for angle = angleRange
        a = a+1;

        BRP = calculate_release_parameters_toPlot_outcome(angle, speed, SkitSet, collisionFlagValue);
        
        distanceToTarget(s,a) = BRP.minDist;
        
        targetHit_landscape(s,a) = target_hit_outcome_analysis(BRP,SkitSet); 

    end
end





% From Mueller & Sternad 2004 (JEP: HPP)
% The two branches pertain to the two major angle ranges in which the ball 
% can be hit. The two branches are not symmetrical because at 90? the 
% distance to the center post is smaller than it is at 90?.
