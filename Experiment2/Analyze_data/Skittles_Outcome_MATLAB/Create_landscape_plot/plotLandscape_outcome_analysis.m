function plotLandscape_outcome_analysis(SkitSet, angleRange, speedRange, distanceToTarget, targetHit)

imagesc(angleRange, speedRange, 100*distanceToTarget);
ax = gca;
ax.YDir = 'normal';
xlabel('release Angle [deg]','FontSize', 17)
ylabel('speed [m/sec]','FontSize', 17)
c = colorbar;
%ylabel(c, 'min distance to target [cm]')
%title(['x = ' num2str(SkitSet.xTarget *100) ', y = ' num2str(SkitSet.yTarget *100) ])
colormap(flipud(gray))
%overlay a white line if the 
hold on
hitHandle = imagesc(angleRange, speedRange, targetHit);
alpha = targetHit;
set(hitHandle,'AlphaData', alpha);
hold off
exportgraphics(ax,'landscape.png','Resolution',300)
end
