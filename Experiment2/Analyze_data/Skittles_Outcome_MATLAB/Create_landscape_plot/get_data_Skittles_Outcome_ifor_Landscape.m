%% A script for transforming my data and excluding based on the criteria
function [same, diff, angle, v, v_alt] = get_data_Skittles_Outcome_ifor_Landscape(resultData,number_of_subj,subject_clean)
   
    % Organise the variables in usable format
    response         =[];
    correct          =[];
    RT_type1         =[];
    RT_type2         =[];
    vdiff_staircase  =[];
    vdiff            =[];
    BRP_alt.v        =[];
    BRP.v            =[];
    condition        =[];
    unequalPosthit   =[];
    errorTrials      =[];
    stimulus         =[];
    confidence       =[];
    targetHit        =[];
    targetHit_alt    =[];
    check_target    =[];
    angle =  [];
    v = [];
    v_alt = [];
    
    for b = 1: length(resultData.type1)   %to loop among blocks
        if resultData.useDaq == 0 %means using mouse for report (run script from home)
            response=[response resultData.type1(b).response]; % concatenates the result from every block in one line
            response =(strcmp(response,'w')); %means 1 to the left 0 to the right
        else
            response=[response resultData.type1(b).response]; % concatenates the result from every block in one line
        end
        correct=[correct resultData.type1(b).correct];
        RT_type1= [RT_type1 resultData.type1(b).RT];
        RT_type2= [RT_type2 resultData.type2(b).RT];
        vdiff_staircase=[vdiff_staircase resultData.type1(b).vdiff_staircase];
        vdiff=[vdiff resultData.type1(b).vdiff];
        condition=[condition resultData.block(b).condition];
        unequalPosthit=[unequalPosthit resultData.type1(b).unequalPosthit];
        errorTrials= [errorTrials resultData.type1(b).errorTrials];
        confidence=[confidence resultData.type2(b).conf];
        targetHit= [targetHit resultData.type1(b).targetHit]   ; % new addition
        targetHit_alt=[targetHit_alt resultData.type1(b).targetHit_alt];       resultData.type1(1).check
        check_target = [check_target resultData.type1(b).check ]; 
        angle =  [angle  resultData.block(b).releaseAngle ];
        v = [v resultData.block(b).releaseSpeed];
        v_alt = [v_alt resultData.block(b).releaseSpeed_alt];
    end % here i stop looping among blocks and start refering to all the trials
    
    
    if numel(find(errorTrials==1))< (length(resultData.type1) * length(resultData.type1(1).correct))/10 % Participants excluded: more than 10% of trials they made a mistake in responding to the type-1 task
        comment{1}=('Procced with this Participant');
        
        %measure the rest now...
        % Set Stimulus
        stimulus = ones(size(vdiff)); %preallocate space for stimulus, to have the size of the total trials
        stimulus(vdiff<0) = 0;         % to the left probably later called S1: I made it that way because in trils2counts the naming does not match the fact that here response==1, means to the left
        response_corrected = 1-response; % i want response==0 meaning to the left, while in the script it is defined: resultData.type1(block).response(trial)     =leftTrajectorySelected
        
 
        %% Test if target Hit/alt Hit corresponds with the condition
          % some time the same condition or diff condition is not saved corectly.
          % This is the case because in some instances due to the staircase we
          % had to select a vdiff that resulted in the condition different from
          % what was originally saved.
          % Find these trials and make the appropriate changes
           
          condition_same_real = targetHit == targetHit_alt; %find those trials they are same or different in terms of target hit
          condition_diff_real = targetHit ~= targetHit_alt; %find those trials they are same or different in terms of target hit
          
          same = condition == 's'; % The outcome was the same
          diff = condition == 'd'; % The outcome was different
          
          error_in_condition_registration_same = find(condition_same_real & ~same); %trials that while the outcome was same was not register as being in condition 'same'
          error_in_condition_registration_diff = find(condition_diff_real & ~ diff);             %trials that while the outcome was diff was not register as being in condition 'diff'
          
          if ~isempty(error_in_condition_registration_same) || ~isempty(error_in_condition_registration_diff)            
              %Find what happened in those trials
              % For those Not registered as same:
              if  ~isempty(error_in_condition_registration_same)
                  condition(error_in_condition_registration_same(ismember(check_target(error_in_condition_registration_same),[1 2 11 14]))) = 's';   %These check values created while run experiment indicate condition = 'same'
              end
              % For those Not registered as DIFF:
              if  ~isempty(error_in_condition_registration_diff)
                  condition(error_in_condition_registration_same(ismember(check_target(error_in_condition_registration_same),[5 6 13 12]))) = 'd';
              end
          end
          
        
        %% Inspect trials to exlude
        %1) Trials will be excluded if the selected vdiff  is not the staircased vdiff
        %    and if because of that the trial is trivially easy or much
        %    too hard compared  to other trials of the same participant
        %    If vdiff_corrected ~=vdiff_staircase, is outside of the boundaries (2std), then exclude it
        
        % 1(i)Find if:  vdiff ~= vdiff_staircase
        vdiff_equals_staircase=ismembertol(abs(vdiff),abs(vdiff_staircase)); %gives the trials where vdiff_correct==vdiff_staircase
        vdiff_diff_staircase=~vdiff_equals_staircase;
        
        index_wrong_v=find(vdiff_diff_staircase);      %gives the index of trials where the vdiff used is not the staircased(see procedure)
        
        if ~isempty(index_wrong_v)
            
            index_wrong_v_same = find(vdiff_diff_staircase(condition=='s'));
            index_wrong_v_diff = find(vdiff_diff_staircase(condition=='d'));
            
            
            % 2(ii) get the lower and upper threshold for exclusion of trials based on vdiff
            %% NOW that we have 2 separate staircase one for same one for diff i need to exclude them based on the staircase of the respective condition
            % same
            vdiff_same = vdiff(condition=='s');
         
            mean_vdiff_Last10_Values_same = movmean(abs(vdiff_same),[10 0]);%M = movmean(A,[kb kf]) computes the mean with a window of length kb+kf+1 that includes the element in the current position, kb elements backward, and kf elements forward
            std_vdiff_Last10_Values_same = movstd(abs(vdiff_same),[10 0]);
    
            upper_threshold_2std_same = abs(mean_vdiff_Last10_Values_same + (2*std_vdiff_Last10_Values_same));
            lower_threshold_2std_same = abs(mean_vdiff_Last10_Values_same - (2*std_vdiff_Last10_Values_same));
            
            vdiff_outside2std_same = upper_threshold_2std_same(vdiff_diff_staircase(condition=='s'))<=abs(vdiff_same(vdiff_diff_staircase(condition=='s')))|abs(lower_threshold_2std_same(vdiff_diff_staircase(condition=='s')))>=abs(vdiff_same(vdiff_diff_staircase(condition=='s'))); %gives logic output when this is true for the vdiff_diff_staircased
            
            index_wrong_v_exclude_same=index_wrong_v_same(vdiff_outside2std_same); %gives the index of the trials that are outside 2STD and these should be excluded!!
            index_wrong_v_exclude_same(index_wrong_v_exclude_same==1) = [] ;       %trial 1 has mean and limits equal to its value. make no sense to exclude
            
               
            % diff
            vdiff_diff = vdiff(condition =='d');
            mean_vdiff_Last10_Values_diff = movmean(abs(vdiff_diff),[10 0]);%M = movmean(A,[kb kf]) computes the mean with a window of length kb+kf+1 that includes the element in the current position, kb elements backward, and kf elements forward
            std_vdiff_Last10_Values_diff = movstd(abs(vdiff_diff),[10 0]);
    
            upper_threshold_2std_diff = abs(mean_vdiff_Last10_Values_diff + (2*std_vdiff_Last10_Values_diff)); %i use absolute values as i do the same with vdiff
            lower_threshold_2std_diff = abs(mean_vdiff_Last10_Values_diff - (2*std_vdiff_Last10_Values_diff));
            
            vdiff_outside2std_diff = upper_threshold_2std_diff(vdiff_diff_staircase(condition=='d'))<=abs(vdiff_diff(vdiff_diff_staircase(condition=='d')))|lower_threshold_2std_diff(vdiff_diff_staircase(condition=='d'))>=abs(vdiff_diff(vdiff_diff_staircase(condition=='d'))); %gives logic output when this is true for the vdiff_diff_staircased
            index_wrong_v_exclude_diff=index_wrong_v_diff(vdiff_outside2std_diff);%gives the index of the trials that are outside 2STD and these should be excluded!!
            
                    
            index_wrong_v_exclude_all = [index_wrong_v_exclude_diff index_wrong_v_exclude_same];
            problem_vdiff_staircased=zeros(size(vdiff)); %preallocate space to have the size of the total trials
            problem_vdiff_staircased(index_wrong_v_exclude_all)=1;% logic indexing for those who have very big or very small vdiff
            
   
            cleanIndexes=find(RT_type1 <= 8 & RT_type1 >= 0.2 & ~errorTrials==1 & ~unequalPosthit==1 & ~problem_vdiff_staircased==1); % find the indexes of the trials to be included
        else
            cleanIndexes=find(RT_type1 <= 8 & RT_type1 >= 0.2 & ~errorTrials==1 & ~unequalPosthit==1);
        end       
    end
    condition_analysis       = condition(cleanIndexes);
    same= condition_analysis=='s';
    diff= condition_analysis=='d';
    angle = angle(cleanIndexes);
    v= v(cleanIndexes);
    v_alt= v_alt(cleanIndexes);
end % here i stop looping among participants




