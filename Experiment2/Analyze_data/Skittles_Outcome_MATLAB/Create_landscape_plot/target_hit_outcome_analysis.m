function targetHit = target_hit_outcome_analysis(BRP,SkitSet)

%input is an array of BRPs 
%output array of booleans that tell you whether you hit the target

targetHit =nan(1,length(BRP));
for i=1:length(BRP)
    targetHit(i)= ~isnan(BRP(i).x_collision) && ( (~BRP(i).posthit) || (BRP(i).t_posthit>BRP(i).t_collision)) && (BRP(i).t_collision < SkitSet.max_fligth_time) ;
end
end


