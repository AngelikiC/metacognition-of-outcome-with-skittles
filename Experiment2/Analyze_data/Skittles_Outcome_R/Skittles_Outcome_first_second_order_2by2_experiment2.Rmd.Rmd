---
title: "Metacognition_Outcome_2by2design"
author: "Angeliki Charalampaki"
date: "March 2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(reshape2) #melt is here
library(here)
library(dplyr)
library(hrbrthemes) #theme ipsum
library(ggplot2)
library(rstatix) #to test normality of data using Shapiro-Wilk test
library(ggpol) # for the plots. it works directly with the outcome of the anova analysis (afex_plot)
library(ez) # for Nonparametric ANOVA (based on permutation tests)
library(BayesFactor)
library(ggpubr) #ggarrange
library(afex) # for the aov_ez, ANOVA

```


# Load Data from experiment 2  
```{r load_data}
#because we saw that 4 participants had negative dprime we will run the same analysis excluding only those participants as follows
dataset=2 #1 for all participant 2 only participants with positive d'

if (dataset==1){
performance_outcome = read.csv(here('Performance.csv'))
data1<- melt(performance_outcome,id.vars = "Participant")

rt_outcome =  read.csv(here("reaction_time_outcome.csv"))
data_rt <- melt(rt_outcome,id.vars = "Participant")

dprime_target =  read.csv(here("target_MLE_JASP_28_part.csv"))

dprime_target$condition <- as.factor(dprime_target$condition)
dprime_target$target<- as.factor(dprime_target$target)

}else{
performance_outcome_all = read.csv(here('Performance.csv'))
performance_outcome<- performance_outcome_all[!(performance_outcome_all$Participant=="213AX" | performance_outcome_all$Participant=="214XW"| performance_outcome_all$Participant=="342RD"|performance_outcome_all$Participant=="432PT"),]
data1_all <- melt(performance_outcome_all,id.vars = "Participant")
data1 <- melt(performance_outcome,id.vars = "Participant")



rt_outcome_all =  read.csv(here("reaction_time_outcome.csv"))
rt_outcome <- rt_outcome_all[!(rt_outcome_all$Participant=="213AX" | rt_outcome_all$Participant=="214XW"| rt_outcome_all$Participant=="342RD"|rt_outcome_all$Participant=="432PT"),]
data_rt <- melt(rt_outcome,id.vars = "Participant")

dprime_target_all =  read.csv(here("target_MLE_JASP_28_part.csv"))
# dprime_target_all = dprime_target
dprime_target<- dprime_target_all[!(dprime_target_all$Participant=="213AX" | dprime_target_all$Participant=="214XW"| dprime_target_all$Participant=="342RD"|dprime_target_all$Participant=="432PT"),]
}

```

# Specify colors for plots
```{r colours}

InCongruent_colour="#e5cce5"
Congruent_colour= "#f5dc90"
gr_wisk="#000000"

#set a basic theme
theme_set(theme_ipsum(plot_title_face = "plain", plot_title_margin = 10,axis_text_size = 20,plot_title_size = 25))

```


# Accuracy
```{r accuracy}
accuracy_target <- subset(data1,variable %in% c("correct_same_Target_hit","correct_same_Target_No_hit","correct_diff_Target_hit", "correct_diff_Target_No_hit" ))%>%
  rename(percent_correct = value) %>%
  mutate(condition = ifelse(variable == "correct_same_Target_hit"|variable == "correct_same_Target_No_hit", "Congruent","Incongruent" ))%>%
  mutate(target = ifelse(variable == "correct_same_Target_hit"|variable == "correct_diff_Target_hit", "Hit","Miss" ))


accuracy_target$condition <- as.factor(accuracy_target$condition)
accuracy_target$target<- as.factor(accuracy_target$target)
accuracy_target$Participant=as.factor(accuracy_target$Participant)




accuracy_target_all <- subset(data1_all,variable %in% c("correct_same_Target_hit","correct_same_Target_No_hit","correct_diff_Target_hit", "correct_diff_Target_No_hit" ))%>%
  rename(percent_correct = value) %>%
  mutate(condition = ifelse(variable == "correct_same_Target_hit"|variable == "correct_same_Target_No_hit", "Congruent","Incongruent" ))%>%
  mutate(target = ifelse(variable == "correct_same_Target_hit"|variable == "correct_diff_Target_hit", "Hit","Miss" ))


accuracy_target_all$condition <- as.factor(accuracy_target_all$condition)
accuracy_target_all$target<- as.factor(accuracy_target_all$target)
accuracy_target_all$Participant=as.factor(accuracy_target_all$Participant)

```

```{r accuracy_summary}
#----- Get Data Summary
accuracy_target %>%   group_by(condition,target) %>%
  dplyr::summarise(mean(percent_correct), sd(percent_correct))


accuracy_target_all %>%   group_by(condition,target) %>%
  dplyr::summarise(mean(percent_correct), sd(percent_correct))
```

## Normality check
#(if p vale you get > 0.05 then null hypothesis stands, which means the data are normal distributed )
```{r accuracy_normality_check}
accuracy_target%>%
  group_by(condition, target) %>%
  shapiro_test(percent_correct)

accuracy_target_all%>%
  group_by(condition, target) %>%
  shapiro_test(percent_correct)
```
#We see one cell deviate from normality,and after trying different transformations we run non parametric ANOVA


# Run non parametric ANOVA
```{r nonparamANOVA_accuracy_run, include=FALSE}

# Nonparametric ANOVA (based on permutation tests)
nonParamANOVA_accuracy <- ezPerm(accuracy_target,
                               wid=Participant,
       dv=percent_correct,
       within=c("condition", "target"),
       perms = 1e3) #1e3

nonParamANOVA_accuracy_all <- ezPerm(accuracy_target_all,
                               wid=Participant,
       dv=percent_correct,
       within=c("condition", "target"),
       perms = 1e3) #1e3
```

```{r nonparamANOVA}
nonParamANOVA_accuracy
nonParamANOVA_accuracy_all
# Get the BF now:
bf_accuracy = anovaBF(percent_correct ~ condition*target + Participant, data = accuracy_target,
             whichRandom="Participant")
bf_accuracy

bf_accuracy[4]/bf_accuracy[3] # compare the model with interaction with the one without to get the BF of interaction


bf_accuracy_all = anovaBF(percent_correct ~ condition*target + Participant, data = accuracy_target_all,
             whichRandom="Participant")
bf_accuracy_all

bf_accuracy[4]/bf_accuracy[3] # compare the model with interaction with the one without to get the BF of interaction

```
#We find main effects and interaction.



# Confidence
```{r Confidence}
#mean confidence 2by2
confidence_target<- subset(data1,variable %in% c("mean_confidence_same_Target_hit","mean_confidence_same_Target_No_hit","mean_confidence_diff_Target_hit","mean_confidence_diff_Target_No_hit"))%>%
 rename(mean_confidence = value) %>%
 mutate(condition = ifelse(variable == "mean_confidence_same_Target_hit"|variable == "mean_confidence_same_Target_No_hit", "Congruent","Incongruent" ))%>%
 mutate(target = ifelse(variable == "mean_confidence_same_Target_hit"|variable =="mean_confidence_diff_Target_hit","Hit","Miss" ))


confidence_target$condition <- as.factor(confidence_target$condition)
confidence_target$target<- as.factor(confidence_target$target)
confidence_target$Participant<- as.factor(confidence_target$Participant)



confidence_target_all<- subset(data1_all,variable %in% c("mean_confidence_same_Target_hit","mean_confidence_same_Target_No_hit","mean_confidence_diff_Target_hit","mean_confidence_diff_Target_No_hit"))%>%
 rename(mean_confidence = value) %>%
 mutate(condition = ifelse(variable == "mean_confidence_same_Target_hit"|variable == "mean_confidence_same_Target_No_hit", "Congruent","Incongruent" ))%>%
 mutate(target = ifelse(variable == "mean_confidence_same_Target_hit"|variable =="mean_confidence_diff_Target_hit","Hit","Miss" ))


confidence_target_all$condition <- as.factor(confidence_target_all$condition)
confidence_target_all$target<- as.factor(confidence_target_all$target)
confidence_target_all$Participant<- as.factor(confidence_target_all$Participant)
```

```{r Confidence_summary}
#----- Get Data Summary
confidence_target %>%   group_by(condition,target) %>%
  dplyr::summarise(mean(mean_confidence), sd(mean_confidence))

confidence_target_all %>%   group_by(condition,target) %>%
  dplyr::summarise(mean(mean_confidence), sd(mean_confidence))
```

## Normality check Confidence
#(if p vale you get > 0.05 then null hypothesis stands, which means the data are normal distributed )
```{r confidence_normality_check}
confidence_target%>%
  group_by(condition, target) %>%
  shapiro_test(mean_confidence)

confidence_target_all%>%
  group_by(condition, target) %>%
  shapiro_test(mean_confidence)
```
#We see all are normal

## Run the 2by2 ANOVA for Confidence
```{r Confidence_ANOVA}
aov_confidence<-aov_ez(id = "Participant",
                               dv = "mean_confidence",
                               data = confidence_target,
                               within = c("condition", "target"),
                               type = "II")
nice(aov_confidence)
summary(aov_confidence)

# Now get the BF
confidence_target$Participant=as.factor(confidence_target$Participant)
bf_confidence = anovaBF(mean_confidence ~ condition*target + Participant, data = confidence_target,
             whichRandom="Participant")
bf_confidence


bf_confidence[4]/bf_confidence[3] #




aov_confidence_all<-aov_ez(id = "Participant",
                               dv = "mean_confidence",
                               data = confidence_target_all,
                               within = c("condition", "target"),
                               type = "II")
nice(aov_confidence_all)
summary(aov_confidence_all)

# Now get the BF
confidence_target_all$Participant=as.factor(confidence_target_all$Participant)
bf_confidence_all = anovaBF(mean_confidence ~ condition*target + Participant, data = confidence_target_all,
             whichRandom="Participant")
bf_confidence_all


bf_confidence_all[4]/bf_confidence_all[3] #
```
#There are only main effect of condition and target hit, but no interaction but BF shows also interaction



# Make violin  plots for Confidence interaction and main effects
```{r violin_plots_confidence}

Incongruent_colour="#e5cce5"
Congruent_colour= "#f5dc90"
gr_wisk="#000000"

#set a basic theme
theme_set(theme_ipsum(plot_title_face = "plain", plot_title_margin = 50,axis_text_size = 35,plot_title_size = 40)+  theme(legend.position='none',axis.title.x = element_blank()))



c<-  ggplot(confidence_target_all,aes(interaction(condition,target), y=mean_confidence, fill=condition)) +
 ggdist::stat_halfeye(adjust = 1, width = .55, .width = 0, justification = -.4) +

  ggbeeswarm::geom_quasirandom( size = 3,alpha = 0.5,width = .3,show.legend = FALSE, position = position_jitterdodge(dodge.width = 1)) +
   geom_boxplot(width = .1, outlier.shape = NA,show.legend = FALSE,position=position_dodge(1),lwd=1) +
  scale_fill_manual(values = c(Congruent_colour,Incongruent_colour))+
   theme(legend.position="bottom",legend.title=element_blank()) +
   theme(legend.text = element_text( size = 40)) +
  xlab("") +
  ylab("")+
  ylim(30,100)+
scale_x_discrete(name="target",labels=c("             Hit","","            Miss", ""))  +
  ggtitle("B. Mean confidence")

c


c_positive<-  ggplot(confidence_target,aes(interaction(condition,target), y=mean_confidence, fill=condition)) +
 ggdist::stat_halfeye(adjust = 1, width = .55, .width = 0, justification = -.4) +

  ggbeeswarm::geom_quasirandom( size = 3,alpha = 0.5,width = .3,show.legend = FALSE, position = position_jitterdodge(dodge.width = 1)) +
   geom_boxplot(width = .1, outlier.shape = NA,show.legend = FALSE,position=position_dodge(1),lwd=1) +
  scale_fill_manual(values = c(Congruent_colour,Incongruent_colour))+
   theme(legend.position="bottom",legend.title=element_blank()) +
   theme(legend.text = element_text( size = 40)) +
  xlab("") +
  ylab("")+
  ylim(25,100)+
scale_x_discrete(name="target",labels=c("              Hit","","            Miss", ""))  +
  ggtitle("B. Mean confidence")

c_positive


```



## dprime

#Get summary of data
```{r d_prime_summary}
# We decided to keep the naming congruent/incongruent therefore i will change the name of the condition so i will make small changes
dprime_target_all$condition <- as.factor(dprime_target_all$condition)
dprime_target_all$target<- as.factor(dprime_target_all$target)
dprime_target_all$Participant<- as.factor(dprime_target_all$Participant)

dprime_target_all <- dprime_target_all %>%
  rename(condition_old=condition) %>%
  mutate(condition = ifelse(condition_old == "Same", "Congruent","Incongruent" ))
dprime_target$Participant=as.factor(dprime_target$Participant)
dprime_target$target=as.factor(dprime_target$target)
dprime_target$condition=as.factor(dprime_target$condition)
dprime_target_all$Participant=as.factor(dprime_target_all$Participant)
dprime_target_all$target=as.factor(dprime_target_all$target)
dprime_target_all$condition=as.factor(dprime_target_all$condition)
dprime_target$condition <- as.factor(dprime_target$condition)
dprime_target$target<- as.factor(dprime_target$target)
dprime_target$Participant<- as.factor(dprime_target$Participant)


dprime_target <- dprime_target %>%
  rename(condition_old=condition) %>%
  mutate(condition = ifelse(condition_old == "Same", "Congruent","Incongruent" ))

#----- Get Data Summary

dprime_target %>%   group_by(condition,target_hit) %>%
  dplyr::summarise(mean(dprime), sd(dprime))

dprime_target %>%   group_by(condition) %>%
  dplyr::summarise(mean(dprime), sd(dprime))

dprime_target %>%   group_by(target_hit) %>%
  dplyr::summarise(mean(dprime), sd(dprime))


#----- Get Data Summary

dprime_target_all %>%   group_by(condition,target_hit) %>%
  dplyr::summarise(mean(dprime), sd(dprime))

dprime_target_all %>%   group_by(condition) %>%
  dplyr::summarise(mean(dprime), sd(dprime))

dprime_target_all %>%   group_by(target_hit) %>%
  dplyr::summarise(mean(dprime), sd(dprime))


```

## Normality check
#(if p vale you get > 0.05 then null hypothesis stands, which means the data are normal distributed )
```{r dprime_normality_check}
dprime_target%>%
  group_by(condition, target_hit) %>%
  shapiro_test(dprime)

dprime_target_all%>%
  group_by(condition, target_hit) %>%
  shapiro_test(dprime)

```
#We see one cell deviates from normality, and  after i tried all possible transformations decided to go with non parametric analysis


# Run non parametric ANOVA
```{r nonparamANOVA_dprime_run, include=FALSE}

# Nonparametric ANOVA (based on permutation tests)
nonParamANOVA_dprime <- ezPerm(dprime_target,
                               wid=Participant,
       dv=dprime,
       within=c("condition", "target"),
       perms = 1e3) #1e3

nonParamANOVA_dprime_all <- ezPerm(dprime_target_all,
                               wid=Participant,
       dv=dprime,
       within=c("condition", "target"),
       perms = 1e3) #1e3

```

```{r nonparamANOVA_dprime}
nonParamANOVA_dprime
nonParamANOVA_dprime_all 

# Visualize the permutation results
permResult = ezBoot(
  data = dprime_target,
  dv=dprime,
  wid=Participant,
  within=c("condition", "target"),
  iterations = 1e3, #1e3 or higher is best for publication
  resample_within = FALSE)

permResult_all = ezBoot(
  data = dprime_target_all,
  dv=dprime,
  wid=Participant,
  within=c("condition", "target"),
  iterations = 1e3, #1e3 or higher is best for publication
  resample_within = FALSE)


```

## Make the dprime plot and save
```{r nonParamANOVA_dprime}

d<-  ggplot(dprime_target_all,aes(interaction(condition,target), y=dprime, fill=condition)) +
  ggdist::stat_halfeye(adjust = 1, width = .55, .width = 0, justification = -.4) +

  ggbeeswarm::geom_quasirandom( size = 3,alpha = 0.5,width = .3,show.legend = FALSE, position = position_jitterdodge(dodge.width = 1)) +
   geom_boxplot(width = .1, outlier.shape = NA,show.legend = FALSE,position=position_dodge(1),lwd=1) +
  scale_fill_manual(values = c(Congruent_colour,Incongruent_colour))+
   theme(legend.position="bottom",legend.title=element_blank()) +
   theme(legend.text = element_text( size = 40)) +
  xlab("") +
  ylab("")+
  #ylim(-1,4)+
scale_x_discrete(name="target",labels=c("            Hit","","              Miss", ""))  +
  ggtitle("A. d'")

d

# # If you want to make the plot only with the posiive participants:
d_positive <-  ggplot(dprime_target,aes(interaction(condition,target), y=dprime, fill=condition)) +
  ggdist::stat_halfeye(adjust = 1, width = .55, .width = 0, justification = -.4) +

  ggbeeswarm::geom_quasirandom( size = 3,alpha = 0.5,width = .3,show.legend = FALSE, position = position_jitterdodge(dodge.width = 1)) +
   geom_boxplot(width = .1, outlier.shape = NA,show.legend = FALSE,position=position_dodge(1),lwd=1) +
  scale_fill_manual(values = c(Congruent_colour,Incongruent_colour))+
   theme(legend.position="bottom",legend.title=element_blank()) +
   theme(legend.text = element_text( size = 40)) +
  xlab("") +
  ylab("")+
  ylim(-1,4)+
 scale_x_discrete(name="target",labels=c("              Hit","","              Miss", ""))  +
  ggtitle("A. d'")

d_positive
```

```{r Plot_combined,echo=TRUE, fig.height=5, fig.width=15}
# Combine plots:
all_plots_positive<-ggarrange(d_positive, c_positive, nrow=1, common.legend = TRUE, legend="bottom")
all_plots_positive
ggsave("Supplementary_figure_dprime_confidence_positivesubj.png", path= here(), device = png,scale = 1,width = 40, height = 20,units = "cm" ,dpi = 700)
all_plots<-ggarrange(d, c, nrow=1, common.legend = TRUE, legend="bottom")
all_plots
ggsave("Figure4.png", path= here(), device = png,scale = 1,width = 40, height = 22,units = "cm" ,dpi = 700)

```

## Reaction time for first order task

```{r RT1}

rt1_target=  subset(data_rt,variable %in%
  c("rt_Type1_same_hit", "rt_Type1_same_miss","rt_Type1_diff_hit","rt_Type1_diff_miss"))%>%
  rename(rt1 = value) %>%
  mutate(condition = ifelse(variable == "rt_Type1_same_hit"|variable == "rt_Type1_same_miss", "Congruent","Incongruent" ))%>%
  mutate(target = ifelse(variable == "rt_Type1_same_hit"|variable =="rt_Type1_diff_hit","Hit","Miss" ))


rt1_target$condition <- as.factor(rt1_target$condition)
rt1_target$target<- as.factor(rt1_target$target)

```

## Get data summary
```{r RT1_summary}
#----- Get Data Summary
rt1_target %>%   group_by(condition,target) %>%
  dplyr::summarise(mean(rt1), sd(rt1))

rt1_target %>%   group_by(condition) %>%
  dplyr::summarise(mean(rt1), sd(rt1))

```

# Normality check RT1
#(if p vale you get > 0.05 then null hypothesis stands, which means the data are normal distributed )
```{r RT1_normality_check}
rt1_target%>%
  group_by(condition, target) %>%
  shapiro_test(rt1)



```
#We see 3 cells deviate from normality, and after trying different transformations as with d prime we run non parametric ANOVA:
#Since no transformation helps we run the non parametric ANOVA

```{r RT1_non_parametric_ANOVA_run, include=FALSE}
# Nonparametric ANOVA (based on permutation tests)

nonParamANOVA_RT1 <- ezPerm(rt1_target,
                               wid=Participant,
       dv=rt1,
       within=c("condition", "target"),
       perms = 1e3) #1e3

```

```{r RT1_non_parametric_ANOVA}
nonParamANOVA_RT1

```


## Reaction time for second order task

```{r RT2}

rt2_target=  subset(data_rt,variable %in%
  c("rt_Type2_same_hit", "rt_Type2_same_miss","rt_Type2_diff_hit","rt_Type2_diff_miss"))%>%
  rename(rt2 = value) %>%
  mutate(condition = ifelse(variable == "rt_Type2_same_hit"|variable == "rt_Type2_same_miss", "Congruent","Incongruent" ))%>%
  mutate(target = ifelse(variable == "rt_Type2_same_hit"|variable =="rt_Type2_diff_hit","Hit","Miss" ))


rt2_target$condition <- as.factor(rt2_target$condition)
rt2_target$target<- as.factor(rt2_target$target)

```

## Get data summary
```{r rt2_summary}
#----- Get Data Summary
rt2_target %>%   group_by(condition,target) %>%
  dplyr::summarise(mean(rt2), sd(rt2))
```

# Normality check rt2 
#(if p vale you get > 0.05 then null hypothesis stands, which means the data are normal distributed )
```{r rt2_normality_check}
rt2_target%>%
  group_by(condition, target) %>%
  shapiro_test(rt2)

```
#We see 4 cells deviate from normality, and after trying different transformations as with d prime we run non parametric ANOVA:


#test transformation
```{r rt2_transform_1/x_and_check}
rt2_target<- rt2_target %>%
  mutate(trans_rt2= 1/(rt2))

rt2_target%>%
  group_by(condition, target) %>%
  shapiro_test(trans_rt2)
```
#transformation works!
#Now that data are normally distributed we will run the 2by 2 ANOVA on accuracy

## Run the 2 way repeated ANOVA or RT2
```{r accuracy_ANOVA}
aov_rt2<-aov_ez(id = "Participant",
                               dv = "trans_rt2",
                               data = rt2_target,
                               within = c("condition", "target"),
                               type = "II")
nice(aov_rt2)
summary(aov_rt2)

# Try to see what happens in unfitted data
aov_rt2_raw <-aov_ez(id = "Participant",
                               dv = "rt2",
                               data = rt2_target,
                               within = c("condition", "target"),
                               type = "II")
nice(aov_rt2_raw)
summary(aov_rt2_raw)




# Now get the BF form transformed
rt2_target$Participant=as.factor(rt2_target$Participant)
bf_rt2 = anovaBF(trans_rt2 ~ condition*target + Participant, data = rt2_target, whichRandom="Participant")
bf_rt2

bf_rt2[4]/bf_rt2[3] # compare the model with interaction with the one without to get the BF of interaction

#check also the raw

rt2_target$Participant=as.factor(rt2_target$Participant)
bf_rt2_raw = anovaBF(rt2 ~ condition*target + Participant, data = rt2_target, whichRandom="Participant")
bf_rt2_raw

bf_rt2_raw[4]/bf_rt2_raw[3] # compare the model with interaction with the one without to get the BF of interaction

```
#We get the same main effect of condition in RT2 in both transformed and raw data. but no interaction. Same for freq & BF

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
