library(here)
library(jsonlite)
library(tidyverse)

# Take all exported text files from JATOS. 
# As I wrote it,  it expects a list of inidivdual (separate) txt files, one per subject.  
# It likely almost works with a single file containing all data together, haven't tested it though 

#dataFiles <- list.files(path = here(), pattern = ".txt")
dataFiles <- list.files(path =getwd(), pattern = ".txt")
# If results were exported individually
if (length(dataFiles)==1){
  #Read the whole thing in 
#  fileName <- here(dataFiles[1])
    fileName <- dataFiles[1]
  allJSONs <- readChar(fileName, file.info(fileName)$size)
  singleJSONs <- Hmisc::string.break.line(allJSONs)
  JSONdata <- singleJSONs[[1]]
  nSubj <- length(JSONdata)
} else {
  JSONdata <- dataFiles
  nSubj <- length(JSONdata)
}

# Initialize general data structure
resultData <- data.frame(mouse.x = numeric(),
                         mouse.y = numeric(),
                         mouse.time = numeric(),
                         subject = factor(),
                         trial = factor())


for (subj in 1:nSubj){
  if (length(dataFiles)==1){
    results.thisSubj <- as.data.frame(jsonlite::fromJSON(singleJSONs[[1]][subj]))
  } else {
    results.thisSubj <- as.data.frame(jsonlite::fromJSON(here(dataFiles[subj])))
    if (!exists("resultData")){
      resultData <- setNames(data.frame(matrix(ncol = 21, nrow = 0)), colnames(results.thisSubj))
    }
  }
  resultData <- rbind(results.thisSubj, resultData)
}

# Convert empty cells to NA
resultData <- resultData %>% mutate_all(na_if,"")

# Make sure columns are doubles
columns <- colnames(resultData)
for (col in columns) {
  if (col == "ID") { next }
  resultData[[col]] = as.numeric(resultData[[col]])
}

# Calculate handedness
resultData <- resultData %>% group_by(ID) %>% 
  mutate(totalLeft = sum(writeL, 
                        drawL,
                        throwL,
                        scissorsL,
                        teethL,
                        knifeL,
                        spoonL,
                        broomL,
                        matchL,
                        jarL, na.rm = TRUE)) %>%
  mutate(totalRight = sum(writeR, 
                          drawR,
                          throwR,
                          scissorsR,
                          teethR,
                          knifeR,
                          spoonR,
                          broomR,
                          matchR,
                          jarR, na.rm = TRUE)) #%>%
resultData <- resultData %>%
  mutate(handednessScore = (totalRight - totalLeft)/(totalRight + totalLeft) * 100)

mainResults <- resultData %>% 
  select(ID, throwR, throwL, handednessScore) %>% 
  print()

mean(mainResults$handednessScore)
sd(mainResults$handednessScore)