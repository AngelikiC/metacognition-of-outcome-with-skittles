%% A script to exclude participants based on the exclusion criterian set  
% You just run the whole script. You must have run first the scripts: 
% 1) create_table_for_performance_analysis_Skittles_AoM
% 2) get_participants_ID_for_each_type_of_analysis_AoM


clearvars; close all;

%% Define path
experiment_folder = ['~' filesep 'Experiment1'  filesep];
data_path = [experiment_folder 'Data_Skittles_AoM_raw' filesep];
save_path = [experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];
preprocessed_data_path =[experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
    

cd(preprocessed_data_path)

%% Make performance table:
% Load ready table:
load('Performance.mat')

% Set Participants to include 
load('participants_after_exclusion_is_done.mat')

% and then create the appropriate table to use in R
subject_clean = participant_MLE; 

%perf_R = Performance;
perf_R = Performance(ismember(Performance.Participant,subject_clean),:);
writetable(perf_R,'performance_included_only_part_Skittles_AoM_forR.csv')


%% Make table for vdiff same diff

vdiff = readtable('vdiff_table.csv');

vdiff_R= vdiff(ismember(vdiff_R.Participant,subject_clean),:);

writetable(vdiff_R,'vdiff_included_only_part_Skittles_AoM_forR.csv')

%% Make table for number of trials

n_R = number_Trials(ismember(number_Trials.Participant,subject_clean),:);
writetable(n_R,'number_of_trials_included_only_part_Skittles_AoM_forR.csv')

    

%% Make table for logistic (maybe unecessary since logistic is affected by type1 performance)
load('data_Skittles_AoM_all.mat')


mixedL_dataAoM=  data_AoM(ismember(data_AoM.id,subject_clean),:);
writetable(mixedL_dataAoM,'data_Skittles_AoM_mixedLog_forR_included_only_part_.csv') 
 

%% Make table for RT for type 1 and Type 2 

load('data_Skittles_AoM_all.mat')
 
    

