% A script for preparing the data for mration analysis. There are two options. 1) for MLE and 2) for Bayesian. This affects the Padcells on the trials2count helper script
% So just press run and choose which analsysis you need

clearvars; close all;

%% Define path
experiment_folder = ['~' filesep 'Experiment1' filesep];
data_path = [experiment_folder  filesep 'Data_Skittles_AoM_raw' filesep];
preprocessed_data_path = [experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
save_path = preprocessed_data_path;
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];

cd(preprocessed_data_path)


%% define participants
load('participants_after_exclusion_is_done.mat')
subject_clean = participant_MLE;

%% 1) load data for MLE
type_of_analysis = input('\n\n What analysis do you want? MLE(M) or Bayes(B) ','s');

switch lower(type_of_analysis)
    case 'm'
        % create table to fill all participants
        metadata_Skittles_AoM_MLE = cell2table(cell(0,7),'VariableNames',{'Participant','same_meta_d','same_d','same_mratio','diff_meta_d','diff_d','diff_m_ratio'});

        for number_of_subj=1:length(subject_clean)
            
            fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
            load(sprintf('data_analysis_%s.mat',subject_clean{number_of_subj}));
            Participant={subject_clean{number_of_subj}};
            
            
            %% Normalize confidence in every condition (i comment the things i dont use for checking specific, eg Target Hit vs overALL analysis!!)
             
            %Normalize confidence:
            conf_norm = (confidence_analysis-min(confidence_analysis))/(max(confidence_analysis)-min(confidence_analysis));
            
            % Confidence Same:
            nor_conf_same =conf_norm(condition_analysis == 's');
                       
            % Confidence Different
            nor_conf_diff=conf_norm(condition_analysis == 'd');

            nRatings=6; % number of ratings
            
            % Ratings for: Overall
            ratings_same=discretize(nor_conf_same,nRatings); % bins the confidence ratings: ratings_same==1 are the smallest and ratings_same==nRatings is the largest rating
            ratings_diff=discretize(nor_conf_diff,nRatings);
            
            
            % Stimulus for: Overall
            stimulus_same = stimulus_analysis(condition_analysis == 's');
            stimulus_diff = stimulus_analysis(condition_analysis == 'd');
            
            
            % Response for: Overall
            response_same=response_analysis(condition_analysis == 's');
            response_diff=response_analysis(condition_analysis == 'd');
            
            
            % Trials2Counts for: Overall
            
            %  function [nR_S1, nR_S2] = trials2counts(stimID, response, rating, nRatings, padCells, padAmount)
            [nR_S1_same, nR_S2_same]=trials2counts(stimulus_same,response_same,ratings_same,nRatings,1);
            [nR_S1_diff, nR_S2_diff]=trials2counts(stimulus_diff,response_diff,ratings_diff,nRatings,1);
            
            % fit data for MLE: Overall
            %fit = fit_meta_d_MLE(nR_S1, nR_S2, s, fncdf, fninv)
            fit_same=fit_meta_d_MLE(nR_S1_same, nR_S2_same);
            fit_diff=fit_meta_d_MLE(nR_S1_diff, nR_S2_diff);
            
            % Plot for: Overall (There is another script: for this: angeliki_plot_step_click_nR_S.m)
            subplot(2,1,1); plot(nR_S1_same);
            hold('on')
            subplot(2,1,2); plot(nR_S2_same);
            hold('on')
            
            MetaData = table(Participant,fit_same.meta_da,fit_same.da,fit_same.M_ratio,fit_diff.meta_da,fit_diff.da,fit_diff.M_ratio);
            MetaData.Properties.VariableNames={'Participant','same_meta_d','same_d','same_mratio','diff_meta_d','diff_d','diff_m_ratio'};
            
            metadata_Skittles_AoM_MLE = [metadata_Skittles_AoM_MLE; MetaData];
%             save('metadata_Skittles_AoM_MLE.mat', 'metadata_Skittles_AoM_MLE');
%             writetable(metadata_Skittles_AoM_MLE,'metadata_Skittles_AoM_MLE.csv')
            save('metadata_Skittles_AoM_MLE_34part_conf.mat', 'metadata_Skittles_AoM_MLE');
            writetable(metadata_Skittles_AoM_MLE,'metadata_Skittles_AoM_MLE_34part_conf.csv')


        end
            case 'b'
               
                for number_of_subj=1:length(subject_clean)
                    
                    fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
                    load(sprintf('data_analysis_%s.mat',subject_clean{number_of_subj}));
                    Participant=subject_clean{number_of_subj};
                    
                    
                    %% Normalize confidence in every condition (i comment the things i dont use for checking specific, eg Target Hit vs overALL analysis!!)
                    
                     %Normalize confidence:
                    conf_norm = (confidence_analysis-min(confidence_analysis))/(max(confidence_analysis)-min(confidence_analysis));
                    
                    % Confidence Same:
                    nor_conf_same =conf_norm(condition_analysis == 's');
                    
                    % Confidence Different
                    nor_conf_diff=conf_norm(condition_analysis == 'd');          
                    
                    nRatings=6; % number of ratings
                    
                    % Ratings for: Overall
                    ratings_same=discretize(nor_conf_same,nRatings); % bins the confidence ratings: ratings_same==1 are the smallest and ratings_same==nRatings is the largest rating
                    ratings_diff=discretize(nor_conf_diff,nRatings);
                    
                    
                    % Stimulus for: Overall
                    stimulus_same = stimulus_analysis(condition_analysis == 's');
                    stimulus_diff = stimulus_analysis(condition_analysis == 'd');
                    
                    
                    % Response for: Overall
                    response_same=response_analysis(condition_analysis == 's');
                    response_diff=response_analysis(condition_analysis == 'd');
                    
                    
                    
                    % Trials2Counts for H-meta
                    [nR_S1_same, nR_S2_same]=trials2counts(stimulus_same,response_same,ratings_same,nRatings,0);
                    [nR_S1_diff, nR_S2_diff]=trials2counts(stimulus_diff,response_diff,ratings_diff,nRatings,0);
                    
                    % create the cells for the Hmetad analysis
                    same_nR_S1{number_of_subj} = nR_S1_same;
                    same_nR_S2{number_of_subj} = nR_S2_same;
                    
                    diff_nR_S1{number_of_subj} = nR_S1_diff;
                    diff_nR_S2{number_of_subj} = nR_S2_diff;
                end
                    Skittles_AoM.nR_S1(1).counts = same_nR_S1; %(same)
                    Skittles_AoM.nR_S1(2).counts = diff_nR_S1; %diff
                    Skittles_AoM.nR_S2(1).counts = same_nR_S2; %(same)
                    Skittles_AoM.nR_S2(2).counts = diff_nR_S2; %diff
                    
%                     Skittles_AoM.fit = fit_meta_d_mcmc_groupCorr(Skittles_AoM.nR_S1, Skittles_AoM.nR_S2);
                    Skittles_AoM.fit = fit_meta_d_mcmc_groupCorr_thinnin3_extra(Skittles_AoM.nR_S1, Skittles_AoM.nR_S2);
                    save('h_fit_AoM_same_diff_th3extra_conf_34part.mat','Skittles_AoM' );

                    
                    % see how it looks
                    Skittles_AoMDiff  = Skittles_AoM.fit.mcmc.samples.mu_logMratio(:,:,1) - Skittles_AoM.fit.mcmc.samples.mu_logMratio(:,:,2);
                    hdi_Skittles_AoM = calc_HDI(Skittles_AoMDiff(:));
                    fprintf(('\n Mratio session values = %.2f and %.2f'), exp(Skittles_AoM.fit.mu_logMratio(1)), exp(Skittles_AoM.fit.mu_logMratio(2)));
                    fprintf(['\n Estimated difference in Mratio between sessions: ', num2str(exp(Skittles_AoM.fit.mu_logMratio(1)) - exp(Skittles_AoM.fit.mu_logMratio(2)))])
                    fprintf(['\n HDI on difference in log(Mratio): ', num2str(hdi_Skittles_AoM) '\n\n'])
                    plotSamples(exp(Skittles_AoM.fit.mcmc.samples.mu_logMratio(:,:,1)))
                    plotSamples(exp(Skittles_AoM.fit.mcmc.samples.mu_logMratio(:,:,2)))
                    % % Plot each of the group distributions for Mratio:
                    name='Skittles AoM h meta : difference between: same & diff';
                    plotSamples(Skittles_AoMDiff)
                    title(name)
                    saveas(gcf,sprintf('%s.jpg', name))
                    
                    mratio_same=Skittles_AoM.fit.Mratio(:,1);
                    mratio_diff=Skittles_AoM.fit.Mratio(:,2);
                    d_same = Skittles_AoM.fit.d1(:,1);
                    d_diff = Skittles_AoM.fit.d1(:,2);
                    meta_h_array = [mratio_same mratio_diff d_same d_diff];
                    meta_h = array2table(meta_h_array);
                    meta_h.Participant=subject_clean;
                    meta_h.Properties.VariableNames ={'mratio_same' 'mratio_diff' 'd_same' 'd_diff','Participant'};
                    
                     writetable(meta_h,'metadata_Skittles_AoM_all_34_H_meta.csv')
                     save('metadata_Skittles_AoM_all_34_H_meta','meta_h','Skittles_AoM');
end
      
        fprintf('\n******\n Finished All the analyses \n******\n\n');
        