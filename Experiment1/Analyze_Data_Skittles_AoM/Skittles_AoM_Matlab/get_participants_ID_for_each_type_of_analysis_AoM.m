%% A script to get participants name for using in the hmeta analysis(based on number of trials within condition)
% You just run the whole script.

clearvars; close all;

%% Define path
experiment_folder = ['~' filesep 'Experiment1'  filesep];
data_path = [experiment_folder 'Data_Skittles_AoM_raw' filesep];
save_path = [experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];
preprocessed_data_path =[experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];



cd(save_path)
%%  Find if there is participant with type 1 performance >85 or <60 
% and then create the appropriate table to use in R
load('Performance.mat') %load the mat file that has the performance from all the participants
index_exclude_performance=find(Performance.correct_same>85|Performance.correct_same<60| Performance.correct_diff>85 | Performance.correct_diff<60);
participant_exclude_perform_same_diff = unique(Performance.Participant(index_exclude_performance)); %exclude those who have perform for both group without repeating their index


%% Find if S1 and S2 were not equally represented and exclude these participan
% I CHECK WITHIN SAME AND DIFFERENT. 

total_same=number_Trials.n_same_S1+number_Trials.n_same_S2	; %number of trials
s1_per_same =(number_Trials.n_same_S1./total_same)*100; %percent S1  The./ divides the element in each row pf one column with the other
%s2_per=(number_Trials.n_same_S2./total_same)*100;
unequal_S_same= find(s1_per_same>70 | s1_per_same<30); %It is the same if i did this for S2
part_excl_S_same=number_Trials.Participant(unequal_S_same);

total_diff=number_Trials.n_diff_S1+number_Trials.n_diff_S2	; %number of trials
s1_per_diff =(number_Trials.n_diff_S1./total_diff)*100; %percent S1  The./ divides the element in each row pf one column with the other
unequal_S_diff= find(s1_per_diff>70 | s1_per_diff<30); %It is the same if i did this for S2
part_excl_S_diff=number_Trials.Participant(unequal_S_diff);

participants_to_exclude_S1_same_diff_separately= unique([part_excl_S_same;  part_excl_S_diff]);


%% Exclude & Create Tables for R
% Concatenate first 
participants_to_exclude= unique([participant_exclude_perform_same_diff;  participants_to_exclude_S1_same_diff_separately]);

participant_MLE =Performance.Participant
participant_MLE(ismember(participant_MLE, participants_to_exclude))=[];


n_trial_corrected_byperformance = number_Trials;

n_trial_corrected_byperformance(ismember(n_trial_corrected_byperformance.Participant,participants_to_exclude),:)=[];

enough_Target_miss_trials = find(n_trial_corrected_byperformance.n_diff_Target_miss>=50 & n_trial_corrected_byperformance.n_same_Target_miss>=50 ); %include those participants that have minimum 50 trials per condition
enough_Target_hit_trials = find(n_trial_corrected_byperformance.n_diff_Target_hit>=50 & n_trial_corrected_byperformance.n_same_Target_hit>=50 );
enough_Target_same_trials = find(n_trial_corrected_byperformance.n_same_Target_miss>=50 & n_trial_corrected_byperformance.n_same_Target_hit>=50 );
enough_Target_diff_trials = find(n_trial_corrected_byperformance.n_diff_Target_miss>=50 & n_trial_corrected_byperformance.n_diff_Target_hit>=50 );

participants_target_miss =n_trial_corrected_byperformance.Participant(enough_Target_miss_trials); 
participants_target_miss(ismember(participants_target_miss, participants_to_exclude))=[];



participant_target_hit = n_trial_corrected_byperformance.Participant(enough_Target_hit_trials);
participant_target_hit(ismember(participant_target_hit, participants_to_exclude))=[]; 


participant_same_target = n_trial_corrected_byperformance.Participant(enough_Target_same_trials);
participant_same_target(ismember(participant_same_target, participants_to_exclude))=[]; 

participant_diff_target = n_trial_corrected_byperformance.Participant(enough_Target_diff_trials);
participant_diff_target(ismember(participant_diff_target, participants_to_exclude))=[]; 


save('participants_for_target_hmeta_separate_for_condition.mat', 'participants_target_miss','participant_target_hit','participant_same_target','participant_diff_target');

% Save participant name to have it ready for each analysis
save('participants_after_exclusion_is_done.mat', 'participant_MLE','participants_target_miss','participant_target_hit','participant_same_target','participant_diff_target');

 
    

