%% A script for transforming my data and excluding based on the criteria
% Simply run the script.

clearvars; close all;

%% Define path
experiment_folder = ['~' filesep 'Experiment1'  filesep];
data_path = [experiment_folder 'Data_Skittles_AoM_raw' filesep];
save_path = [experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];
preprocessed_data_path =[experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];


cd(data_path)

%% Define participants
[subject_list] = (get_participant_name(data_path))'; %i have a function for that and i transpoze to meet my standrard script for analysis


%% Create Table where data will be loaded

% create an empty table that will contain the data from all participants at tne end
data_AoM = cell2table(cell(0,13),'VariableNames',{'id','trial', 'response', 'correct','condition','stimulus','confidence',...
    'TargetHit', 'TargetHit_alt','vdiff_stair', 'vdiff','rt_Type1', 'rt_Type2'});



%% Load data
for number_of_subj=1:length(subject_list)
    
    fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_list{number_of_subj});
    load([data_path  subject_list{number_of_subj} filesep 'SkittlesResult_' subject_list{number_of_subj} '.mat' ]);
    
    % Organise the variables in usable format
    
    response         =[];
    correct          =[];
    RT_type1         =[];
    RT_type2         =[];
    vdiff_staircase  =[];
    vdiff            =[];
    vdiff_corrected  =[];
    BRP_alt.v        =[];
    BRP.v            =[];
    condition        =[];
    unequalPosthit   =[];
    errorTrials      =[];
    BRPleft          =[];
    BRP.trajectory.x5=[];
    BRP_alt.trajectory.x5=[];
    stimulus         =[];
    confidence       =[];
    targetHit        =[];
    targetHit_alt    =[];
    
    
    for b = 1:length(resultData.type1)   %to loop among blocks
        response=[response resultData.type1(b).response]; % concatenates the result from every block in one line
        correct=[correct resultData.type1(b).correct];
        RT_type1= [RT_type1 resultData.type1(b).RT];
        RT_type2= [RT_type2 resultData.type2(b).RT];
        vdiff_staircase=[vdiff_staircase resultData.type1(b).vdiff_staircase];
        vdiff=[vdiff resultData.type1(b).vdiff];
        BRP_alt.v=[BRP_alt.v resultData.block(b).BRP_alt.v];
        BRP.v=[BRP.v resultData.block(b).BRP.v];
        condition=[condition resultData.block(b).condition];
        unequalPosthit=[unequalPosthit resultData.type1(b).unequalPosthit];
        errorTrials= [errorTrials resultData.type1(b).errorTrials];
        confidence=[confidence resultData.type2(b).conf];
        targetHit= [resultData.type1(:).targetHit]   ; % new addition
        targetHit_alt=[resultData.type1(:).targetHit_alt];
        
        
        %for t=1:120 % in case i need to loop within a structure of a block % i use this to loop within each block f.x. for trajectory
        % I need this as there was an error in code when running
        % experiment. See comments.
        
        for t=1:length(resultData.block(b).condition)
            BRP.trajectory.x5=[BRP.trajectory.x5 resultData.block(b).BRP(t).trajectory.x(5)];  % i need this to find the trials to the left or right
            BRP_alt.trajectory.x5=[BRP_alt.trajectory.x5 resultData.block(b).BRP_alt(t).trajectory.x(5)];
        end  %here i stop looping within each block and return to refuring to looping among all the blocks
        
    end % here i stop looping among blocks and start refering to all the trials
    
    
    %if numel(find(errorTrials==1))<48 % Participants will be excluded in case they indicate themselves in more than 10% of trials % that they made a mistake in responding to the type-1 task
     if numel(find(errorTrials==1))< (length(resultData.type1) * length(resultData.type1(1).correct))/10 
         comment{1}=('Procced with this Participant');
        
        
        %measure the rest now...
        vdiff_corrected=BRP.v-BRP_alt.v; % originally i had it vdiff_corrected=BRP_alt.v-BRP.v but changed it to match the BRPLEFT subtraction signs
        
        % Set Stimulus
        stimulus=ones(size(vdiff_corrected)); %preallocate space for stimulus, to have the size of the total trials
        stimulus(vdiff_corrected<0)=0; % to the left probably later called S1, stimulus(vdiff_corrected>0)=right or S2: I made it that way because in trils2counts the naming does not match the fact that here response==1, means to the left
        
        % Set BRPleft
        % according to getType1Response_1ms_LJ.m: BRPleft = BRP.trajectory.x(5)-BRP_alt.trajectory.x(5)<0
        % is registered as the trials to the LEFT
        BRPleft=BRP.trajectory.x5-BRP_alt.trajectory.x5<0; %when this is true (1) stimulus==LEFT, in this case participant chose the correct one but the trajectory was registered as the opossite. due to trajectories at an early point shifted
        left_trials=vdiff_corrected<=0; %this is true (1) when trials to the left
        trials_to_flip=find(BRPleft~=left_trials);
        
        correct_test=stimulus==(1-response); %use this as a sanity check for the responses registered as correct. i use 1-response because stimulus=0 means stimulus was left stim  response==1 means response was left
        correct2verify=find(correct~=correct_test);
        
        if  ~isempty(trials_to_flip) && ~isempty(correct2verify) %when they dont match it means there are trials that need to flip && the correct~=(stimulus==response) which means we have a problem with registration
            %i am interested for the cases the response was not registered properly
            % wrongLEFT=find(BRPleft(trials_to_flip)==1); in case i am also  interested in changing the BRPleft
            % wrongRIGHT=find(BRPleft(trials_to_flip)==0); in case i am also  interested in changing the BRPleft
            
            fprintf('\n******\nThere are trials to flip for participant %s\n******\n\n',subject_list{number_of_subj});
            
            response_corrected=1-response; % I do that to match the indexing for trial2count as to have response==1 or stimullus==1 to refer to the right trajectory
            wrongLEFT_response=find(response(trials_to_flip)==1); % find the trials registered wrong as left traject responses
            wrongRIGHT_response=find(response(trials_to_flip)==0);% find the trials registered wrong as right traject responses
            
            response_corrected(trials_to_flip(wrongLEFT_response))=1; % change the response for the wrong registered left
            response_corrected(trials_to_flip(wrongRIGHT_response))=0; % change the response for the wrong registered right
            
                    
            comment{2}=('Corrected Responses');
            fprintf('\n******\nTrials were flipped for participant %s\n******\n\n',subject_list{number_of_subj});
                    
            %% check if it makes sense that these changes were made
            correct_test2=stimulus==response_corrected; % define the correct based on the changes
            correct_sanity_check=find(correct~=correct_test2);
            if isempty(correct_sanity_check) && ~isempty(trials_to_flip) && ~isempty(correct2verify)
                comment{7}=('Reasonable flipping trials');
            else
                fprintf('\n******\nProblem with flipped trials!!!!!! for participant %s\n******\n\n',subject_list{number_of_subj});
                comment{7}=('Error flipping trials');
            end
            
            
        elseif isempty(trials_to_flip) && ~isempty(correct2verify)
            comment{2}=('Error error with vdiff!!!');
            response_corrected=1-response; % i want response==0 meaning to the left
        else
            fprintf('\n******\n NO trials to flip for participant %s\n******\n\n',subject_list{number_of_subj});
            comment{2}=('NO problem with responses');
            response_corrected=1-response; % i want response==0 meaning to the left
        end
        
      
       
        %% Inspect trials to exlude
        %1) check if vdiff was correctly calculated, in theory they should have equal abs values
        %  meaning vdiff==vdiff_corrected
        
        
        %vdiff_reallyDiff=ismembertol(abs(vdiff_corrected),abs(vdiff), 3.330669073875470e-16); % (older version of the code)i used this tolerance method because they are floating numbers and this means they are basically never even. I used the abs because vdiff sign was defined randomly
        vdiff_reallyDiff=ismembertol(abs(vdiff_corrected),abs(vdiff)); % comparing method using a tolerance, because they are floating numbers
        trials_unequal_vdiff_corrected_vdiff=find(~vdiff_reallyDiff);
        if isempty(trials_unequal_vdiff_corrected_vdiff)   %find the trials where vdiff and vdiff_corrected they are not equal within tolerance levels
            comment{3}=('vdiff was correctly calculated');
        else
            comment{3}=('Problem with vdiff');
        end
        
        
        % 2) Trials will be excluded if the selected vdiff  is not the staircased vdiff
        %    and if because of that the trial is trivially easy or much
        %    too hard compared  to other trials of the same participant
        %    If vdiff_corrected ~=vdiff_staircase, is outside of the boundaries (2std), then exclude it
              
        vdiff_equals_staircase=ismembertol(abs(vdiff_corrected),abs(vdiff_staircase)); %gives the trials where vdiff_correct==vdiff_staircase
        trials_v_match=find(vdiff_equals_staircase);
        
        if ~isempty(trials_v_match)
            
            vdiff_diff_staircase=~vdiff_equals_staircase;
            index_correct_v=find(vdiff_equals_staircase);
            index_wrong_v=find(vdiff_diff_staircase); %gives the index of trials where the vdiff used is not the staircased(see procedure)
            percent_number_wrong_v=length(index_wrong_v)*10/48;
            values_vdiff_for_diff_staircase = vdiff_corrected(vdiff_diff_staircase); %gives the values of the vdiff in those unequal trials
            values_vdiff_staircased_for_diff_staircase=vdiff_staircase(vdiff_diff_staircase);%gives the values of the vdiff_staircased in those unequal trials
                        
            
            mean_vdiff_Last10_Values = movmean(abs(vdiff_corrected),[10 0]);%M = movmean(A,[kb kf]) computes the mean with a window of length kb+kf+1 that includes the element in the current position, kb elements backward, and kf elements forward
            std_vdiff_Last10_Values = movstd(abs(vdiff_corrected),[10 0]);
            
            
            upper_threshold_2std = mean_vdiff_Last10_Values + (2*std_vdiff_Last10_Values);
            lower_threshold_2std = mean_vdiff_Last10_Values - (2*std_vdiff_Last10_Values);
            
            vdiff_outside2std=upper_threshold_2std(vdiff_diff_staircase)<=abs(vdiff_corrected(vdiff_diff_staircase))|lower_threshold_2std(vdiff_diff_staircase)>=abs(vdiff_corrected(vdiff_diff_staircase)); %gives logic output when this is true for the vdiff_diff_staircased
            index_wrong_v_include=index_wrong_v(~vdiff_outside2std);%gives the index of the trials that are inside 2STD
            index_wrong_v_exclude=index_wrong_v(vdiff_outside2std);%gives the index of the trials that are outside 2STD and these should be excluded!!
            
            problem_vdiff_staircased=zeros(size(vdiff_corrected)); %preallocate space to have the size of the total trials
            problem_vdiff_staircased(index_wrong_v_exclude)=1;% logic indexing for those who have very big vdiff
            v_excluded=numel(index_wrong_v_exclude);
            
                       
            % plot vdiff %uncommenting saves the image
            plot(1:length(vdiff_corrected),abs(vdiff_corrected),'-o','MarkerFaceColor','c');
            hold on
            plot(index_wrong_v_include,abs(vdiff_corrected(index_wrong_v_include)),'o','MarkerFaceColor','g','MarkerEdgeColor','g');
            plot(index_wrong_v_exclude,abs(vdiff_corrected(index_wrong_v_exclude)),'o','MarkerFaceColor','r','MarkerEdgeColor','r');
            legend('vdiff staircased','within 2STD','out of 2STD');
            title('Vdiff')
            hold off
            saveas(gca,fullfile(figure_path,sprintf('vdiff participant_%s',subject_list{number_of_subj})),'png');
            
           
            cleanIndexes=find(RT_type1 <= 8 & RT_type1 >= 0.2 & ~errorTrials==1 & ~unequalPosthit==1 & ~problem_vdiff_staircased==1); % find the indexes of the trials to be included
            comment{4}=sprintf('%d Trials had random vdiff', numel(index_wrong_v));
            comment{5}=sprintf('%d of which were excluded', v_excluded);
            comment{6}=sprintf('%d Trials were included',numel(cleanIndexes));
        else
            cleanIndexes=find(RT_type1 <= 8 & RT_type1 >= 0.2 & ~errorTrials==1 & ~unequalPosthit==1);
            comment{4}=('No trials had random vdiff');
            comment{5}=('No trials had random vdiff');
            comment{6}=sprintf('%d Trials were included',numel(cleanIndexes));
        end      
        
         
    else
        fprintf('\n******\n `Participant needs to be excluded: %s\n******\n\n',subject_list{number_of_subj});
        comment{1}=('Participant to be excluded');
        
    end
    
   
    
    
    correct_analysis=correct(cleanIndexes);
    response_analysis=response_corrected(cleanIndexes);
    stimulus_analysis=stimulus(cleanIndexes);
    condition_analysis=condition(cleanIndexes);
    confidence_analysis=confidence(cleanIndexes);
    trials_for_analysis=numel(cleanIndexes); % How many trials will be finally included?
    targetHit_analysis=targetHit(cleanIndexes);
    targetHit_alt_analysis=targetHit_alt(cleanIndexes);
    BRP_alt_v=BRP_alt.v(cleanIndexes);
    BRP_v=BRP.v(cleanIndexes);
    vdiff_staircase_analysis=vdiff_staircase(cleanIndexes);
    vdiff_corrected_analysis=vdiff_corrected(cleanIndexes);
    BRPleft=BRPleft(cleanIndexes);
    RT_type1_analysis                 = RT_type1(cleanIndexes);
    RT_type2_analysis                 = RT_type2(cleanIndexes);
    id                       = subject_list{number_of_subj};
    id_column =  repmat(id,trials_for_analysis,1);
    trials = 1: trials_for_analysis;
      
    
    %% make some sanity check before saving the data
    % some time the same condition or save condition is not saved corectly.
    % Find these trials and make the appropriate changes
       targetHit_really= targetHit_analysis==targetHit_alt_analysis; %find those trials they are same or different in terms of target hit
       same=condition_analysis == 's'; % The outcome was the same
       diff=condition_analysis == 'd'; % The outcome was different
       sanity_target_Hit=find(targetHit_really~=same);
       sanity_target_NoHit=find((~targetHit_really)~=diff);
       
       
       
       if ~isempty(sanity_target_Hit) || ~isempty(sanity_target_NoHit)
           comment{8}=('problem with original target hit info');
           if targetHit_analysis(sanity_target_Hit)==targetHit_alt_analysis(sanity_target_Hit)
               condition_analysis(sanity_target_Hit)='s';
               
           else
               condition_analysis(sanity_target_Hit)='d';
           end
       else
           
           comment{8}=('No problem with original target hit info');
       end
            
       %% create  table for each participant
       sub_data_AoM = table(id_column,trials',response_analysis', correct_analysis', condition_analysis',stimulus_analysis',...
           confidence_analysis',targetHit_analysis', targetHit_alt_analysis',vdiff_corrected_analysis',vdiff_staircase_analysis', RT_type1_analysis', RT_type2_analysis');
       sub_data_AoM.Properties.VariableNames={'id','trial', 'response', 'correct','condition','stimulus','confidence',...
           'TargetHit', 'TargetHit_alt','vdiff','vdiff_stair', 'rt_Type1', 'rt_Type2'};
       
       %concatenate data from all participants
       data_AoM = [data_AoM; sub_data_AoM];
       cd(save_path); 
       save(sprintf('data_analysis_%s',subject_list{number_of_subj}),'correct_analysis','response_analysis','stimulus_analysis',...
           'condition_analysis','confidence_analysis','trials_for_analysis','targetHit_analysis','targetHit_alt_analysis', 'BRP_alt_v',...
           'BRP_v','vdiff_staircase_analysis','vdiff_corrected_analysis','BRPleft','comment', 'vdiff_staircase_analysis','RT_type1_analysis','RT_type2_analysis','sub_data_AoM');
  
       
       fprintf('\n******\n Finished the analysis for participant %s\n******\n\n',subject_list{number_of_subj});
       
    
end % here i stop looping among participants

    
       save('data_Skittles_AoM_all.mat','data_AoM')
       
     fprintf('\n******\n Finished All the analyses \n******\n\n');

