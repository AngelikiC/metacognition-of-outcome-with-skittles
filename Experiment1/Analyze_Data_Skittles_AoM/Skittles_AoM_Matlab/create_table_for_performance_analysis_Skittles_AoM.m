%% A script to create tables of performance (correct,confidence..) and number of trials for exclusions and further analysis. 
% Simply run the script.
% In my case i have a folder structure as follows: 'Data_Skittles_AoM' in
% which i have a folder with the raw data('Data_Skittles_AoM_raw') and one
% were i save the preprocessed data ('Data_Skittles_AoM_preprocessed')

clearvars; close all;
experiment_folder = ['~' filesep 'Experiment1'  filesep];
data_path = [experiment_folder 'Data_Skittles_AoM_raw' filesep];
save_path = [experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];
preprocessed_data_path =[experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];



cd(preprocessed_data_path)

%% define participants
% Set Participants to include 
load('participants_after_exclusion_is_done.mat')

subject_clean = participant_MLE;

% create an empty table that will contain the data from all participants at tne end
Performance = cell2table(cell(0,24),'VariableNames',{'Participant','percent_correct','mean_confidence_same',...
    'std_confidence_same','mean_confidence_diff','std_confidence_diff','correct_same','correct_diff',...
    'correct_same_Target_hit','correct_same_Target_miss','correct_diff_Target_hit','correct_diff_Target_miss',...
    'mean_confidence_same_Target_hit','std_confidence_same_Target_hit','mean_confidence_same_Target_miss',...
    'std_confidence_same_Target_miss','mean_confidence_diff_Target_hit','std_confidence_diff_Target_hit',...
    'mean_confidence_diff_Target_miss','std_confidence_diff_Target_miss','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'});

Performance_correct = cell2table(cell(0,12),'VariableNames',{'Participant','percent_correct','correct_same','correct_diff','correct_same_Target_hit','correct_same_Target_miss','correct_diff_Target_hit','correct_diff_Target_miss','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'});

number_Trials = cell2table(cell(0,13),'VariableNames',{'Participant','n_same','n_diff','n_same_Target_hit','n_same_Target_miss',...
    'n_diff_Target_hit','n_diff_Target_miss', 'n_S1','n_S2','n_same_S1','n_same_S2','n_diff_S1','n_diff_S2'});

vdiff = cell2table(cell(0,5),'VariableNames',{'Participant','vdiff_same','vdiff_diff','vdiff_stair_same','vdiff_stair_diff'});  


RTs = cell2table(cell(0,15),'VariableNames',{'Participant', 'rt_Type1', 'rt_Type2', 'rt_Type1_same', 'rt_Type1_diff', 'rt_Type2_same', 'rt_Type2_diff',...
    'rt_Type1_same_hit', 'rt_Type1_same_miss', 'rt_Type1_diff_hit', 'rt_Type1_diff_miss', 'rt_Type2_same_hit', 'rt_Type2_same_miss', 'rt_Type2_diff_hit', 'rt_Type2_diff_miss'});
    
hit_rate_all = cell2table(cell(0,4),'VariableNames',{'Participant', 'hit_rate', 'hit_rate_same','hit_rate_diff'});
%% load data
for number_of_subj=1:length(subject_clean)
    
    fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
    load(sprintf('data_analysis_%s.mat',subject_clean{number_of_subj}));
    Participant={subject_clean{number_of_subj}};
    
    same = condition_analysis == 's'; % The outcome was the same
    diff = condition_analysis == 'd'; % The outcome was different
  
    Target_hit = targetHit_analysis ==1;
    Target_miss = targetHit_analysis ==0;
    
    
    %overall performance
    percent_correct =(sum(correct_analysis==1)/trials_for_analysis)*100;
   
    % same correct
    correct_same = (sum(correct_analysis==1 & same)/sum(same))*100;
    correct_same_S1 = (sum(correct_analysis==1 & same & stimulus_analysis==0)/sum(same & stimulus_analysis==0))*100;
    correct_same_S2 = (sum(correct_analysis==1 & same & stimulus_analysis==1)/sum(same & stimulus_analysis==1))*100;
    
    correct_same_Target_hit = (sum(correct_analysis==1 & same & targetHit_analysis==1)/sum(same & targetHit_analysis==1))*100;
    correct_same_Target_miss = (sum(correct_analysis==1 & same & targetHit_analysis==0)/sum(same & targetHit_analysis==0))*100;
   
    % same confidence
    mean_confidence_same = mean(confidence_analysis(same)); %all
    std_confidence_same = std(confidence_analysis(same));
    
    mean_confidence_same_Target_hit = mean(confidence_analysis(same & targetHit_analysis==1)); %hit
    std_confidence_same_Target_hit = std(confidence_analysis(same & targetHit_analysis==1));
    
    mean_confidence_same_Target_miss = mean(confidence_analysis(same & targetHit_analysis==0)); % No hit
    std_confidence_same_Target_miss = std(confidence_analysis(same & targetHit_analysis==0));
        
    
    %different correct
    correct_diff = (sum(correct_analysis==1 & diff)/sum(diff))*100;
    correct_diff_S1 = (sum(correct_analysis==1 & diff & stimulus_analysis==0)/sum(diff & stimulus_analysis==0))*100;
    correct_diff_S2 = (sum(correct_analysis==1 & diff & stimulus_analysis==1)/sum(diff & stimulus_analysis==1))*100;
    
    correct_diff_Target_hit = (sum(correct_analysis==1 & diff & targetHit_analysis==1)/sum(diff & targetHit_analysis==1))*100;
    correct_diff_Target_miss = (sum(correct_analysis==1 & diff & targetHit_analysis==0)/sum(diff & targetHit_analysis==0))*100;
    
    % different confidence
    mean_confidence_diff = mean(confidence_analysis(diff));
    std_confidence_diff = std(confidence_analysis(diff));
    
    mean_confidence_diff_Target_hit = mean(confidence_analysis(diff & targetHit_analysis==1));
    std_confidence_diff_Target_hit = std(confidence_analysis(diff & targetHit_analysis==1));    
    
    mean_confidence_diff_Target_miss = mean(confidence_analysis(diff & targetHit_analysis==0));
    std_confidence_diff_Target_miss = std(confidence_analysis(diff & targetHit_analysis==0));    
    
    %number of trials for each condition
    n_S1=sum(stimulus_analysis==0);    % number of S1 trials
    n_S2=sum(stimulus_analysis==1);    % number of S2 trials
    n_same=sum(same);
    n_diff=sum(diff);
    n_same_S1=sum(same & stimulus_analysis==0);
    n_same_S2=sum(same & stimulus_analysis==1);
    n_diff_S1=sum(diff & stimulus_analysis==0);
    n_diff_S2=sum(diff & stimulus_analysis==1);
    n_same_Target_hit=sum(same & targetHit_analysis==1);
    n_same_Target_miss=sum(same & targetHit_analysis==0);
    
    n_diff_Target_hit=sum(diff & targetHit_analysis==1);
    n_diff_Target_miss=sum(diff & targetHit_analysis==0);
    
    
    
    %Hit Rate
    hit = sum(Target_hit)/length(Target_hit) *100;
    hit_same = sum(Target_hit & same)/length(Target_hit & same) *100;
    hit_diff = sum(Target_hit & diff)/length(Target_hit & diff) *100;

    
    % Get Reaction time for type 1 and type 2
    rt_Type1 = round(mean (RT_type1_analysis),2);
    rt_Type1_same =  round(mean (RT_type1_analysis(same)),2);
    rt_Type1_diff =  round(mean (RT_type1_analysis(diff)),2);
    rt_Type1_same_hit = round(mean (RT_type1_analysis(same & Target_hit)),2);
    rt_Type1_same_miss = round(mean (RT_type1_analysis(same & Target_miss)),2);
    rt_Type1_diff_hit = round(mean (RT_type1_analysis(diff & Target_hit)),2);
    rt_Type1_diff_miss = round(mean (RT_type1_analysis(diff & Target_miss)),2);
  
    
    rt_Type2 = round(mean (RT_type2_analysis),2);
    rt_Type2_same =  round(mean (RT_type2_analysis(same)),2);
    rt_Type2_diff =  round(mean (RT_type2_analysis(diff)),2);
    rt_Type2_same_hit = round(mean (RT_type2_analysis(same & Target_hit)),2);
    rt_Type2_same_miss = round(mean (RT_type2_analysis(same & Target_miss)),2);
    rt_Type2_diff_hit = round(mean (RT_type2_analysis(diff & Target_hit)),2);
    rt_Type2_diff_miss = round(mean (RT_type2_analysis(diff & Target_miss)),2);
    
    
    
    %Vdiff values
    vdiff_same=vdiff_corrected_analysis(condition_analysis == 's');
    vdiff_diff=vdiff_corrected_analysis(condition_analysis == 'd');
    
    mean_vdiff_same=mean(abs(vdiff_same));
    mean_vdiff_diff=mean(abs(vdiff_diff));
    
    vdiff_stair_same=vdiff_staircase_analysis(condition_analysis == 's');
    vdiff_stair_diff=vdiff_staircase_analysis(condition_analysis == 'd');
    
    mean_vdiff_stair_same=mean(abs(vdiff_stair_same));
    mean_vdiff_stair_diff=mean(abs(vdiff_stair_diff));
    
    %% create  table for each participant
    performance = table(Participant,percent_correct,mean_confidence_same,std_confidence_same,mean_confidence_diff,...
        std_confidence_diff,correct_same,correct_diff,correct_same_Target_hit,correct_same_Target_miss,correct_diff_Target_hit,correct_diff_Target_miss,...
        mean_confidence_same_Target_hit,std_confidence_same_Target_hit,mean_confidence_same_Target_miss,std_confidence_same_Target_miss,mean_confidence_diff_Target_hit,...
        std_confidence_diff_Target_hit,mean_confidence_diff_Target_miss,std_confidence_diff_Target_miss, correct_same_S1,correct_same_S2,correct_diff_S1,correct_diff_S2);
    
    performance.Properties.VariableNames={'Participant','percent_correct','mean_confidence_same',...
        'std_confidence_same','mean_confidence_diff','std_confidence_diff','correct_same','correct_diff',...
        'correct_same_Target_hit','correct_same_Target_miss','correct_diff_Target_hit','correct_diff_Target_miss',...
        'mean_confidence_same_Target_hit','std_confidence_same_Target_hit','mean_confidence_same_Target_miss',...
        'std_confidence_same_Target_miss','mean_confidence_diff_Target_hit','std_confidence_diff_Target_hit',...
        'mean_confidence_diff_Target_miss','std_confidence_diff_Target_miss','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'};
    
    performance_correct = table(Participant,percent_correct,correct_same,correct_diff,correct_same_Target_hit,correct_same_Target_miss,correct_diff_Target_hit,correct_diff_Target_miss,correct_same_S1,correct_same_S2,correct_diff_S1,correct_diff_S2);
    
    performance_correct.Properties.VariableNames={'Participant','percent_correct','correct_same','correct_diff','correct_same_Target_hit','correct_same_Target_miss','correct_diff_Target_hit','correct_diff_Target_miss','correct_same_S1','correct_same_S2','correct_diff_S1','correct_diff_S2'};
    
    
    nbrOfTrials = table(Participant,n_same,n_diff,n_same_Target_hit,n_same_Target_miss,n_diff_Target_hit,n_diff_Target_miss,n_S1,n_S2,n_same_S1,n_same_S2,n_diff_S1,n_diff_S2);
    
    nbrOfTrials.Properties.VariableNames={'Participant','n_same','n_diff','n_same_Target_hit','n_same_Target_miss',...
        'n_diff_Target_hit','n_diff_Target_miss', 'n_S1','n_S2','n_same_S1','n_same_S2','n_diff_S1','n_diff_S2'};
    
    
    
    vdiff_sub = table(Participant,mean_vdiff_same,mean_vdiff_diff,mean_vdiff_stair_same,mean_vdiff_stair_diff);
    vdiff_sub.Properties.VariableNames={'Participant','vdiff_same','vdiff_diff','vdiff_stair_same','vdiff_stair_diff'};
    
    reaction_time = table(Participant, rt_Type1, rt_Type2, rt_Type1_same, rt_Type1_diff, rt_Type2_same, rt_Type2_diff, rt_Type1_same_hit, rt_Type1_same_miss,...
        rt_Type1_diff_hit, rt_Type1_diff_miss, rt_Type2_same_hit, rt_Type2_same_miss, rt_Type2_diff_hit, rt_Type2_diff_miss);
    reaction_time.Properties.VariableNames = {'Participant', 'rt_Type1', 'rt_Type2', 'rt_Type1_same', 'rt_Type1_diff', 'rt_Type2_same', 'rt_Type2_diff',...
        'rt_Type1_same_hit', 'rt_Type1_same_miss', 'rt_Type1_diff_hit', 'rt_Type1_diff_miss', 'rt_Type2_same_hit', 'rt_Type2_same_miss', 'rt_Type2_diff_hit', 'rt_Type2_diff_miss'};
    
    hit_rate_sub = table(Participant, hit,hit_same,hit_diff);
    hit_rate_sub.Properties.VariableNames = {'Participant', 'hit_rate','hit_rate_same','hit_rate_diff'};
   
    %concatenate data from all participants
    Performance = [Performance; performance];
    number_Trials = [number_Trials; nbrOfTrials];
    Performance_correct = [Performance_correct; performance_correct];
    vdiff = [vdiff; vdiff_sub];
    
    
    RTs = [RTs ;reaction_time];
    hit_rate_all = [hit_rate_all; hit_rate_sub]
        
end
% save('Performance.mat', 'Performance','number_Trials','performance_correct');
% writetable(Performance,'Performance.csv')
% writetable(number_Trials,'number_Trials.csv')
% writetable(vdiff,'vdiff_table.csv')
% writetable(RTs,'reaction_time_AoM.csv');
writetable(hit_rate_all,'hit_rate_AoM.csv');