%% A script for transforming my data and excluding based on the criteria
% Simply run the script.

clearvars; close all;

%% Define path
experiment_folder = ['~' filesep 'Experiment1'  filesep];
data_path = [experiment_folder 'Data_Skittles_AoM_raw' filesep];
save_path = [experiment_folder filesep 'Data_Skittles_AoM_preprocessed'];
figure_path = [experiment_folder filesep 'Figures_Skittles_AoM'];
data_path ='/Users/Angeliki/Seafile/My Library/Angeliki/Metacognition_of_outcome_Skittles/Experiment1/Analyze_Data_Skittles_AoM/Data_Skittles_AoM_do_not_copy_gitlab/Data_Skittles_AoM_raw/'
save_path =    '/Users/Angeliki/Seafile/My Library/Angeliki/Metacognition_of_outcome_Skittles/Experiment1/Analyze_Data_Skittles_AoM/Data_Skittles_AoM_do_not_copy_gitlab/Data_Skittles_AoM_preprocessed/'

cd(data_path)

%% Define participants
[subject_list] = (get_participant_name(data_path))'; %i have a function for that and i transpoze to meet my standrard script for analysis
load([save_path filesep 'participants_exclusion.mat'])
participants_to_exclude= unique([participant_exclude_perform_overall;  part_excl_S]);
subject_clean=subject_list;
subject_clean(ismember(subject_clean,participants_to_exclude))=[];
% a=readtable('number_Trials.csv')
% a(ismember(a.Participant,participants_to_exclude),:)=[];

%% Create Table where data will be loaded

% create an empty table that will contain the data from all participants at tne end
error_Trials_AoM = cell2table(cell(0,2));
error_Trials_AoM = cell2table(cell(0,2),'VariableNames',{'Participant','n_error_trial'});

%% Load data
for number_of_subj=1:length(subject_clean)
    
    fprintf('\n******\nProcessing Participant %s\n******\n\n', subject_clean{number_of_subj});
    load([data_path  subject_clean{number_of_subj} filesep 'SkittlesResult_' subject_clean{number_of_subj} '.mat' ]);
    Participant={subject_clean{number_of_subj}};
    errorTrials      =[];
    for b = 1:length(resultData.type1)   %to loop among blocks
        errorTrials= [errorTrials resultData.type1(b).errorTrials];
    end
    n_error_trials = sum(errorTrials);
    %% create  table for each participant
    error_trials_AoM = table(Participant, n_error_trials);
    error_trials_AoM.Properties.VariableNames={'Participant','n_error_trial'};
    
    %concatenate data from all participants
    error_Trials_AoM = [error_Trials_AoM; error_trials_AoM];
    cd(save_path);
    
    
end % here i stop looping among participants


% save('error_trials_AoM_all.mat','error_Trials_AoM')

fprintf('\n******\n Finished All the analyses \n******\n\n');

trial_error_mean= mean(error_Trials_AoM.n_error_trial);
trial_error_std = std(error_Trials_AoM.n_error_trial);
iqr(error_Trials_AoM.n_error_trial)